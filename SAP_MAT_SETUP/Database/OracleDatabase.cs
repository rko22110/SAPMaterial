﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SAP_MAT_SETUP.Database
{
    public class OracleDatabase
    {
        public static string ConnectionStrings;//ConfigurationManager.ConnectionStrings["ConnectionString1"].ConnectionString;

        public static OracleConnection conOra;
        public static OracleConnection conOraTr;

        #region Oracle

        public void ConnectDatabase(string ConStr)
        {
            if (ConStr == "TKCDEV")
            {
                ConnectionStrings = "User Id=TKCDEV;Password=TKCDEV;Data Source=172.18.1.8:1521/PRONES;";
            }

            conOra = new OracleConnection(ConnectionStrings);
            conOraTr = new OracleConnection(ConnectionStrings);
        }

        public void Oracle_connectTr()
        {
            if (conOraTr.State == ConnectionState.Open)
            {
                conOraTr.Close();
            }
            else
            {
                conOraTr.Open();
            }
        }
        public DataSet OracleGet(string sql, string tblName)
        {

            OracleDataAdapter da = new OracleDataAdapter(sql, conOra);
            DataSet ds = new DataSet();
            da.Fill(ds, tblName);
            return ds;
        }

        public DataSet OracleGet(string sql, string tblName, OracleParameterCollection parameters)
        {

            OracleDataAdapter da = new OracleDataAdapter(sql, conOra);
            DataSet ds = new DataSet();
            foreach (OracleParameter param in parameters)
            {
                da.SelectCommand.Parameters.Add(param.ParameterName, param.OracleDbType).Value = param.Value;
            }
            da.Fill(ds, tblName);
            return ds;
        }

        public int OracleExecute(string sql)
        {
            int i;
            OracleCommand cmd = new OracleCommand(sql, conOra);
            conOra.Open();
            i = cmd.ExecuteNonQuery();
            conOra.Close();
            return i;
        }

        public int OracleExecute(string sql, OracleParameterCollection parameters)
        {
            int i;
            OracleCommand cmd = new OracleCommand(sql, conOra);
            foreach (OracleParameter param in parameters)
            {
                cmd.Parameters.Add(param.ParameterName, param.OracleDbType).Value = param.Value;
            }
            conOra.Open();
            i = cmd.ExecuteNonQuery();
            conOra.Close();
            return i;
        }

        public int OracleExecute(string sql, OracleParameterCollection parameters, OracleTransaction Tr, OracleConnection Bconn)
        {
            int i;
            OracleCommand cmd = new OracleCommand(sql, Bconn);
            cmd.Transaction = Tr;
            foreach (OracleParameter param in parameters)
            {
                cmd.Parameters.Add(param.ParameterName, param.OracleDbType).Value = param.Value;
            }
            // conOra.Open();
            i = cmd.ExecuteNonQuery();
            // conOra.Close();
            return i;
        }

        public int OracleExecute(string sql, Oracle.ManagedDataAccess.Client.OracleTransaction Tr, Oracle.ManagedDataAccess.Client.OracleConnection Bconn)
        {
            int i;
            Oracle.ManagedDataAccess.Client.OracleCommand cmd = new Oracle.ManagedDataAccess.Client.OracleCommand(sql, Bconn);
            cmd.Transaction = Tr;
            i = cmd.ExecuteNonQuery();
            return i;
        }


        public DataSet OracleExcSto(string stpName, string tblName, Oracle.ManagedDataAccess.Client.OracleParameterCollection parameters)
        {
            Oracle.ManagedDataAccess.Client.OracleCommand cmd = new Oracle.ManagedDataAccess.Client.OracleCommand();
            cmd.Connection = conOra;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = stpName;
            foreach (Oracle.ManagedDataAccess.Client.OracleParameter param in parameters)
            {
                cmd.Parameters.Add(param.ParameterName, param.OracleDbType).Value = param.Value;
            }

            Oracle.ManagedDataAccess.Client.OracleDataAdapter da = new Oracle.ManagedDataAccess.Client.OracleDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds, tblName);
            return ds;
        }

        #endregion
    }

}