//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SAP_MAT_SETUP.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_SPIS_PATH_GTOPAS
    {
        public string PartNumber { get; set; }
        public string SPIS_PATH { get; set; }
        public string Partcommon { get; set; }
        public Nullable<int> Qty { get; set; }
        public string QPoint_PATH { get; set; }
        public Nullable<int> StdQty { get; set; }
        public string StampType { get; set; }
        public Nullable<int> NOPOS { get; set; }
    }
}
