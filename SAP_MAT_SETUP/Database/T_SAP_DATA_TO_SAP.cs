//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SAP_MAT_SETUP.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_SAP_DATA_TO_SAP
    {
        public int seq { get; set; }
        public string DATA_NAME { get; set; }
        public string DATA_SOURCE { get; set; }
        public Nullable<int> flag { get; set; }
    }
}
