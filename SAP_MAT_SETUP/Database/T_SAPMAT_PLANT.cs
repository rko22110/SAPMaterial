//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SAP_MAT_SETUP.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_SAPMAT_PLANT
    {
        public string CUSTID { get; set; }
        public string SOLD_TO { get; set; }
        public string PLANT { get; set; }
        public string DIVISION { get; set; }
        public string CHANNEL { get; set; }
        public string SHIP_TO { get; set; }
        public string CUST_NAME { get; set; }
        public string SELLING_TYPE { get; set; }
        public string CBU_CKD { get; set; }
        public string TAX { get; set; }
        public string Str_Loc { get; set; }
    }
}
