//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SAP_MAT_SETUP.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_SAP_RPT_GROUP
    {
        public string I_ITEM_CD { get; set; }
        public string PLANT { get; set; }
        public string SELL_TYPE { get; set; }
        public string RPT_GROUP { get; set; }
        public string ADD_MNTH { get; set; }
    }
}
