//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SAP_MAT_SETUP.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_SAPMAT_Part_Name
    {
        public string PartNameCD { get; set; }
        public string PartNameDesc { get; set; }
    }
}
