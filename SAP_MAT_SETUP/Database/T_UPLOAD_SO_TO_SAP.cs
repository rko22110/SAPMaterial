//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SAP_MAT_SETUP.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_UPLOAD_SO_TO_SAP
    {
        public int Id { get; set; }
        public string CUST_ID { get; set; }
        public string DATATYPE { get; set; }
        public string CUSTNAME_GROUP { get; set; }
        public string PlantCode { get; set; }
        public string DueTime { get; set; }
        public string Type { get; set; }
        public string Rem1 { get; set; }
        public string Rem2 { get; set; }
    }
}
