//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SAP_MAT_SETUP.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_SAPMAT_REVISE_LOG
    {
        public int LogNo { get; set; }
        public string page { get; set; }
        public string i_Reg_id { get; set; }
        public string updDept { get; set; }
        public string updby { get; set; }
        public Nullable<System.DateTime> updDate { get; set; }
        public Nullable<int> RevNo { get; set; }
    }
}
