//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SAP_MAT_SETUP.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class V_SAP_MAT_SETUP_LAST
    {
        public string I_RegID { get; set; }
        public string CUSTOMER { get; set; }
        public string M_Model { get; set; }
        public string M_Lineoff { get; set; }
        public string M_MatNo { get; set; }
        public string C_CustPNo { get; set; }
        public string M_CustPName { get; set; }
        public Nullable<decimal> M_SellPrice { get; set; }
        public Nullable<System.DateTime> M_ST_Effect { get; set; }
        public string M_MatDesc { get; set; }
        public string M_Remark { get; set; }
        public string P_TkcPName { get; set; }
        public string M_CustName { get; set; }
        public string flag03 { get; set; }
        public string flag02 { get; set; }
        public string flag01 { get; set; }
        public string P_Kb2 { get; set; }
        public string P_Process { get; set; }
        public string P_Spec { get; set; }
        public string P_KBNo { get; set; }
        public string P_FacNo { get; set; }
        public string M_TKCCustPNo { get; set; }
        public Nullable<int> L_BoxQty { get; set; }
        public Nullable<decimal> C_STD { get; set; }
        public string A_Mat_Group { get; set; }
        public string A_Part_Name { get; set; }
        public string A_Mold_Owner { get; set; }
        public string A_Light_Type { get; set; }
        public string A_Type_No { get; set; }
        public string A_Part_Side { get; set; }
        public string I_Status { get; set; }
        public string I_Status_MK { get; set; }
        public string I_Status_Admin { get; set; }
        public string I_Status_CS { get; set; }
        public string I_Status_PC { get; set; }
        public string I_Status_LG { get; set; }
        public string I_Status_CO { get; set; }
        public Nullable<System.DateTime> I_Approve_MK { get; set; }
        public Nullable<System.DateTime> I_Approve_CS { get; set; }
        public Nullable<System.DateTime> I_Approve_PPC { get; set; }
        public Nullable<System.DateTime> I_Approve_LG { get; set; }
        public Nullable<System.DateTime> I_Approve_CO { get; set; }
        public string Mat_Exported { get; set; }
        public string Mat_ExpBy { get; set; }
        public Nullable<System.DateTime> Mat_ExpDate { get; set; }
        public string Price_Exported { get; set; }
        public string Price_ExpBy { get; set; }
        public Nullable<System.DateTime> Price_ExpDate { get; set; }
        public string UpdBy { get; set; }
        public Nullable<System.DateTime> UpdDate { get; set; }
        public string LastUpdBy { get; set; }
        public Nullable<System.DateTime> LastUpdDate { get; set; }
        public string SellPrice_Status { get; set; }
        public string STD_Status { get; set; }
    }
}
