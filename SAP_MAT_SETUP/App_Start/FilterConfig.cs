﻿using System.Web;
using System.Web.Mvc;

namespace SAP_MAT_SETUP
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
