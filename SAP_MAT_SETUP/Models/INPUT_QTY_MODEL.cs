﻿using SAP_MAT_SETUP.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAP_MAT_SETUP.MultipleModels
{
    public class INPUT_QTY_MODEL
    {
        public List<T_IMCT_PART_INPUT_QTY> t_IMCT_PART_INPUT_QTY { get; set; }
        public List<T_CustID_MS> t_CustID_MS { get; set; }
        public List<T_SPIS_PATH_GTOPAS> t_SPIS_PATH_GTOPAS { get; set; }
        public INPUT_QTY_MODEL()
        {
            t_IMCT_PART_INPUT_QTY = new List<T_IMCT_PART_INPUT_QTY>();
            t_CustID_MS = new List<T_CustID_MS>();
            t_SPIS_PATH_GTOPAS = new List<T_SPIS_PATH_GTOPAS>();
        }
    }
}