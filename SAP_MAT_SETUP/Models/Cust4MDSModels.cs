﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAP_MAT_SETUP.Models
{
    public class Cust4MDSModels
    {
        public string CustID { get; set; }
        public string DataType { get; set; }
        public string DataTypeRPT { get; set; }
        public string Sellingtype { get; set; }
    }
}