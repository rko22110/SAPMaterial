﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAP_MAT_SETUP.Models
{
    public class ListDropDown
    {
        public string iKey { get; set; }
        public string iValue { get; set; }

    }




    public class ListDropDownMatG
    {
        public string iKey { get; set; }
        public string iValue { get; set; }

    }

    public partial class Marketing
    {
        public string I_RegID { get; set; }
        public string CUSTOMER { get; set; }
        [Required(ErrorMessage = "Model required")]
        public string M_Model { get; set; }

        [Required(ErrorMessage = "Lineoff required")]
        [Display(Name = "M_Lineoff")]
        public string M_Lineoff { get; set; }

        [Required(ErrorMessage = "Material No required")]
        [Display(Name = "M_MatNo")]
        public string M_MatNo { get; set; }

        /*[Required(ErrorMessage = "MatDesc required")]
        [Display(Name = "M_MatDesc")]
        public string M_MatDesc { get; set; }*/

        /* [Required(ErrorMessage = "TKCCustPNo required")]
         [Display(Name = "M_TKCCustPNo")]
         public string M_TKCCustPNo { get; set; }*/

        [Required(ErrorMessage = "Customer Part Name required")]
        [Display(Name = "M_CustPName")]
        public string M_CustPName { get; set; }
        public string M_Remark { get; set; }

        /* [Required(ErrorMessage = "Sell Price required")]*/
        [RegularExpression(@"^[0-9]+(\.[0-9]{1,3})$", ErrorMessage = "Valid Decimal number with maximum 3 decimal places.")]
        //[Range(0.001, 100000.000)]
        /*[Display(Name = "Sell Price")]*/
        public string M_SellPrice { get; set; }

        /* [Required(ErrorMessage = "ST_Effect required")]*/
        [DataType(DataType.Date)]
        [Display(Name = "M_ST_Effect")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]//กำหนดformatของวันที่ในการแสดงวันที่ของST_Effect
        public Nullable<System.DateTime> M_ST_Effect { get; set; }

        [Required(ErrorMessage = "Customer Part No required")]
        [Display(Name = "C_CustPNo")]
        public string C_CustPNo { get; set; }
        public string I_Status_MK { get; internal set; }
        [Required(ErrorMessage = "Material Group required")]
        public string A_Mat_Group { get; set; }
        [Required(ErrorMessage = "Part Name required")]
        public string A_Part_Name { get; set; }
        /*[Required(ErrorMessage = "Mold Owner required")]*/
        public string A_Mold_Owner { get; set; }
        [Required(ErrorMessage = "Light Type required")]
        public string A_Light_Type { get; set; }

        /*public string A_Type_No { get; set; }*/
        [Required(ErrorMessage = "Part Side required")]
        public string A_Part_Side { get; set; }
        public bool getFirst { get; set; }
        public string SellPrice_Status { get; set; }

        public string I_Approve_MK { get; set; }
        public Nullable<decimal> C_STD { get; set; }
        public string STD_Status { get; set; }

        public string Car_Type { get; set; }



    }

    public partial class SaleAdmin
    {
        public string I_RegID { get; set; }
        public string M_Model { get; set; }
        public string M_Lineoff { get; set; }
        public string M_MatNo { get; set; }
        public string M_MatDesc { get; set; }
        [Required(ErrorMessage = "Material Group required")]
        [Display(Name = "A_Mat_Group")]
        public string A_Mat_Group { get; set; }

        [Required(ErrorMessage = "Part Name required")]
        [Display(Name = "A_Part_Name")]
        public string A_Part_Name { get; set; }

        [Required(ErrorMessage = "Mold Owner required")]
        [Display(Name = "A_Mold_Owner")]
        public string A_Mold_Owner { get; set; }

        [Required]
        public string A_Light_Type { get; set; }

        [Required(ErrorMessage = "Type No required")]
        [Display(Name = "A_Type_No")]
        public string A_Type_No { get; set; }

        [Required(ErrorMessage = "Part Side required")]
        [Display(Name = "A_Part_Side")]
        public string A_Part_Side { get; set; }
        public string I_Status_Admin { get; internal set; }
    }

    public partial class Costing
    {
        public string I_RegID { get; set; }
        public string M_Model { get; set; }
        public string M_Lineoff { get; set; }
        public string M_MatNo { get; set; }
        public string M_MatDesc { get; set; }
        
        [RegularExpression(@"^[0-9]+(\.[0-9]{1,2})?$", ErrorMessage = "Valid Decimal number with maximum 2 decimal places.")]
        [Range(0.01, 900000000.00)]
        [Display(Name = "Amount")]
        public Nullable<decimal> C_STD { get; set; }
        public string I_Status_CO { get; internal set; }
        public string reson { get; set; }

        public string STD_Status { get; internal set; }
        public string I_Approve_CO { get; set; }
        [Required]
        public string M_Remark { get; set; }

    }

    public partial class Logistic
    {
        public string I_RegID { get; set; }
        public string M_Model { get; set; }
        public string M_Lineoff { get; set; }
        public string M_MatNo { get; set; }
        public string M_MatDesc { get; set; }

        [Required(ErrorMessage = "Box Qty. required")]
        [Display(Name = "L_BoxQty")]
        public Nullable<decimal> L_BoxQty { get; set; }
        public string I_Status_LG { get; internal set; }
        public string I_Approve_LG { get; set; }
        [Required]
        public string M_Remark { get; set; }
        public int OEM { get; set; }
        public Nullable<int> Part_Center { get; set; }
        public int Export { get; set; }
        public Nullable<int> ComExport { get; set; }

        public Boolean L_BoxQtyType { get; set; }
    }

    public partial class CustomerService
    {
        public string I_RegID { get; set; }
        public string M_Model { get; set; }
        public string M_Lineoff { get; set; }
        public string M_MatNo { get; set; }
        public string M_MatDesc { get; set; }
        public string I_Status_CS { get; internal set; }
        [Required(ErrorMessage = "CustID required")]
        [Display(Name = "M_CustID")]
        public string M_CustID { get; set; }

        public string M_CustName { get; set; }

        public string M_SoldTo { get; set; }

        public string M_Channel { get; set; }

        public string M_Division { get; set; }

        [Required(ErrorMessage = "ShipTo required")]
        [Display(Name = "C_ShipTo")]
        public string C_ShipTo { get; set; }

    }

    public partial class Purchase
    {
        public string I_RegID { get; set; }
        public string M_Model { get; set; }
        public string M_Lineoff { get; set; }
        public string M_MatNo { get; set; }
        public string M_MatDesc { get; set; }
        public string I_Status_PC { get; internal set; }
        /* [Required(ErrorMessage = "Must be two digits numbers")]
         [RegularExpressionAttribute(@"^[0-9]{2,2}$", ErrorMessage = "Must be two digits numbers")]*/
        public string P_Process { get; set; }
        /*[Required(ErrorMessage = "Spec required")]
        [Display(Name = "P_Spec")]*/
        public string P_Spec { get; set; }
        [Required(ErrorMessage = "KB No required")]
        [Display(Name = "P_KBNo")]
        public string P_KBNo { get; set; }
        public string P_Kb2 { get; set; }
        public string P_TkcPName { get; set; }

        [Required(ErrorMessage = "Factory No required")]
        [Display(Name = "P_FacNo")]
        public string P_FacNo { get; set; }

        public string M_TKCCustPNo { get; set; }
        public string I_Approve_PPC { get; set; }
        [Required]
        public string M_Remark { get; set; }

    }

    public partial class Sellprice
    {
        public string I_RegID { get; set; }

        [Required(ErrorMessage = "Seq required")]
        [Display(Name = "Seq")]
        public string Seq { get; set; }

        [Required(ErrorMessage = "Package required")]
        [Display(Name = "Package")]
        public string Package { get; set; }

        [Required(ErrorMessage = "Price required")]
        [Display(Name = "Price")]
        [RegularExpression(@"^[0-9]+(\.[0-9]{1,2})$", ErrorMessage = "Valid Decimal number with maximum 2 decimal places.")]
        [Range(0.01, 100000.00)]
        public string Price { get; set; }

        [Required(ErrorMessage = "Date required")]
        [DataType(DataType.Date)]
        [Display(Name = "ST_EFFECT")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",
           ApplyFormatInEditMode = true)]
        public string ST_EFFECT { get; set; }

        public string C_CustPNo { get; set; }
    }
    public partial class ShipToMk
    {
        public string seq { get; set; }
        public string I_RegID { get; set; }
        public string Cust_ID { get; set; }
        public string Plant { get; set; }
        public string CustomerName { get; set; }
        public string CustomerID { get; set; }
        public string SoldTo { get; set; }
        public string Channel { get; set; }
        public string Division { get; set; }
        public string ShipTo { get; set; }
        public string SLoc { get; set; }
        public Decimal? Amount { get; set; }
        public string Currency { get; set; }

    }
    public partial class RPT
    {
        public string I_ITEM_CD { get; set; }

        public string PLANT { get; set; }

        public string SELL_TYPE { get; set; }

        public string RPT_GROUP { get; set; }

        public string ADD_MNTH { get; set; }

    }


    public static class Utilities
    {
        public static string IsActive(this HtmlHelper html,
                                      string action,
                                      string control)
        {
            var routeData = html.ViewContext.RouteData;

            var routeAction = (string)routeData.Values["action"];
            var routeControl = (string)routeData.Values["controller"];

            // both must match
            var returnActive = control == routeControl &&
                               action == routeAction;

            return returnActive ? "active" : "";
        }
    }
}

