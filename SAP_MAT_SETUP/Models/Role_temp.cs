﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAP_MAT_SETUP.Models
{
    public class Role_temp
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public virtual ICollection<SAP_MAT_SETUP.Users.Users> Users { get; set; }
    }
}