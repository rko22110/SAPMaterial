﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAP_MAT_SETUP.Models
{
    public class User_tmp
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public Guid ActivationCode { get; set; }
        public virtual ICollection<SAP_MAT_SETUP.Users.Roles> Roles { get; set; }

    }
}