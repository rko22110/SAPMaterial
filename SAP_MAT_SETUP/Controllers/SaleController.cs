﻿using ClosedXML.Excel;
using SAP_MAT_SETUP.Database;
using SAP_MAT_SETUP.Models;
using SAP_MAT_SETUP.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAP_MAT_SETUP.CustomAuthentication
{
   
    public class SaleController : Controller
    {
        // GET: Sale
        string _RegId, errorMsg;

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult DelMat(string id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var items = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == id).FirstOrDefault();
            if (items != null)
            {
                db.T_SAP_MAT_SETUP.Remove(items);
                db.SaveChanges();
            }
            //ViewBag.plant = items;
            return Json(new { result = "Success !!", message = "request successfully" });
        }

        public ActionResult MTS()
        {
            TKC_MASTER_SAPEntities db = new TKC_MASTER_SAPEntities();

            var name = db.T_SAP_DATA_TO_SAP.ToList();
            List<ListDropDown> Name = new List<ListDropDown>();
            foreach (var item in name.Where(x => x.flag == 1).OrderBy(x => x.DATA_NAME).Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.DATA_NAME;
                iTmp.iValue = item.DATA_SOURCE;
                Name.Add(iTmp);
            }
            ViewBag.name = Name;

            var model = db.T_SAP_MAT_SETUP.OrderBy(x => x.M_Model).ToList();
            List<ListDropDown> Model = new List<ListDropDown>();
            var disModel = model.Select(x => x.M_Model).Distinct().ToList();
            foreach (var item in disModel)
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item;
                iTmp.iValue = item;
                Model.Add(iTmp);
            }
            ViewBag.model = Model;
            return View();
        }

        public async System.Threading.Tasks.Task<JsonResult> ExcelCM(string Model,string Datatype) //////////cust mat
        {
            var db = new TKC_MASTER_SAPEntities();
            var conn = db.Database.Connection;
            //var data = db.v_sap_mat_to_sap.ToList();
            var dt = new DataTable();
            var dtupd = new DataTable();
            var fileName = User.Identity.Name + "_CUSTOMER MATERIAL" + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                await conn.OpenAsync();
                DbDataReader reader;

                var _Data = db.T_SAPMAT_SELLING_PLANT1.ToList(); //ใช้ foreach วนแล้วใช้ if เช็คว่า exported != 1 ,count_exp >=1

                //ใช้exported(dt)
                using (var command = conn.CreateCommand())
                {
                    string sql = "SELECT * FROM v_sap_cust_mat_to_sap WHERE Model = '" + Model + "' ORDER BY SoldTo , ShipTo , Material1 ASC";
                    command.CommandText = sql;
                    command.CommandTimeout = 0;
                    reader = await command.ExecuteReaderAsync();
                    dt.Load(reader);
                    reader.Close();
                }
                //ใช้update(dtupd)
                using (var command = conn.CreateCommand())
                {
                    string sql = "SELECT  * FROM v_sap_cust_mat_to_sap WHERE Model = '" + Model + "' and exported is null ";
                    command.CommandText = sql;
                    command.CommandTimeout = 0;

                    reader = await command.ExecuteReaderAsync();
                    dtupd.Load(reader);
                    reader.Close();
                }
                string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);
                FileStream file = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
                try
                {
                    //Create column and inser rows
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        /* var ws = wb.Worksheets.Add(dt, "ALL");*/ //สร้าง worksheet
                        var SoldTo = dt.DefaultView.ToTable(true, "SoldTo");

                        //แยก Sheet ตาม SoldTo
                        foreach (DataRow item in SoldTo.Rows)
                        {
                            var result = dt.AsEnumerable();

                            DataTable dtResult = result.CopyToDataTable();

                            DataRow[] results = dt.Select("SoldTo = '" + item[0].ToString() + "'");
                            dtResult.Rows.Clear();
                            foreach (DataRow row in results)
                            {
                                dtResult.ImportRow(row);
                            }
                            if (dtResult.Rows.Count > 0)
                            {
                                var ws = wb.Worksheets.Add(dtResult, "SoldTo" + item[0].ToString());
                            }
                        }


                        /* var ws = wb.Worksheets.Add(dt, "ALL"); //สร้าง worksheet*/
                        /* ws = wb.Worksheets.Add("Sheet1");*/
                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(file);
                            file.Close();
                        }
                    }
                }
                catch (Exception e)
                {
                    return Json(new { fileName = "", errorMessage = "" });
                }

                if (dtupd.Rows.Count > 0)
                {
                    foreach (DataRow item in dtupd.Rows)
                    {
                        /*foreach (DataColumn col in dtupd.Columns)
                        {*/

                        string _id = item["I_RegID"].ToString();
                        string soldto = item["SoldTo"].ToString();
                        string shipto = item["ShipTo"].ToString();

                        var sapmatSellingPlant = (from c in db.T_SAPMAT_SELLING_PLANT1 where c.I_RegID == _id select c).ToList();
                        if (sapmatSellingPlant.Count > 0)
                        {
                            foreach (var data in sapmatSellingPlant)
                            {
                                data.Exported = "1";
                                data.ExpBy = User.Identity.Name;
                                data.ExpDate = DateTime.Now;
                            }

                            db.SaveChanges();
                        }

                        //updateSapmatSellingPlant(_id, db);
                        //var upd = db.T_SAPMAT_SELLING_PLANT1.Where(x => x.I_RegID == _id).FirstOrDefault();

                        //if (upd != null)
                        //{
                        //    upd.Exported = "1";
                        //    upd.ExpBy = User.Identity.Name;
                        //    upd.ExpDate = DateTime.Now;
                        //}

                        /* }*/
                        /* T_SAP_MAT_SETUP_LOG inslog = new T_SAP_MAT_SETUP_LOG();
                         inslog.UserId = User.Identity.Name;
                         inslog.FieldData = "Cust Mat";
                         inslog.Old = "";
                         inslog.New = Model;
                         inslog.UpDateTime = DateTime.Now.ToString();

                         db.T_SAP_MAT_SETUP_LOG.Add(inslog);
                         var j = db.SaveChanges();*/
                    }

                    //db.SaveChanges();
                }

                var user = new AuthenticationDB();
                var getupdate = User.Identity.Name;
                var getUserRole = user.Users.Where(x => x.Username == getupdate).FirstOrDefault();
                var Save = new T_SAPMAT_REVISE_LOG();
                Save.i_Reg_id = Model;
                Save.page = "Export " + Datatype;
                Save.RevNo = 0;
                Save.updDept = getUserRole.Roles.FirstOrDefault().RoleFullName.ToString();
                Save.updby = User.Identity.Name;
                Save.updDate = DateTime.Now;
                db.T_SAPMAT_REVISE_LOG.Add(Save);
                //upd.Count_Exp = upd.Count_Exp+1;//นับรอบในการexp
                var i = db.SaveChanges();
                return Json(new { isValid = true, fileName = fileName, result = "export data success !!", message = "export data success !!" });
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message.ToString();
                return Json(new { isValid = false, fileName = "", result = "export data failed !!", message = errorMsg });
            }
            finally
            {
                conn.Close();
            }
            //return Json(new { fileName = fileName, errorMessage = "" });
        }
        public async System.Threading.Tasks.Task<JsonResult> ExcelMM(string Model,string Datatype) //////////mat master
        {
            var db = new TKC_MASTER_SAPEntities();
            var conn = db.Database.Connection;
            //var data = db.v_sap_mat_to_sap.ToList();
            var dt = new DataTable();
            var fileName = User.Identity.Name + "_MATERIAL MASTER" + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                await conn.OpenAsync();
                DbDataReader reader;
                using (var command = conn.CreateCommand())
                {
                    string sql = "SELECT  * FROM v_sap_mat_to_sap WHERE Model = '" + Model + "' ";
                    command.CommandText = sql;
                    command.CommandTimeout = 0;

                    //DbParameter param1 = command.CreateParameter();
                    //param1.ParameterName = "@PROD_DATE";
                    //param1.Value = models.PRD_DATE.ToString("yyyy-MM-dd");
                    //command.Parameters.Add(param1);

                    //DbParameter param3 = command.CreateParameter();
                    //param3.ParameterName = "@LINE";
                    //param3.Value = models.LINE;
                    //command.Parameters.Add(param3);

                    reader = await command.ExecuteReaderAsync();
                    dt.Load(reader);
                    reader.Close();
                }
                string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);
                FileStream file = new FileStream(fullPath, FileMode.Create, FileAccess.Write);

                //Create column and inser rows
                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add(dt, "DataExport");
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(file);
                        file.Close();
                    }

                    var mm = db.T_SAP_MAT_SETUP.Where(x => x.I_Approve_MK != null
                    && x.I_Approve_PPC != null && x.I_Approve_LG != null && x.I_Approve_CO != null
                    && x.Mat_Exported == null).ToList();

                    foreach (var item in mm)
                    {
                        item.Mat_Exported = "1";
                        item.Mat_ExpBy = User.Identity.Name;
                        item.Mat_ExpDate = DateTime.Now;
                        var i = db.SaveChanges();
                    }

                    T_SAP_MAT_SETUP_LOG inslog = new T_SAP_MAT_SETUP_LOG();
                    inslog.UserId = User.Identity.Name;
                    inslog.FieldData = "Mat MS";
                    inslog.Old = "";
                    inslog.New = Model;
                    inslog.UpDateTime = DateTime.Now.ToString();
                    db.T_SAP_MAT_SETUP_LOG.Add(inslog);

                    var user = new AuthenticationDB();
                    var getupdate = User.Identity.Name;
                    var getUserRole = user.Users.Where(x => x.Username == getupdate).FirstOrDefault();
                    var Save = new T_SAPMAT_REVISE_LOG();
                    Save.i_Reg_id = Model;
                    Save.page = "Export " + Datatype;
                    Save.RevNo = 0;
                    Save.updDept = getUserRole.Roles.FirstOrDefault().RoleFullName.ToString();
                    Save.updby = User.Identity.Name;
                    Save.updDate = DateTime.Now;
                    db.T_SAPMAT_REVISE_LOG.Add(Save);
                    var j = db.SaveChanges();
                }

                return Json(new { isValid = true, fileName = fileName, result = "export data success !!", message = "export data success !!" });
            }
            catch (Exception ex)
            {
                //throw;
                errorMsg = ex.Message.ToString();
                return Json(new { isValid = false, fileName = "", result = "export data failed !!", message = errorMsg });
            }
            finally
            {
                conn.Close();
            }
            //return Json(new { fileName = fileName, errorMessage = "" });
        }
        public async System.Threading.Tasks.Task<JsonResult> ExcelSP(string Model,string Datatype) //////////sell price with package
        {
            var db = new TKC_MASTER_SAPEntities();
            var conn = db.Database.Connection;
            //var data = db.v_sap_mat_to_sap.ToList();
            var dt = new DataTable();

            var fileName = User.Identity.Name + "_SELLING PRICE With PACKAGE" + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                await conn.OpenAsync();
                DbDataReader reader;
                using (var command = conn.CreateCommand())
                {
                    string sql = "SELECT  * FROM v_sap_price_package_to_sap WHERE Model = '" + Model + "' ";
                    command.CommandText = sql;
                    command.CommandTimeout = 0;

                    //DbParameter param1 = command.CreateParameter();
                    //param1.ParameterName = "@PROD_DATE";
                    //param1.Value = models.PRD_DATE.ToString("yyyy-MM-dd");
                    //command.Parameters.Add(param1);

                    //DbParameter param3 = command.CreateParameter();
                    //param3.ParameterName = "@LINE";
                    //param3.Value = models.LINE;
                    //command.Parameters.Add(param3);

                    reader = await command.ExecuteReaderAsync();
                    dt.Load(reader);
                    reader.Close();
                }
                string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);
                FileStream file = new FileStream(fullPath, FileMode.Create, FileAccess.Write);

                //Create column and inser rows
                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add(dt, "Selling Price With Package");
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(file);
                        file.Close();
                    }

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            _RegId = row["I_RegID"].ToString();

                            var sellingPlant = db.T_SAP_MAT_SETUP.Where(c => c.I_RegID == _RegId).FirstOrDefault();

                            if (sellingPlant != null)
                            {
                                sellingPlant.Price_Exported = "1";
                                sellingPlant.Price_ExpBy = User.Identity.Name;
                                sellingPlant.Price_ExpDate = DateTime.Now;
                            }

                            //updateSapmatSetup(_RegId, db);
                            //updateSapmatSellingPlant(_RegId, db);
                        }

                        db.SaveChanges();
                    }

                    //var sp = db.T_SAPMAT_PRICE_PACK1.Where(x => x.Exported == null ).ToList();

                    //foreach (var item in sp)
                    //{
                    //    item.Exported = "1";
                    //    item.ExpBy = User.Identity.Name;
                    //    item.ExpDate = DateTime.Now;
                    //    var i = db.SaveChanges();
                    //}

                    T_SAP_MAT_SETUP_LOG inslog = new T_SAP_MAT_SETUP_LOG();
                    inslog.UserId = User.Identity.Name;
                    inslog.FieldData = "Price Pack";
                    inslog.Old = "";
                    inslog.New = Model;
                    inslog.UpDateTime = DateTime.Now.ToString();

                    db.T_SAP_MAT_SETUP_LOG.Add(inslog);

                    var user = new AuthenticationDB();
                    var getupdate = User.Identity.Name;
                    var getUserRole = user.Users.Where(x => x.Username == getupdate).FirstOrDefault();
                    var Save = new T_SAPMAT_REVISE_LOG();
                    Save.i_Reg_id = Model;
                    Save.page = "Export " + Datatype;
                    Save.RevNo = 0;
                    Save.updDept = getUserRole.Roles.FirstOrDefault().RoleFullName.ToString();
                    Save.updby = User.Identity.Name;
                    Save.updDate = DateTime.Now;
                    db.T_SAPMAT_REVISE_LOG.Add(Save);
                    var j = db.SaveChanges();
                }
                return Json(new { isValid = true, fileName = fileName, result = "export data success !!", message = "export data success !!" });
            }
            catch (Exception ex)
            {
                //throw;
                errorMsg = ex.Message.ToString();
                return Json(new { isValid = false, fileName = "", result = "export data failed !!", message = errorMsg });
            }
            finally
            {
                conn.Close();
            }
            //return Json(new { fileName = fileName, errorMessage = "" });
        }

        private void updateSapmatSellingPlant(string regId, TKC_MASTER_SAPEntities db)
        {
            var sapmatSellingPlant = (from c in db.T_SAPMAT_SELLING_PLANT1 where c.I_RegID == regId select c).ToList();
            if (sapmatSellingPlant.Count > 0)
            {
                foreach (var item in sapmatSellingPlant)
                {
                    item.Exported = "1";
                    item.ExpBy = User.Identity.Name;
                    item.ExpDate = DateTime.Now;
                }
            }

            db.SaveChanges();
        }

        private void updateSapmatSetup(string regId, TKC_MASTER_SAPEntities db)
        {
            var sellingPlant = db.T_SAP_MAT_SETUP.Where(c => c.I_RegID == regId).FirstOrDefault();
            
            if (sellingPlant != null)
            {
                sellingPlant.Price_Exported = "1";
                sellingPlant.Price_ExpBy = User.Identity.Name;
                sellingPlant.Price_ExpDate = DateTime.Now;
            }
        }

        public async System.Threading.Tasks.Task<JsonResult> ExcelSPNP(string Model,string Datatype) //////////sell price no package
        {
            var db = new TKC_MASTER_SAPEntities();
            var conn = db.Database.Connection;
            //var data = db.v_sap_mat_to_sap.ToList();
            var dt = new DataTable();
            var fileName = User.Identity.Name + "_SELLING PRICE NO PACKAGE" + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                await conn.OpenAsync();
                DbDataReader reader;
                using (var command = conn.CreateCommand())
                {
                    string sql = "SELECT  * FROM v_sap_price_no_package_to_sap WHERE Model = '" + Model + "' ";
                    command.CommandText = sql;
                    command.CommandTimeout = 0;

                    //DbParameter param1 = command.CreateParameter();
                    //param1.ParameterName = "@PROD_DATE";
                    //param1.Value = models.PRD_DATE.ToString("yyyy-MM-dd");
                    //command.Parameters.Add(param1);

                    //DbParameter param3 = command.CreateParameter();
                    //param3.ParameterName = "@LINE";
                    //param3.Value = models.LINE;
                    //command.Parameters.Add(param3);

                    reader = await command.ExecuteReaderAsync();
                    dt.Load(reader);
                    reader.Close();
                }
                string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);
                FileStream file = new FileStream(fullPath, FileMode.Create, FileAccess.Write);

                //Create column and inser rows
                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add(dt, "Selling Price None Package");
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(file);
                        file.Close();
                    }

                    // var sp = db.T_SAP_MAT_SETUP.Where(x => x.I_Approve_MK != null
                    //&& x.I_Approve_PPC != null && x.I_Approve_LG != null && x.I_Approve_CO != null
                    //&& x.Price_Exported == null).ToList();

                    // foreach (var item in sp)
                    // {
                    //     item.Price_Exported = "1";
                    //     item.Price_ExpBy = User.Identity.Name;
                    //     item.Price_ExpDate = DateTime.Now;
                    //     var i = db.SaveChanges();
                    // }
                    
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            _RegId = row["I_RegID"].ToString();

                            var sellingPlant = db.T_SAP_MAT_SETUP.Where(c => c.I_RegID == _RegId).FirstOrDefault();

                            if (sellingPlant != null)
                            {
                                sellingPlant.Price_Exported = "1";
                                sellingPlant.Price_ExpBy = User.Identity.Name;
                                sellingPlant.Price_ExpDate = DateTime.Now;
                            }

                            //updateSapmatSetup(_RegId, db);
                            //updateSapmatSellingPlant(_RegId, db);

                            //var sellingPlant = db.T_SAP_MAT_SETUP.Where(c => c.I_RegID == _RegId).FirstOrDefault();
                            //var sapmatSellingPlant = (from c in db.T_SAPMAT_SELLING_PLANT1 where c.I_RegID == _RegId select c).ToList();

                            //if (sellingPlant != null)
                            //{
                            //    sellingPlant.Price_Exported = "1";
                            //    sellingPlant.Price_ExpBy = User.Identity.Name;
                            //    sellingPlant.Price_ExpDate = DateTime.Now;

                            //    sellingPlant.Mat_Exported = "1";
                            //    sellingPlant.Mat_ExpBy = User.Identity.Name;
                            //    sellingPlant.Mat_ExpDate = DateTime.Now;
                            //}

                            //if (sapmatSellingPlant.Count > 0)
                            //{
                            //    foreach (var item in sapmatSellingPlant)
                            //    {
                            //        item.Exported = "1";
                            //        item.ExpBy = User.Identity.Name;
                            //        item.ExpDate = DateTime.Now;
                            //    }
                            //}

                        }

                        db.SaveChanges();
                    }

                    T_SAP_MAT_SETUP_LOG inslog = new T_SAP_MAT_SETUP_LOG();
                    inslog.UserId = User.Identity.Name;
                    inslog.FieldData = "Price No Pack";
                    inslog.Old = "";
                    inslog.New = Model;
                    inslog.UpDateTime = DateTime.Now.ToString();

                    db.T_SAP_MAT_SETUP_LOG.Add(inslog);

                    var user = new AuthenticationDB();
                    var getupdate = User.Identity.Name;
                    var getUserRole = user.Users.Where(x => x.Username == getupdate).FirstOrDefault();
                    var Save = new T_SAPMAT_REVISE_LOG();
                    Save.i_Reg_id = Model;
                    Save.page = "Export " + Datatype;
                    Save.RevNo = 0;
                    Save.updDept = getUserRole.Roles.FirstOrDefault().RoleFullName.ToString();
                    Save.updby = User.Identity.Name;
                    Save.updDate = DateTime.Now;
                    db.T_SAPMAT_REVISE_LOG.Add(Save);
                    var j = db.SaveChanges();
                }

                return Json(new { isValid = true, fileName = fileName, result = "export data success !!", message = "export data success !!" });
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
            //return Json(new { fileName = fileName, errorMessage = "" });
        }
        public async System.Threading.Tasks.Task<JsonResult> ExcelMMZFG(string Model,string Datatype) //////////mat master
        {
            var db = new TKC_MASTER_SAPEntities();
            var conn = db.Database.Connection;
            //var data = db.v_sap_mat_to_sap.ToList();
            var dt = new DataTable();
            var fileName = User.Identity.Name + "_MATERIAL MASTER ZFG" + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                await conn.OpenAsync();
                DbDataReader reader;
                using (var command = conn.CreateCommand())
                {
                    string sql = "SELECT  * FROM V_SAP_MAT_ZFG_TO_SAP WHERE Model = '" + Model + "' ";
                    command.CommandText = sql;
                    command.CommandTimeout = 0;

                    //DbParameter param1 = command.CreateParameter();
                    //param1.ParameterName = "@PROD_DATE";
                    //param1.Value = models.PRD_DATE.ToString("yyyy-MM-dd");
                    //command.Parameters.Add(param1);

                    //DbParameter param3 = command.CreateParameter();
                    //param3.ParameterName = "@LINE";
                    //param3.Value = models.LINE;
                    //command.Parameters.Add(param3);

                    reader = await command.ExecuteReaderAsync();
                    dt.Load(reader);
                    reader.Close();
                }
                string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);
                FileStream file = new FileStream(fullPath, FileMode.Create, FileAccess.Write);

                //Create column and inser rows
                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add(dt, "ZFG");
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(file);
                        file.Close();
                    }

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            _RegId = row["I_RegID"].ToString();

                            var sellingPlant = db.T_SAP_MAT_SETUP.Where(c => c.I_RegID == _RegId).FirstOrDefault();

                            if (sellingPlant != null)
                            {
                                sellingPlant.Mat_Exported = "1";
                                sellingPlant.Mat_ExpBy = User.Identity.Name;
                                sellingPlant.Mat_ExpDate = DateTime.Now;
                            }

                        }

                        db.SaveChanges();
                    }

                    //var mm = db.T_SAP_MAT_SETUP.Where(x => x.I_Approve_MK != null
                    //&& x.I_Approve_PPC != null && x.I_Approve_LG != null && x.I_Approve_CO != null
                    //&& x.Mat_Exported == null).ToList();

                    //foreach (var item in mm)
                    //{
                    //    item.Mat_Exported = "1";
                    //    item.Mat_ExpBy = User.Identity.Name;
                    //    item.Mat_ExpDate = DateTime.Now;
                    //    var i = db.SaveChanges();
                    //}
                    /* T_SAP_MAT_SETUP_LOG inslog = new T_SAP_MAT_SETUP_LOG();
                     inslog.UserId = User.Identity.Name;
                     inslog.FieldData = "Mat MS ZFG";
                     inslog.Old = "";
                     inslog.New = Model;
                     inslog.UpDateTime = DateTime.Now.ToString();

                     db.T_SAP_MAT_SETUP_LOG.Add(inslog);
                    */
                    var user = new AuthenticationDB();
                    var getupdate = User.Identity.Name;
                    var getUserRole = user.Users.Where(x => x.Username == getupdate).FirstOrDefault();
                    var Save = new T_SAPMAT_REVISE_LOG();
                    Save.i_Reg_id = Model;
                    Save.page = "Export " + Datatype;
                    Save.RevNo = 0;
                    Save.updDept = getUserRole.Roles.FirstOrDefault().RoleFullName.ToString();
                    Save.updby = User.Identity.Name;
                    Save.updDate = DateTime.Now;
                    db.T_SAPMAT_REVISE_LOG.Add(Save);
                    var j = db.SaveChanges();
                    

                }

                return Json(new { isValid = true, fileName = fileName, result = "export data success !!", message = "export data success !!" });
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message.ToString();
                return Json(new { isValid = false, fileName = "", result = "export data failed !!", message = errorMsg });
                //throw;
            }
            finally
            {
                conn.Close();
            }
            
        }

        public async System.Threading.Tasks.Task<JsonResult> ExcelMMZRM(string Model,string Datatype) //////////mat master
        {
            var db = new TKC_MASTER_SAPEntities();
            var conn = db.Database.Connection;
            //var data = db.v_sap_mat_to_sap.ToList();
            var dt = new DataTable();
            var fileName = User.Identity.Name + "_MATERIAL MASTER ZRM" + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                await conn.OpenAsync();
                DbDataReader reader;
                using (var command = conn.CreateCommand())
                {
                    string sql = "SELECT  * FROM V_SAP_MAT_ZRM_TO_SAP WHERE Model = '" + Model + "' ";
                    command.CommandText = sql;
                    command.CommandTimeout = 0;

                    //DbParameter param1 = command.CreateParameter();
                    //param1.ParameterName = "@PROD_DATE";
                    //param1.Value = models.PRD_DATE.ToString("yyyy-MM-dd");
                    //command.Parameters.Add(param1);

                    //DbParameter param3 = command.CreateParameter();
                    //param3.ParameterName = "@LINE";
                    //param3.Value = models.LINE;
                    //command.Parameters.Add(param3);

                    reader = await command.ExecuteReaderAsync();
                    dt.Load(reader);
                    reader.Close();
                }
                string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);
                FileStream file = new FileStream(fullPath, FileMode.Create, FileAccess.Write);

                //Create column and inser rows
                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add(dt, "ZRM");
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(file);
                        file.Close();
                    }

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            _RegId = row["I_RegID"].ToString();

                            var sellingPlant = db.T_SAP_MAT_SETUP.Where(c => c.I_RegID == _RegId).FirstOrDefault();

                            if (sellingPlant != null)
                            {
                                sellingPlant.Mat_Exported = "1";
                                sellingPlant.Mat_ExpBy = User.Identity.Name;
                                sellingPlant.Mat_ExpDate = DateTime.Now;
                            }

                        }

                        db.SaveChanges();
                    }

                    //var mm = db.T_SAP_MAT_SETUP.Where(x => x.I_Approve_MK != null
                    //&& x.I_Approve_PPC != null && x.I_Approve_LG != null && x.I_Approve_CO != null
                    //&& x.Mat_Exported == null).ToList();

                    //foreach (var item in mm)
                    //{
                    //    item.Mat_Exported = "1";
                    //    item.Mat_ExpBy = User.Identity.Name;
                    //    item.Mat_ExpDate = DateTime.Now;
                    //    var i = db.SaveChanges();
                    //}
                    /* T_SAP_MAT_SETUP_LOG inslog = new T_SAP_MAT_SETUP_LOG();
                     inslog.UserId = User.Identity.Name;
                     inslog.FieldData = "Mat MS ZRM";
                     inslog.Old = "";
                     inslog.New = Model;
                     inslog.UpDateTime = DateTime.Now.ToString();

                     db.T_SAP_MAT_SETUP_LOG.Add(inslog);*/
                    var user = new AuthenticationDB();
                    var getupdate = User.Identity.Name;
                    var getUserRole = user.Users.Where(x => x.Username == getupdate).FirstOrDefault();
                    var Save = new T_SAPMAT_REVISE_LOG();
                    Save.i_Reg_id = Model;
                    Save.page = "Export " + Datatype;
                    Save.RevNo = 0;
                    Save.updDept = getUserRole.Roles.FirstOrDefault().RoleFullName.ToString();
                    Save.updby = User.Identity.Name;
                    Save.updDate = DateTime.Now;
                    db.T_SAPMAT_REVISE_LOG.Add(Save);
                    var j = db.SaveChanges();
                }

                return Json(new { isValid = true, fileName = fileName, result = "export data success !!", message = "export data success !!" });
            }
            catch (Exception ex)
            {
                //throw;
                errorMsg = ex.Message.ToString();
                return Json(new { isValid = false, fileName = "", result = "export data failed !!", message = errorMsg });

            }
            finally
            {
                conn.Close();
            }
            //return Json(new { fileName = fileName, errorMessage = "" });
        }

        public async System.Threading.Tasks.Task<JsonResult> ExcelMMZTD(string Model,string Datatype) //////////mat master
        {
            var db = new TKC_MASTER_SAPEntities();
            var conn = db.Database.Connection;
            //var data = db.v_sap_mat_to_sap.ToList();
            var dt = new DataTable();
            var fileName = User.Identity.Name + "_MATERIAL MASTER ZTD" + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                await conn.OpenAsync();
                DbDataReader reader;
                using (var command = conn.CreateCommand())
                {
                    string sql = "SELECT  * FROM V_SAP_MAT_ZTD_TO_SAP WHERE Model = '" + Model + "' ";
                    command.CommandText = sql;
                    command.CommandTimeout = 0;

                    //DbParameter param1 = command.CreateParameter();
                    //param1.ParameterName = "@PROD_DATE";
                    //param1.Value = models.PRD_DATE.ToString("yyyy-MM-dd");
                    //command.Parameters.Add(param1);

                    //DbParameter param3 = command.CreateParameter();
                    //param3.ParameterName = "@LINE";
                    //param3.Value = models.LINE;
                    //command.Parameters.Add(param3);

                    reader = await command.ExecuteReaderAsync();
                    dt.Load(reader);
                    reader.Close();
                }
                string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);
                FileStream file = new FileStream(fullPath, FileMode.Create, FileAccess.Write);

                //Create column and inser rows
                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add(dt, "ZTD");
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(file);
                        file.Close();
                    }

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            _RegId = row["I_RegID"].ToString();

                            var sellingPlant = db.T_SAP_MAT_SETUP.Where(c => c.I_RegID == _RegId).FirstOrDefault();

                            if (sellingPlant != null)
                            {
                                sellingPlant.Mat_Exported = "1";
                                sellingPlant.Mat_ExpBy = User.Identity.Name;
                                sellingPlant.Mat_ExpDate = DateTime.Now;
                            }

                        }

                        db.SaveChanges();
                    }

                    //var mm = db.T_SAP_MAT_SETUP.Where(x => x.I_Approve_MK != null
                    //&& x.I_Approve_PPC != null && x.I_Approve_LG != null && x.I_Approve_CO != null
                    //&& x.Mat_Exported == null).ToList();

                    //foreach (var item in mm)
                    //{
                    //    item.Mat_Exported = "1";
                    //    item.Mat_ExpBy = User.Identity.Name;
                    //    item.Mat_ExpDate = DateTime.Now;
                    //    var i = db.SaveChanges();
                    //}
                    /* T_SAP_MAT_SETUP_LOG inslog = new T_SAP_MAT_SETUP_LOG();
                     inslog.UserId = User.Identity.Name;
                     inslog.FieldData = "Mat MS ZTD";
                     inslog.Old = "";
                     inslog.New = Model;
                     inslog.UpDateTime = DateTime.Now.ToString();

                     db.T_SAP_MAT_SETUP_LOG.Add(inslog);*/

                    var user = new AuthenticationDB();
                     var getupdate = User.Identity.Name;
                     var getUserRole = user.Users.Where(x => x.Username == getupdate).FirstOrDefault();
                     var Save = new T_SAPMAT_REVISE_LOG();
                     Save.i_Reg_id = Model;
                     Save.page = "Export" + Datatype;
                     Save.RevNo = 0;
                     Save.updDept = getUserRole.Roles.FirstOrDefault().RoleFullName.ToString();
                     Save.updby = User.Identity.Name;
                     Save.updDate = DateTime.Now;
                     db.T_SAPMAT_REVISE_LOG.Add(Save);
                     var j = db.SaveChanges();
                }

                return Json(new { isValid = true, fileName = fileName, result = "export data success !!", message = "export data success !!" });
            }
            catch (Exception ex)
            {
                //throw;
                errorMsg = ex.Message.ToString();
                return Json(new { isValid = false, fileName = "", result = "export data failed !!", message = errorMsg });
            }
            finally
            {
                conn.Close();
            }
            //return Json(new { fileName = fileName, errorMessage = "" });
        }

        [HttpGet]
        [DeleteFileAttribute] //Action Filter, it will auto delete the file after download, 
                              //I will explain it later
        public ActionResult Download(string file)
        {
            //get the temp folder and file path in server
            string fullPath = Path.Combine(Server.MapPath("~/temp"), file);

            //return the file for download, this is an Excel 
            //so I set the file content type to "application/vnd.ms-excel"
            return File(fullPath, "application/vnd.ms-excel", file);
        }

        public class DeleteFileAttribute : ActionFilterAttribute
        {
            public override void OnResultExecuted(ResultExecutedContext filterContext)
            {
                filterContext.HttpContext.Response.Flush();
                //convert the current filter context to file and get the file path
                string filePath = (filterContext.Result as FilePathResult).FileName;
                //delete the file after download
                System.IO.File.Delete(filePath);
            }
        }

        public JsonResult ExcelDisplay() //save file
        {
            var db = new TKC_MASTER_SAPEntities();
            //////////////////////////////////////////////////// เปลี่ยน table กับเงื่อนไขที่ใช้ในการดึง
            var _Data = db.v_sap_mat_setup_ng.ToList();
            foreach (var item in _Data)
            {

                var splitline = item.Line_Off.Split('-');
                var lineoff = Convert.ToDateTime(splitline[0].ToString() + "-" + splitline[1].ToString() + "-01");
                var currentLineoff = DateTime.Now;
                var Endlineoff = lineoff.AddDays(+30);
                var s_lineoff = (lineoff.Date - currentLineoff.Date).Days;
                item.Line_Off = s_lineoff.ToString();
            }
            var data = _Data.Where(x => Convert.ToInt32(x.Line_Off) <= 62).ToList();
            //////////////////////////////////////////////////////////////////////////////////////////
            var fileName = User.Identity.Name + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);
            FileStream file = new FileStream(fullPath, FileMode.Create, FileAccess.Write);

            //Create column and inser rows
            using (XLWorkbook wb = new XLWorkbook())
            {
                var ws = wb.Worksheets.Add(ConvertToDataTable(data), "DataExport");
                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(file);
                    file.Close();
                }
            }
            return Json(new { fileName = fileName, errorMessage = "" });
        }

        public DataTable ConvertToDataTable<T>(IList<T> data) //เอา data เข้า table
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));

            DataTable table = new DataTable();

            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);

            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }
                table.Rows.Add(row);
            }
            return table;
        }

        [CustomAuthorize(Roles = "MK,MgrMK,SaleAdmin,MgrSaleAdmin")]
        public ActionResult AddCFPricePack()
        {
            var db = new TKC_MASTEREntities();
            var data = db.T_SAP_Price_Package.ToList();
            return View(data);
        }

        public JsonResult insCFPricePack(T_SAP_Price_Package model)
        {
            string msg = "";
            var db = new TKC_MASTEREntities();
            TKC_MASTEREntities dbcontext = new TKC_MASTEREntities();
            var _front = "";
            var CheckPk_CD = dbcontext.T_SAP_Price_Package.Where(x => x.Pk_CD.Substring(0, 1) == x.Pk_CD.Substring(0, 1))
                                                               .OrderByDescending(y => y.Pk_CD).FirstOrDefault();
            try
            {
                string Runno = "";
                if (CheckPk_CD == null)
                {
                    Runno = "A" + "00";
                }
                else
                {
                    _front = CheckPk_CD.Pk_CD.Substring(0, 1).ToString();
                    var sumno = Convert.ToInt32(CheckPk_CD.Pk_CD.Substring(1, 2).ToString()) + 1;
                    if (sumno > 99)
                    {
                        int _ascii = char.ConvertToUtf32(_front, 0);
                        char character = (char)(_ascii + 1);
                        _front = character.ToString();
                        Runno = _front + "00";
                    }
                    else if (sumno <= 99)
                    {
                        _front = CheckPk_CD.Pk_CD.Substring(0, 1).ToString();
                        Runno = _front + sumno.ToString("00");
                    }
                }
                T_SAP_Price_Package insMat = new T_SAP_Price_Package();

                insMat.Master_Price_Confirm_No = model.Master_Price_Confirm_No;
                if (model.Master_Price_Confirm_No.Length < 15)
                {
                    insMat.Pk_CD = model.Master_Price_Confirm_No;
                }
                else
                {
                    insMat.Pk_CD = Runno;
                }
                insMat.UpdBy = User.Identity.Name;
                insMat.UpdDate = DateTime.Now;
                //model.Channel = "10"; สามารถfixedค่าได้
                dbcontext.T_SAP_Price_Package.Add(insMat);
                var i = dbcontext.SaveChanges();
                msg = "Success!!";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                msg = "error !!";
            }
            return Json(new { result = msg, message = "request successfully" });
        }

        [CustomAuthorize(Roles = "MK,MgrMK,SaleAdmin,MgrSaleAdmin")]
        public ActionResult AddConfirm()
        {
            var db = new TKC_MASTEREntities();
            var data = db.T_SAP_Price_Package.ToList();
            return View();
        }

        [CustomAuthorize(Roles = "SaleAdmin,MgrSaleAdmin,CS")]
        public ActionResult ShipTo()//ชื่อหน้า
        {
            var db = new TKC_MASTER_SAPEntities();
            var ship = db.T_SAPMAT_PLANT.ToList();
            return View(ship);
        }

        public JsonResult DelShipTo(string id)//ฟังก์ชั่น delete shipto
        {
            var db = new TKC_MASTER_SAPEntities();
            var items = db.T_SAPMAT_PLANT.Where(x => x.CUSTID == id).FirstOrDefault();
            if (items != null)
            {
                db.T_SAPMAT_PLANT.Remove(items);
                db.SaveChanges();
            }
            return Json(new { result = "Success !!", message = "request successfully" });
        }
        
        public ActionResult AddShipTo()//ชื่อหน้า
        {
            return View();
        }

        [HttpPost]
        public JsonResult insShipTo(T_SAPMAT_PLANT infos)//ฟังก์ชั่น insert data to sql
        {
            string msg = "";
            var db = new TKC_MASTER_SAPEntities();
            TKC_MASTER_SAPEntities dbcontext = new TKC_MASTER_SAPEntities();
            try
            {
                T_SAPMAT_PLANT insMat = new T_SAPMAT_PLANT();

                insMat.CUSTID = infos.CUSTID;
                insMat.SOLD_TO = infos.SOLD_TO;
                insMat.PLANT = infos.PLANT;
                insMat.DIVISION = infos.DIVISION;
                insMat.CHANNEL = infos.CHANNEL;
                insMat.SHIP_TO = infos.SHIP_TO;
                insMat.CUST_NAME = infos.CUST_NAME;
                insMat.Str_Loc = infos.Str_Loc;

                //model.Channel = "10"; สามารถfixedค่าได้
                dbcontext.T_SAPMAT_PLANT.Add(insMat);
                var i = dbcontext.SaveChanges();
                msg = "Success!!";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                msg = "error !!";
            }
            return Json(new { result = msg, message = "request successfully" });
        }

        public ActionResult EditShipTo(string Id)//ชื่อหน้า
        {
            var db = new TKC_MASTER_SAPEntities();
            var list = db.T_SAPMAT_PLANT.Where(x => x.CUSTID == Id).FirstOrDefault();

            T_SAPMAT_PLANT tmp = new T_SAPMAT_PLANT();
            tmp.CUSTID = list.CUSTID;
            tmp.CUST_NAME = list.CUST_NAME;
            tmp.SOLD_TO = list.SOLD_TO;
            tmp.DIVISION = list.DIVISION;
            tmp.CHANNEL = list.CHANNEL;
            tmp.SHIP_TO = list.SHIP_TO;
            tmp.PLANT = list.PLANT;
            tmp.Str_Loc = list.Str_Loc;

            return View(tmp);
        }

        public JsonResult updateShipTo(T_SAPMAT_PLANT model)//ฟังก์ชั่น update data to sql
        {
            try
            {
                var db = new TKC_MASTER_SAPEntities();
                var Data = db.T_SAPMAT_PLANT.Where(x => x.CUSTID == model.CUSTID).FirstOrDefault();

                if (Data != null)
                {
                    Data.PLANT = model.PLANT;
                    Data.SOLD_TO = model.SOLD_TO;
                    Data.SHIP_TO = model.SHIP_TO;
                    Data.Str_Loc = model.Str_Loc;
                    Data.CUST_NAME = model.CUST_NAME;
                    Data.CHANNEL = model.CHANNEL;
                    Data.DIVISION = model.DIVISION;
                   
                    var i = db.SaveChanges();
                }
                else
                {
                    return Json(new { result = "no data !!", message = "request successfully" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "error", message = "request successfully" });
            }
            return Json(new { result = "insert success !!", message = "request successfully" });
        }

        public ActionResult Model()
        {
            var db = new TKC_MASTER_SAPEntities();
            var Custms = db.T_SAPMAT_Model.ToList();
            return View(Custms);
        }

        public JsonResult DelModel(string id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var items = db.T_SAPMAT_Model.Where(x => x.CustCD == id).FirstOrDefault();
            if (items != null)
            {
                db.T_SAPMAT_Model.Remove(items);
                db.SaveChanges();
            }
            //ViewBag.plant = items;
            return Json(new { result = "Success !!", message = "request successfully" });
        }

        public ActionResult AddModel(string Id)//ดึง data ขึ้นมา show
        {
            var db = new TKC_MASTER_SAPEntities();
            var cust = db.T_SAPMAT_Cust_MS.ToList();
            List<ListDropDown> Customer = new List<ListDropDown>();
            foreach (var item in cust.OrderBy(x => x.CustName).Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.CustName;
                iTmp.iValue = item.CustCD;
                Customer.Add(iTmp);
            }
            ViewBag.cust = Customer;

            return View();
        }

        [HttpPost]
        public JsonResult insModel(T_SAPMAT_Model infos)
        {
            string msg = "";
            var db = new TKC_MASTER_SAPEntities();
            TKC_MASTER_SAPEntities dbcontext = new TKC_MASTER_SAPEntities();
            try
            {
                T_SAPMAT_Model insMat = new T_SAPMAT_Model();

                insMat.Model = infos.Model;
                insMat.CustCD = infos.CustCD;
                dbcontext.T_SAPMAT_Model.Add(insMat);
                var i = dbcontext.SaveChanges();
                msg = "Success!!";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                msg = "error !!";
            }
            return Json(new { result = msg, message = "request successfully" });
        }

        public JsonResult updateModel(T_SAPMAT_Model model) // update data ลง sql
        {
            try
            {
                var db = new TKC_MASTER_SAPEntities();
                var Data = db.T_SAPMAT_Model.Where(x => x.CustCD == model.CustCD).FirstOrDefault();

                if (Data != null)
                {
                    Data.Model = model.Model;
                    Data.CustCD = model.CustCD;

                    var i = db.SaveChanges();
                }
                else
                {
                    return Json(new { result = "no data !!", message = "request successfully" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "error", message = "request successfully" });
            }
            return Json(new { result = "insert success !!", message = "request successfully" });
        }

        public ActionResult NExport()
        {
            var db = new TKC_MASTER_SAPEntities();
            var MatList = db.v_sapmat_nexport.Where(x => x.Mat_Exported == null || x.Cust_Exported == null || x.Pack_Exported == null || x.Price_Exported == null).OrderByDescending(c => c.I_RegID).ToList();

            ViewBag.export = MatList;
            return View();
        }
    }
}