﻿using SAP_MAT_SETUP.CustomAuthentication;
using SAP_MAT_SETUP.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SAP_MAT_SETUP.Users;
using SAP_MAT_SETUP.Database;
using System.Data;

namespace SAP_MAT_SETUP.Controllers
{
    public class MaterialMasterView
    {
        public string I_RegID { get; set; }
        public string M_CustPName { get; set; }
        public string M_Remark { get; set; }

        public string M_Model { get; set; }

        public string M_Lineoff { get; set; }

        public string M_MatNo { get; set; }

        public string C_CustPNo { get; set; }

        public string I_Approve_MK { get; set; }

        public string I_Approve_PPC { get; set; }

        public string I_Approve_LG { get; set; }
        public string P_FacNo { get; set; }
        public string I_Approve_CO { get; set; }

        public string CUSTOMER { get; set; }

        public string Plant { get; set; }

        public string Car_Type { get; set; }

        public string Status_Approve_MK { get; set; }

        public string Status_Approve_PPC { get; set; }

        public string Status_Approve_LG { get; set; }

        public string Status_Approve_CO { get; set; }
    }

    public class UserLogView
    {
        public int LogNo { get; set; }
        public string page { get; set; }
        public string i_Reg_id { get; set; }
        public string M_MatNo { get; set; }
        public string M_Model { get; set; }
        public string updDept { get; set; }
        public string updby { get; set; }
        public DateTime updDate { get; set; }
    }

    [AllowAnonymous]
    public class AccountController : Controller
    {
        DataTable tblData;
        DateTime dateLineCutOff, dateNow, dateExpCutOff;
        string lineCutOff;
        public static List<MaterialMasterView> matList;

        // GET: Account
        public ActionResult Index(string ApproveStatus)
        {
            var db = new TKC_MASTER_SAPEntities();
            matList = getDataTableMaterialMaster();

            var user = new AuthenticationDB();

            var userName = User.Identity.Name;
            var getUserRole = user.Users.Where(x => x.Username == userName).FirstOrDefault();
            string RoleName = getUserRole.Roles.FirstOrDefault().RoleName.ToString();
            ViewBag.ApproveStatus = ApproveStatus;

            if (RoleName == "MK" || RoleName == "MgrMK")
            {
                if (ApproveStatus == "OK")
                {
                    matList = matList.Where(c => c.I_Approve_MK != "").ToList();
                }

                else if (ApproveStatus == "NG")
                {
                    matList = matList.Where(c => c.Status_Approve_MK == "NG").ToList();
                }

                else if (ApproveStatus == "ONSCH")
                {
                    matList = matList.Where(c => c.Status_Approve_MK == "On Schedule").ToList();
                }
            }

            else if (RoleName == "PPC")
            {
                if (ApproveStatus == "OK")
                {
                    matList = matList.Where(c => c.I_Approve_PPC != "").ToList();
                }

                else if (ApproveStatus == "NG")
                {
                    matList = matList.Where(c => c.Status_Approve_PPC == "NG").ToList();
                }
                
                else if (ApproveStatus == "ONSCH")
                {
                    matList = matList.Where(c => c.Status_Approve_PPC == "On Schedule").ToList();
                }
            }

            else if (RoleName == "LG")
            {
                if (ApproveStatus == "OK")
                {
                    matList = matList.Where(c => c.I_Approve_LG != "").ToList();
                }

                else if (ApproveStatus == "NG")
                {
                    matList = matList.Where(c => c.Status_Approve_LG == "NG").ToList();
                }

                else if (ApproveStatus == "ONSCH")
                {
                    matList = matList.Where(c => c.Status_Approve_LG == "On Schedule").ToList();
                }
            }

            else if (RoleName == "CO" || RoleName == "PU")
            {
                if (ApproveStatus == "OK")
                {
                    matList = matList.Where(c => c.I_Approve_CO != "").ToList();
                }

                else if (ApproveStatus == "NG")
                {
                    matList = matList.Where(c => c.Status_Approve_CO == "NG").ToList();
                }
                
                else if (ApproveStatus == "ONSCH")
                {
                    matList = matList.Where(c => c.Status_Approve_CO == "On Schedule").ToList();
                }
            }

            else if (RoleName.Contains("Admin"))
            {
                if (ApproveStatus == "OK")
                {
                    matList = matList.Where(c => (c.I_Approve_MK != "") && (c.I_Approve_PPC != "") && (c.I_Approve_LG != "") && (c.I_Approve_CO != "")).ToList();
                }

                else if (ApproveStatus == "NG")
                {
                    //matList = matList.Where(c => (c.I_Approve_MK == "") || (c.I_Approve_PPC == "") || (c.I_Approve_LG == "") || (c.I_Approve_CO == "")).ToList();
                    matList = matList.Where(c => (c.Status_Approve_MK == "NG") || (c.Status_Approve_PPC == "NG") || (c.Status_Approve_LG == "NG") || (c.Status_Approve_CO == "NG")).ToList();
                }

                else if (ApproveStatus == "ONSCH")
                {
                    matList = matList.Where(c => (c.Status_Approve_MK == "On Schedule") || (c.Status_Approve_PPC == "On Schedule") || (c.Status_Approve_LG == "On Schedule") || (c.Status_Approve_CO == "On Schedule")).ToList();
                }
            }


            return View(matList);
        }

        private List<MaterialMasterView> getDataTableMaterialMaster()
        {
            matList = new List<MaterialMasterView>();

            string cmd = "select t.I_RegID,t.M_CustPName,t.M_Remark,t.M_Model,t.M_Lineoff,t.M_MatNo,t.C_CustPNo,t.I_Approve_MK," +
                "t.I_Approve_PPC,t.I_Approve_LG,t.P_FacNo,t.I_Approve_CO,t.CUSTOMER, s.Plant, t.Car_Type " +
                "from T_SAP_MAT_SETUP t left join T_SAPMAT_SELLING_PLANT1 s on t.I_RegID = s.I_RegID " +
                "group by t.I_RegID,t.M_CustPName,t.M_Remark,t.M_Model,t.M_Lineoff,t.M_MatNo,t.C_CustPNo,t.I_Approve_MK,t.I_Approve_PPC," +
                "t.I_Approve_LG,t.P_FacNo,t.I_Approve_CO,t.CUSTOMER, s.Plant, t.Car_Type " +
                "order by t.I_RegID desc";

            tblData = SqlData.SelectSqlData(cmd);

            if (tblData.Rows.Count > 0)
            {
                foreach (DataRow row in tblData.Rows)
                {
                    MaterialMasterView data = new MaterialMasterView();

                    lineCutOff = row["M_Lineoff"].ToString();

                    if (!string.IsNullOrEmpty(lineCutOff))
                    {
                        dateLineCutOff = Convert.ToDateTime(row["M_Lineoff"].ToString());
                    }


                    dateNow = DateTime.Now;

                    data.I_RegID = row["I_RegID"].ToString();
                    data.M_CustPName = row["M_CustPName"].ToString();
                    data.M_Remark = row["M_Remark"].ToString();
                    data.M_Model = row["M_Model"].ToString();
                    data.M_Lineoff = row["M_Lineoff"].ToString();
                    data.M_MatNo = row["M_MatNo"].ToString();
                    data.C_CustPNo = row["C_CustPNo"].ToString();
                    data.I_Approve_MK = row["I_Approve_MK"].ToString();
                    data.I_Approve_PPC = row["I_Approve_PPC"].ToString();
                    data.I_Approve_LG = row["I_Approve_LG"].ToString();
                    data.P_FacNo = row["P_FacNo"].ToString();
                    data.I_Approve_CO = row["I_Approve_CO"].ToString();
                    data.CUSTOMER = row["CUSTOMER"].ToString();
                    data.Plant = row["Plant"].ToString();
                    data.Car_Type = row["Car_Type"].ToString();

                    if (!string.IsNullOrEmpty(lineCutOff))
                    {
                        data.Status_Approve_PPC = GetStatusApprove(data.I_Approve_PPC, data.Car_Type, dateLineCutOff);
                        data.Status_Approve_LG = GetStatusApprove(data.I_Approve_LG, data.Car_Type, dateLineCutOff);
                    }

                    data.Status_Approve_MK = GetStatusApproveNGOK(data.I_Approve_MK);
                    data.Status_Approve_CO = GetStatusApproveNGOK(data.I_Approve_CO);
                    matList.Add(data);
                }
            }

            return matList;
        }

        private string GetStatusApproveNGOK(string approveStatus)
        {
            string statusApprove = string.Empty;
            if (string.IsNullOrEmpty(approveStatus))
            {
                statusApprove = "NG";
            }
            return statusApprove;
        }

        private string GetStatusApprove(string approveStatus, string carType, DateTime dateLineCutOff)
        {
            string statusApprove = string.Empty;

            if (string.IsNullOrEmpty(approveStatus))
            {
                //รถ 2 ล้อ
                if (carType == "W2")
                {
                    dateExpCutOff = dateLineCutOff.AddMonths(-4);

                    //อยู่ในช่วง 4 เดือนก่อน L/ O
                    if (dateNow < dateExpCutOff)
                    {
                        if (dateNow.Year < dateExpCutOff.Year)
                        {
                            statusApprove = "NG";
                        }

                        else
                        {
                            statusApprove = "On Schedule";
                        }
                    }

                    else
                    {
                        statusApprove = "NG";
                    }
                }

                //รถ 4 ล้อ หรือมากกว่า 4 ล้อ
                else if (carType == "W4")
                {
                    dateExpCutOff = dateLineCutOff.AddMonths(-6);

                    //อยู่ในช่วง 6 เดือนก่อน L/O
                    if (dateNow < dateExpCutOff)
                    {
                        if (dateNow.Year < dateExpCutOff.Year)
                        {
                            statusApprove = "NG";
                        }

                        else
                        {
                            statusApprove = "On Schedule";
                        }
                    }

                    else
                    {
                        statusApprove = "NG";
                    }
                }

                else
                {
                    dateExpCutOff = dateLineCutOff.AddMonths(-6);

                    //อยู่ในช่วง 4 เดือนก่อน L/ O
                    if (dateNow < dateExpCutOff)
                    {
                        if (dateNow.Year < dateExpCutOff.Year)
                        {
                            statusApprove = "NG";
                        }

                        else
                        {
                            statusApprove = "On Schedule";
                        }
                    }

                    else
                    {
                        statusApprove = "NG";
                    }
                }
            }

            return statusApprove;
        }

        public ActionResult UserLog() {
            var db = new TKC_MASTER_SAPEntities();
            var log = db.v_sapmat_revise_log.OrderByDescending(x=>x.updDate).ToList();
            return View(log);
        }
        public ActionResult NGList()
        {
            var db = new TKC_MASTER_SAPEntities();
            var user = new AuthenticationDB();
            List<T_SAP_MAT_SETUP> ngList = new List<T_SAP_MAT_SETUP>();

            var userName = User.Identity.Name;
            var getUserRole = user.Users.Where(x => x.Username == userName).FirstOrDefault();
            string RoleName = getUserRole.Roles.FirstOrDefault().RoleName.ToString();
            

            if (RoleName == "MK" || RoleName == "MgrMK")
            {
                ngList = (from c in db.T_SAP_MAT_SETUP
                             where c.I_Approve_MK == null
                             select c).OrderByDescending(c => c.I_RegID).ToList();
            }

            else if (RoleName == "PPC")
            {
                ngList = (from c in db.T_SAP_MAT_SETUP
                          where c.I_Approve_PPC == null
                          select c).OrderByDescending(c => c.I_RegID).ToList();
            }

            else if (RoleName == "LG")
            {
                ngList = (from c in db.T_SAP_MAT_SETUP
                          where c.I_Approve_LG == null
                          select c).OrderByDescending(c => c.I_RegID).ToList();
            }

            else if (RoleName == "CO" || RoleName == "PU")
            {
                ngList = (from c in db.T_SAP_MAT_SETUP
                          where c.I_Approve_CO == null
                          select c).OrderByDescending(c => c.I_RegID).ToList();
            }


            //var RegID = db.T_SAP_MAT_SETUP.Where(x => x.I_Approve_MK == null).OrderByDescending(c => c.I_RegID).ToList();
            return View(ngList);
        }


        [HttpGet]
        public ActionResult Login(string ReturnUrl = "")
        {
            if (User.Identity.IsAuthenticated)
            {
                return LogOut();
            }
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginView loginView, string ReturnUrl = "")
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(loginView.UserName, loginView.Password))
                {
                    var user = (CustomMembershipUser)Membership.GetUser(loginView.UserName, false);
                    if (user != null)
                    {
                        CustomSerializeModel userModel = new CustomSerializeModel()
                        {
                            UserId = user.UserId,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            RoleName = user.Roles.Select(r => r.RoleName).ToList()
                        };

                        string userData = JsonConvert.SerializeObject(userModel);
                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                            (
                            1, loginView.UserName, DateTime.Now, DateTime.Now.AddMinutes(15), false, userData
                            );

                        string enTicket = FormsAuthentication.Encrypt(authTicket);
                        HttpCookie faCookie = new HttpCookie("SAPMAT", enTicket);
                        Response.Cookies.Add(faCookie);
                    }

                    if (Url.IsLocalUrl(ReturnUrl))
                    {
                        return Redirect(ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                }
            }
            ModelState.AddModelError("", "Something Wrong : Username or Password invalid ^_^ ");
            return View(loginView);
        }

        [HttpGet]
        public ActionResult Registration()
        {
            var db = new AuthenticationDB();
            var ComboRoles = db.Roles.Where(x => x.RoleId != 1).ToList();
            List<ListDropDown> Roleuser = new List<ListDropDown>();
            foreach (var item in ComboRoles)
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iValue = item.RoleName;
                iTmp.iKey = item.RoleName;
                Roleuser.Add(iTmp);
            }
            ViewBag.RolesCombo = Roleuser.ToList(); //.OrderBy(x => x.iValue)
            return View();
        }

        public JsonResult CopyMat(string MatNo)
        {
            var db = new TKC_MASTER_SAPEntities();
            var Material = db.T_SAP_MAT_SETUP.Where(x => x.M_MatNo == MatNo).Distinct()
                .FirstOrDefault();
            //.OrderBy(x => x.iValue)
            return Json(new { result = Material, message = "request successfully" });
        }


        [HttpPost]
        public ActionResult Registration(RegistrationView registrationView)
        {

            var db = new AuthenticationDB();
            var ComboRoles = db.Roles.Where(x => x.RoleId != 1).ToList();
            List<ListDropDown> Roleuser = new List<ListDropDown>();
            foreach (var item in ComboRoles)
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iValue = item.RoleName;
                iTmp.iKey = item.RoleName;
                Roleuser.Add(iTmp);
            }
            ViewBag.RolesCombo = Roleuser.ToList(); //.OrderBy(x => x.iValue)

            bool statusRegistration = false;
            string messageRegistration = string.Empty;

            if (ModelState.IsValid)
            {
                // Email Verification
                string userName = Membership.GetUserNameByEmail(registrationView.Email);

                if (!string.IsNullOrEmpty(userName))
                {
                    ModelState.AddModelError("Warning Email", "Sorry: Email already Exists");
                    return View(registrationView);
                }

                //Save User Data 
                using (AuthenticationDB dbContext = new AuthenticationDB())
                {

                    var user = new SAP_MAT_SETUP.Users.Users()
                    {
                        Username = registrationView.Username,
                        FirstName = registrationView.FirstName,
                        LastName = registrationView.LastName,
                        Email = registrationView.Email,
                        Password = registrationView.Password,
                        IsActive = true,
                        ActivationCode = Guid.NewGuid(),
                    };

                    dbContext.Users.Add(user);
                    dbContext.SaveChanges();

                    string[] usernames = { registrationView.Username };

                    string[] roleNames = { registrationView.Department };


                    CustomRole custom = new CustomRole();
                    custom.AddUsersToRoles(usernames, roleNames);
                }

                //Verification Email
                //VerificationEmail(registrationView.Email, registrationView.ActivationCode.ToString());
                messageRegistration = "Your account has been created successfully. ^_^";
                statusRegistration = true;
            }
            else
            {
                messageRegistration = "Something Wrong!";
            }
            ViewBag.Message = messageRegistration;
            ViewBag.Status = statusRegistration;

          


            return View(registrationView);
        }

        [HttpGet]
        public ActionResult ActivationAccount(string id)
        {
            bool statusAccount = false;
            using (AuthenticationDB dbContext = new AuthenticationDB())
            {
                var userAccount = dbContext.Users.Where(u => u.ActivationCode.ToString().Equals(id)).FirstOrDefault();

                if (userAccount != null)
                {
                    userAccount.IsActive = true;
                    dbContext.SaveChanges();
                    statusAccount = true;
                }
                else
                {
                    ViewBag.Message = "Something Wrong !!";
                }
            }
            ViewBag.Status = statusAccount;
            return View();
        }
        public ActionResult LogOut()
        {
            HttpCookie cookie = new HttpCookie("SAPMAT", "");
            cookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie);

            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account", null);
        }

        [NonAction]
        public void VerificationEmail(string email, string activationCode)
        {
            var url = string.Format("/Account/ActivationAccount/{0}", activationCode);
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, url);

            var fromEmail = new MailAddress("prones_g@tkoito.co.th", "Activation Account - TKC");
            var toEmail = new MailAddress(email);

            var fromEmailPassword = "zaq1@wsx";
            string subject = "Activation Account !";

            string body = "<br/> Please click on the following link in order to activate your account" + "<br/><a href='" + link + "'> Activation Account ! </a>";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })

                smtp.Send(message);
        }

        public JsonResult DelMat(string id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var items = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == id).FirstOrDefault();
            if (items != null)
            {
                db.T_SAP_MAT_SETUP.Remove(items);
                db.SaveChanges();
            }
            //ViewBag.plant = items;
            return Json(new { result = "Success !!", message = "request successfully" });
        }
    }
}
