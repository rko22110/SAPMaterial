﻿using SAP_MAT_SETUP.CustomAuthentication;
using SAP_MAT_SETUP.Database;
using SAP_MAT_SETUP.Models;
using SAP_MAT_SETUP.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace SAP_MAT_SETUP.Controllers
{
    public class CostController : Controller
    {
        // GET: Cost
        string errorMsg;

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult NGList()
        {
            var db = new TKC_MASTER_SAPEntities();
            var RegID = db.T_SAP_MAT_SETUP.Where(x => x.I_Status_CO == null).OrderByDescending(c => c.I_RegID).ToList();
            return View(RegID);
        }

        [CustomAuthorize(Roles = "CO,PU")]
        public ActionResult Costing(string Id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var Mat = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == Id).FirstOrDefault();
            Costing tmp = new Costing();
            tmp.I_RegID = Mat.I_RegID;
            tmp.M_Model = Mat.M_Model;
            tmp.M_Lineoff = Mat.M_Lineoff;
            tmp.M_MatNo = Mat.M_MatNo;
            tmp.M_MatDesc = Mat.M_MatDesc;
            tmp.C_STD = Mat.C_STD;
            tmp.STD_Status = Mat.STD_Status;
            tmp.reson = Mat.flag01;

            return View(tmp);
        }

        [HttpPost]
        public JsonResult updateCosting(T_SAP_MAT_SETUP model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = new AuthenticationDB();
                    var db = new TKC_MASTER_SAPEntities();
                    var Data = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == model.I_RegID).FirstOrDefault();

                    if (Data != null)
                    {
                        Data.C_STD = model.C_STD;
                        Data.I_Status_CO = "OK";
                        Data.LastUpdBy = User.Identity.Name;
                        Data.LastUpdDate = DateTime.Now;
                        Data.STD_Status = model.STD_Status;
                        if (Data.I_Approve_CO != null)
                        {
                            Data.I_Approve_CO = null;
                        }
                        Data.flag01 = model.flag01;

                        ////////////////reset Price Export//////////////////////
                        //if (Data.Price_Revise == null)
                        //{
                        //    Data.Price_Revise = 0;
                        //}
                        //if (Data.Price_Exported == "1")
                        //{
                        //    Data.Price_Exported = null;
                        //    Data.Price_Revise += 1;
                        //}
                        //else {
                        //    Data.Price_Revise = Data.Price_Revise;
                        //}
                        //Data.Price_ExpBy = null;
                        //Data.Price_ExpDate = null;
                        ////////////////reset Mat Export//////////////////////
                        Data.Mat_Exported = null;
                        Data.Mat_ExpBy = null;
                        Data.Mat_ExpDate = null;
                        //if (Data.Mat_Revise == null)
                        //{
                        //    Data.Mat_Revise = 0;
                        //}
                        //if (Data.Mat_Exported == "1")
                        //{
                        //    Data.Mat_Exported = null;
                        //    Data.Mat_Revise += 1;
                        //}
                        //else
                        //{
                        //    Data.Mat_Revise = Data.Mat_Revise;
                        //}
                        //Data.Mat_ExpBy = null;
                        //Data.Mat_ExpDate = null;
                        ////////////////////////////////////

                        var getupdate = User.Identity.Name;
                        var getUserRole = user.Users.Where(x => x.Username == getupdate).FirstOrDefault();
                        var Save = new T_SAPMAT_REVISE_LOG();
                        Save.i_Reg_id = Data.I_RegID;
                        Save.page = "Mat Master";
                        Save.RevNo = Data.Price_Revise;
                        Save.updDept = getUserRole.Roles.FirstOrDefault().RoleFullName.ToString();
                        Save.updby = User.Identity.Name;
                        Save.updDate = DateTime.Now;
                        db.T_SAPMAT_REVISE_LOG.Add(Save);

                        var i = db.SaveChanges();
                        getCOOK(model.I_RegID);
                        getPartOK(model.I_RegID);
                        List<string> _Roles = new List<string> { "CO" };
                        var getemail = user.Users.Where(x => (x.Roles.Any(y => _Roles.Contains(y.RoleName)))).ToList();

                        foreach (var items in getemail)
                        {
                            string html = "<span style='font-family:Tahoma;font-size: 13pt;color:#1E90FF;'>";
                            html += "Dear  All concerns<br>";
                            html += "Cost have update in SAP Material System.<br>";
                            html += "I_RegID " + Data.I_RegID + "<br>";
                            html += "Model " + Data.M_Model + "<br>";
                            html += "Material number " + Data.M_MatNo + "<br>";
                            html += "Please check and prepare the information that related to you within due date.<br>";
                            html += "TKC <br>";
                            html += "System Development. <br>";
                            html += "Tel. 109";
                            html += "</span>";
                            NotificationEmail(items.Email, "SAP Material System have update", html);

                        }

                        //return Json(new { result = "insert success !!", message = "request successfully" });
                        return Json(new { isValid = true, result = "insert success !!", message = "request successfully" });

                    }
                    else
                    {
                        //return Json(new { result = "no data !!", message = "request successfully" });
                        return Json(new { isValid = false, result = "no data !!", message = "no data to send email !!" });
                    }
                }
                catch (Exception ex)
                {
                    //return Json(new { result = "error", message = "request successfully" });
                    errorMsg = ex.Message.ToString();
                    return Json(new { isValid = false, result = "error !!", message = errorMsg });

                }
            }
            else
            {
                //return Json(new { result = "input data !!", message = "request successfully" });
                //return Json("Error");
                return Json(new { isValid = false, result = "error !!", message = "input data!!" });
            }
            
        }

        public void getCOOK(string TkcPart) //check status
        {
            var db = new TKC_MASTER_SAPEntities();
            var Customer = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == TkcPart)
                .FirstOrDefault();

            if (Customer.CUSTOMER != null) // true
            {
                if (Customer.C_STD != null || Customer.flag01 != null)
                {
                    Customer.I_Status_CO = "OK";
                    db.SaveChanges();
                }
                else
                {
                    Customer.I_Status_CO = "NG";
                    db.SaveChanges();
                }
            }
            else
            {
                Customer.I_Status_CO = "NG";
                db.SaveChanges();
            }
        }

        public void getPartOK(string TkcPart) //check status
        {
            var db = new TKC_MASTER_SAPEntities();
            var Customer = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == TkcPart)
                .FirstOrDefault();

            if (Customer.I_Status_MK == "OK")
            {
                if (Customer.I_Status_PC == "OK")
                {
                    if (Customer.I_Status_LG == "OK")
                    {
                        if (Customer.I_Status_CO == "OK")
                        {
                            Customer.I_Status = "OK";
                            db.SaveChanges();
                        }
                        else
                        {
                            Customer.I_Status = "NG";
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        Customer.I_Status = "NG";
                        db.SaveChanges();
                    }
                }
                else
                {
                    Customer.I_Status = "NG";
                    db.SaveChanges();
                }
            }
            else
            {
                Customer.I_Status = "NG";
                db.SaveChanges();
            }
        }
        public void NotificationEmail(string email, string subject, string body)
        {

            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress("prones_g@tkoito.co.th");
            msg.To.Add(email);
            msg.Subject = subject;
            msg.Body = body;
            msg.IsBodyHtml = true;

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Port = 25;
            smtpClient.Host = "172.18.1.2";
            smtpClient.Credentials = new NetworkCredential("prones", "zaq1@wsx");
            smtpClient.Send(msg);
        }
        public ActionResult ApproveCO()
        {
            var db = new TKC_MASTER_SAPEntities();
            var items = db.T_SAP_MAT_SETUP.Where(c => c.I_Status_CO == "OK").Where(x => x.I_Approve_CO == null).ToList();
            return View(items);
        }

        [HttpPost]
        public JsonResult updateApproveCO(List<ListDropDown> listkey)
        {
            try
            {
                using (var db = new TKC_MASTER_SAPEntities())
                {
                    foreach (var item in listkey)
                    {
                        var update = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == item.iKey).FirstOrDefault();

                        if (update != null)
                        {
                            update.I_Approve_CO = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"); ;
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "error", message = "request successfully" });
            }
            return Json(new { result = "insert success !!", message = "request successfully" });
        }
    }
}