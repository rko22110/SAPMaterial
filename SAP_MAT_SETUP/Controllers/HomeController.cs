﻿using ClosedXML.Excel;
using OfficeOpenXml;
using Oracle.ManagedDataAccess.Client;
using SAP_MAT_SETUP.CustomAuthentication;
using SAP_MAT_SETUP.Database;
using SAP_MAT_SETUP.Models;
using SAP_MAT_SETUP.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using PagedList;
using System.Web.Mvc;


namespace SAP_MAT_SETUP.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        string errorMsg;

        public JsonResult CopyMat(string MatNo)
        {
            var db = new TKC_MASTER_SAPEntities();
            var Material = db.T_SAP_MAT_SETUP.Where(x => x.M_MatNo == MatNo).Distinct()
                .FirstOrDefault();
            //.OrderBy(x => x.iValue)
            return Json(new { result = Material, message = "request successfully" });
        }

        [HttpPost]
        public JsonResult getCustName(string Customers)
        {
            var db = new TKC_MASTER_SAPEntities();
            var Customer = db.T_SAPMAT_Cust_MS.Where(x => x.CustCD == Customers).FirstOrDefault();
            if (Customer == null)
            {
                Customer = db.T_SAPMAT_Cust_MS.Where(x => x.CustName == Customers).FirstOrDefault();
            }
            //.OrderBy(x => x.iValue)
            return Json(new { result = Customer.CustName, message = "request successfully" });
        }

        [HttpPost]
        public JsonResult getKeyCustomer(string Customers)
        {
            var db = new TKC_MASTER_SAPEntities();
            var Customer = db.T_CUST_SOLD_TO.Where(x => x.CustID_SOLD_TO == Customers)
                .FirstOrDefault();
            //.OrderBy(x => x.iValue)
            return Json(new { result = Customer.CustSAP_SOLD_TO, message = "request successfully" });
        }

        public void getPartOK(string TkcPart) //check status
        {
            var db = new TKC_MASTER_SAPEntities();
            var Customer = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == TkcPart)
                .FirstOrDefault();

            if (Customer.I_Status_MK == "OK")
            {
                if (Customer.I_Status_PC == "OK")
                {
                    if (Customer.I_Status_LG == "OK")
                    {
                        if (Customer.I_Status_CO == "OK")
                        {
                            Customer.I_Status = "OK";
                            db.SaveChanges();
                        }
                        else
                        {
                            Customer.I_Status = "NG";
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        Customer.I_Status = "NG";
                        db.SaveChanges();
                    }
                }
                else
                {
                    Customer.I_Status = "NG";
                    db.SaveChanges();
                }
            }
            else
            {
                Customer.I_Status = "NG";
                db.SaveChanges();
            }
        }

        public JsonResult getNameCustomer(string Customers)
        {
            var db = new TKC_MASTER_SAPEntities();
            var Customer = db.T_CUST_SOLD_TO.Where(x => x.CustID_SOLD_TO == Customers)
                .FirstOrDefault();
            //.OrderBy(x => x.iValue)
            return Json(new { result = Customer.CustomerDesc, message = "request successfully" });
        }

        public JsonResult getChannel(string Customers)
        {
            var db = new TKC_MASTER_SAPEntities();
            var Customer = db.T_CUST_SOLD_TO.Where(x => x.CustID_SOLD_TO == Customers)
                .FirstOrDefault();
            //.OrderBy(x => x.iValue)
            return Json(new { result = Customer.Channel, message = "request successfully" });
        }

        public JsonResult getDivision(string Customers)
        {
            var db = new TKC_MASTER_SAPEntities();
            var Customer = db.T_CUST_SOLD_TO.Where(x => x.CustID_SOLD_TO == Customers)
                .FirstOrDefault();
            //.OrderBy(x => x.iValue)
            return Json(new { result = Customer.Division, message = "request successfully" });
        }

        [HttpPost]
        public JsonResult insMarketing(Marketing model)
        {
            string msg = "";
            var db = new TKC_MASTER_SAPEntities();
            TKC_MASTER_SAPEntities dbcontext = new TKC_MASTER_SAPEntities();
            try
            {
                string year = DateTime.Now.ToString("yy");
                string Month = DateTime.Now.ToString("MM");
                T_SAP_MAT_SETUP insMat = new T_SAP_MAT_SETUP();
                var CheckRegID = dbcontext.T_SAP_MAT_SETUP.Where(x => x.I_RegID.Substring(0, 2) == year
                                                                 && x.I_RegID.Substring(2, 2) == Month).OrderByDescending(y => y.I_RegID).FirstOrDefault();

                //2101 - 002
                //01234567
                //var _RunNo = CheckRegID == null ? year + Month + "001" : year + Month + "" + Convert.ToInt32(CheckRegID.I_RegID.Substring(5, 3).ToString()) + 1;
                string Runno = "";
                if (CheckRegID == null)
                {
                    Runno = year + Month + "-001";
                }
                else
                {
                    var sumno = Convert.ToInt32(CheckRegID.I_RegID.Substring(5, 3).ToString()) + 1;
                    Runno = year + Month + "-" + sumno.ToString("000");
                }
                var copy = db.T_SAP_MAT_SETUP.Where(y => y.M_MatNo == model.M_MatNo).ToList();
                if (copy.Count >= 1)
                {
                    var _copy = copy.Where(z => z.M_MatNo == model.M_MatNo).FirstOrDefault();

                    insMat.I_RegID = Runno;
                    insMat.CUSTOMER = model.CUSTOMER;
                    insMat.I_Status = "OK";
                    insMat.I_Status_MK = "OK";
                    insMat.I_Status_Admin = "OK";
                    if (model.M_Model == "Choose Model")
                    {
                        insMat.M_Model = null;
                    }
                    else
                    {
                        insMat.M_Model = model.M_Model;
                    }
                    insMat.M_Lineoff = model.M_Lineoff;
                    insMat.M_MatNo = model.M_MatNo;
                    /*insMat.M_MatDesc = model.M_MatDesc;*/
                    insMat.M_TKCCustPNo = model.C_CustPNo.Replace("-", "");
                    insMat.M_CustPName = model.M_CustPName;
                    insMat.M_SellPrice = decimal.Parse(model.M_SellPrice);
                    insMat.M_ST_Effect = model.M_ST_Effect;
                    insMat.A_Mat_Group = model.A_Mat_Group;
                    insMat.A_Part_Name = model.A_Part_Name;
                    insMat.A_Mold_Owner = model.A_Mold_Owner;
                    insMat.A_Light_Type = model.A_Light_Type;
                    /*insMat.A_Type_No = model.A_Type_No;*/
                    insMat.A_Part_Side = model.A_Part_Side;
                    insMat.M_Remark = model.M_Remark;
                    insMat.C_CustPNo = model.C_CustPNo;
                    insMat.SellPrice_Status = model.SellPrice_Status;

                    insMat.C_STD = model.C_STD;
                    insMat.STD_Status = model.STD_Status;
                    //////////////////////////
                    insMat.P_KBNo = _copy.P_KBNo;
                    insMat.P_Kb2 = _copy.P_Kb2;
                    insMat.P_FacNo = _copy.P_FacNo;
                    insMat.P_Process = _copy.P_Process;
                    insMat.P_Spec = _copy.P_Spec;
                    insMat.P_TkcPName = _copy.P_TkcPName;
                    insMat.L_BoxQty = _copy.L_BoxQty;
                    //insMat.I_Status_CO = "OK";
                    //insMat.I_Status_PC = "OK";
                    //insMat.I_Status_LG = "OK";
                    //insert updateby updatedate////
                    insMat.UpdBy = User.Identity.Name;
                    insMat.UpdDate = DateTime.Now;
                    insMat.Car_Type = model.Car_Type;
                    //////////////////////////////////////////////

                    //model.Channel = "10"; สามารถfixedค่าได้
                    dbcontext.T_SAP_MAT_SETUP.Add(insMat);
                    var i = dbcontext.SaveChanges();
                    /*if (i > 0)
                    {
                        // message dialog
                        getMKOK(insMat.I_RegID);
                        getPartOK(insMat.I_RegID);
                    }
                    else
                    {
                        // message dialog
                        getMKOK(insMat.I_RegID);
                        getPartOK(insMat.I_RegID);
                    }*/
                    msg = "Success!!";
                }
                else
                {
                    insMat.I_RegID = Runno;
                    insMat.CUSTOMER = model.CUSTOMER;
                    insMat.I_Status = "NG";
                    insMat.I_Status_MK = "NG";
                    insMat.I_Status_Admin = "OK";
                    if (model.M_Model == "Choose Model")
                    {
                        insMat.M_Model = null;
                    }
                    else
                    {
                        insMat.M_Model = model.M_Model;
                    }
                    insMat.M_Lineoff = model.M_Lineoff;
                    insMat.M_MatNo = model.M_MatNo;
                    /*insMat.M_MatDesc = model.M_MatDesc;*/
                    insMat.M_TKCCustPNo = model.C_CustPNo.Replace("-", "");
                    insMat.M_CustPName = model.M_CustPName;
                    insMat.M_SellPrice = decimal.Parse(model.M_SellPrice);
                    insMat.M_ST_Effect = model.M_ST_Effect;
                    insMat.A_Mat_Group = model.A_Mat_Group;
                    insMat.A_Part_Name = model.A_Part_Name;
                    insMat.A_Mold_Owner = model.A_Mold_Owner;
                    insMat.A_Light_Type = model.A_Light_Type;
                    /*insMat.A_Type_No = model.A_Type_No;*/
                    insMat.A_Part_Side = model.A_Part_Side;
                    insMat.M_Remark = model.M_Remark;
                    insMat.C_CustPNo = model.C_CustPNo;
                    insMat.SellPrice_Status = model.SellPrice_Status;

                    insMat.C_STD = model.C_STD;
                    insMat.STD_Status = model.STD_Status;

                    //insert updateby updatedate////
                    insMat.UpdBy = User.Identity.Name;
                    insMat.UpdDate = DateTime.Now;
                    insMat.Car_Type = model.Car_Type;
                    //////////////////////////////////////////////

                    //model.Channel = "10"; สามารถfixedค่าได้
                    dbcontext.T_SAP_MAT_SETUP.Add(insMat);
                    var i = dbcontext.SaveChanges();
                    if (i > 0)
                    {
                        // message dialog
                        getMKOK(insMat.I_RegID);
                        getPartOK(insMat.I_RegID);
                    }
                    else
                    {
                        // message dialog
                        getMKOK(insMat.I_RegID);
                        getPartOK(insMat.I_RegID);
                    }
                    msg = "Success!!";
                }
                //model.I_RegID = CheckRegID == null ? year + Month + "001" : year + Month + "" + Convert.ToInt32(CheckRegID.I_RegID.Substring(5, 3).ToString()) + 1;;

                //send email for notification//

                var dbUser = new AuthenticationDB();
                List<string> _Roles = new List<string> { "MK", "CO", "PPC", "LG", "PU" };
                var getemail = dbUser.Users.Where(x => (x.Roles.Any(y => _Roles.Contains(y.RoleName)))).ToList();

                foreach (var item in getemail)
                {
                    string html = "<span style='font-family:Tahoma;font-size: 13pt;color:#1E90FF;'>";
                    html += "Dear  All concerns<br>";
                    html += "TKC have new material was input into SAP Material System.<br>";
                    html += "Customer " + model.CUSTOMER + "<br>";
                    html += "Model " + model.M_Model + "<br>";
                    html += "Material number " + model.M_MatNo + "<br>";
                    html += "Please check and prepare the information that related to you within due date.<br>";
                    html += "TKC <br>";
                    html += "System Development. <br>";
                    html += "Tel. 109";
                    html += "</span>";
                    NotificationEmail(item.Email, "Start new material from SAP Material System", html);
                }

                return Json(new { isValid = true, result = "insert success !!", message = "request successfully" });
            }
            catch (Exception ex)
            {
                //foreach (var eve in e.EntityValidationErrors)
                //{
                //    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                //        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                //    foreach (var ve in eve.ValidationErrors)
                //    {
                //        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                //            ve.PropertyName, ve.ErrorMessage);
                //    }
                //}
                errorMsg = ex.Message.ToString();
                return Json(new { isValid = false, result = "error !!", message = errorMsg });
            }
            
        }

        [CustomAuthorize(Roles = "MK,MgrMK")]

        public ActionResult EditMarketing(string Id)
        {
            TKC_MASTER_SAPEntities db = new TKC_MASTER_SAPEntities();
            var list = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == Id).FirstOrDefault();

            Marketing tmp = new Marketing();
            tmp.I_RegID = list.I_RegID;
            tmp.CUSTOMER = list.CUSTOMER;
            tmp.M_Model = list.M_Model;
            tmp.M_Lineoff = list.M_Lineoff;
            tmp.M_MatNo = list.M_MatNo;
            /*tmp.M_MatDesc = list.M_MatDesc;*/
            tmp.M_Remark = list.M_Remark;
            /*tmp.M_TKCCustPNo = list.M_TKCCustPNo;*/
            tmp.M_CustPName = list.M_CustPName;
            tmp.M_SellPrice = list.M_SellPrice.ToString();
            tmp.M_ST_Effect = list.M_ST_Effect;
            tmp.A_Mat_Group = list.A_Mat_Group;
            tmp.A_Part_Name = list.A_Part_Name;
            tmp.A_Mold_Owner = list.A_Mold_Owner;
            tmp.A_Light_Type = list.A_Light_Type;
            /*tmp.A_Type_No = list.A_Type_No;*/
            tmp.A_Part_Side = list.A_Part_Side;
            tmp.C_CustPNo = list.C_CustPNo;
            tmp.SellPrice_Status = list.SellPrice_Status;
            /* if (tmp.I_Approve_MK != null)
             {
                 tmp.I_Approve_MK = null;
             }*/

            var cust = db.T_SAPMAT_Cust_MS.ToList();
            List<ListDropDown> Customer = new List<ListDropDown>();
            foreach (var item in cust.OrderBy(x => x.CustName).Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.CustName;
                iTmp.iValue = item.CustCD;
                Customer.Add(iTmp);
            }
            ViewBag.cust = Customer;

            var model = db.T_SAPMAT_Model.ToList();
            List<ListDropDown> Model = new List<ListDropDown>();
            foreach (var item in model.Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.Model;
                iTmp.iValue = item.Model;
                Model.Add(iTmp);
            }
            ViewBag.model = Model;

            var Pname = db.T_SAPMAT_Part_Name.ToList();
            List<ListDropDownMatG> pname = new List<ListDropDownMatG>();
            foreach (var item in Pname.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.PartNameDesc;
                iTmp.iValue = item.PartNameCD;
                pname.Add(iTmp);
            }
            ViewBag.Pname = pname;

            var partn = db.T_SAPMAT_Part_Name.ToList();
            List<ListDropDown> partname = new List<ListDropDown>();
            foreach (var item in partn.Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.PartNameCD;
                iTmp.iValue = item.PartNameDesc;
                partname.Add(iTmp);
            }
            ViewBag.partn = partname;

            var Matg = db.T_SAPMAT_Mat_Group.ToList();
            List<ListDropDownMatG> matg = new List<ListDropDownMatG>();
            foreach (var item in Matg.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.MatGroupCD;
                iTmp.iValue = item.MatGroupName;
                matg.Add(iTmp);
            }
            ViewBag.Matg = matg;

            //dropdown customer marketing////////////////////////////////////////////////////////
            var CustMS = db.T_SAPMAT_Cust_MS.ToList();
            List<ListDropDownMatG> custms = new List<ListDropDownMatG>();
            foreach (var item in CustMS.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.CustName;
                iTmp.iValue = item.CustCD;
                custms.Add(iTmp);
            }
            ViewBag.CustMS = custms;

            var Mold = db.T_SAPMAT_Mold_Owner.OrderBy(x => x.MoldOwnerName).ToList();
            List<ListDropDownMatG> mold = new List<ListDropDownMatG>();
            foreach (var item in Mold.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.MoldOwnerName;
                iTmp.iValue = item.MoldOwnerCD;
                mold.Add(iTmp);
            }
            ViewBag.Mold = mold;

            var LightT = db.T_SAPMAT_Light_Type.ToList();
            List<ListDropDownMatG> light = new List<ListDropDownMatG>();
            foreach (var item in LightT.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.LightTypeName;
                iTmp.iValue = item.LightTypeCD;
                light.Add(iTmp);
            }
            ViewBag.LightT = light;

            var Side = db.T_SAPMAT_Part_Side.ToList();
            List<ListDropDownMatG> pside = new List<ListDropDownMatG>();
            foreach (var item in Side.OrderByDescending(x => x.SideCD).Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.SideName;
                iTmp.iValue = item.SideCD;
                pside.Add(iTmp);
            }
            ViewBag.Side = pside;

            return View();
        }

        [CustomAuthorize(Roles = "MK,MgrMK")]
        public ActionResult EdtMar(string Id)
        {
            TKC_MASTER_SAPEntities db = new TKC_MASTER_SAPEntities();
            var list = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == Id).FirstOrDefault();

            Marketing tmp = new Marketing();
            tmp.I_RegID = list.I_RegID;
            tmp.CUSTOMER = list.CUSTOMER;
            tmp.M_Model = list.M_Model;
            tmp.M_Lineoff = list.M_Lineoff;
            tmp.M_MatNo = list.M_MatNo;
            /*tmp.M_MatDesc = list.M_MatDesc;*/
            tmp.M_Remark = list.M_Remark;
            /*tmp.M_TKCCustPNo = list.M_TKCCustPNo;*/
            tmp.M_CustPName = list.M_CustPName;
            tmp.M_SellPrice = list.M_SellPrice.ToString();
            tmp.M_ST_Effect = list.M_ST_Effect;
            tmp.A_Mat_Group = list.A_Mat_Group;
            tmp.A_Part_Name = list.A_Part_Name;
            tmp.A_Mold_Owner = list.A_Mold_Owner;
            tmp.A_Light_Type = list.A_Light_Type;
            /*tmp.A_Type_No = list.A_Type_No;*/
            tmp.A_Part_Side = list.A_Part_Side;
            tmp.C_CustPNo = list.C_CustPNo;
            tmp.SellPrice_Status = list.SellPrice_Status;
            tmp.Car_Type = list.Car_Type;
            /* if (tmp.I_Approve_MK != null)
             {
                 tmp.I_Approve_MK = null;
             }*/

            var cust = db.T_SAPMAT_Cust_MS.ToList();
            List<ListDropDown> Customer = new List<ListDropDown>();
            foreach (var item in cust.OrderBy(x => x.CustName).Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.CustName;
                iTmp.iValue = item.CustCD;
                Customer.Add(iTmp);
            }
            ViewBag.cust = Customer;

            var model = db.T_SAPMAT_Model.ToList();
            List<ListDropDown> Model = new List<ListDropDown>();
            foreach (var item in model.Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.Model;
                iTmp.iValue = item.Model;
                Model.Add(iTmp);
            }
            ViewBag.model = Model;

            var Pname = db.T_SAPMAT_Part_Name.ToList();
            List<ListDropDownMatG> pname = new List<ListDropDownMatG>();
            foreach (var item in Pname.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.PartNameDesc;
                iTmp.iValue = item.PartNameCD;
                pname.Add(iTmp);
            }
            ViewBag.Pname = pname;

            var partn = db.T_SAPMAT_Part_Name.ToList();
            List<ListDropDown> partname = new List<ListDropDown>();
            foreach (var item in partn.Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.PartNameCD;
                iTmp.iValue = item.PartNameDesc;
                partname.Add(iTmp);
            }
            ViewBag.partn = partname;

            var Matg = db.T_SAPMAT_Mat_Group.ToList();
            List<ListDropDownMatG> matg = new List<ListDropDownMatG>();
            foreach (var item in Matg.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.MatGroupCD;
                iTmp.iValue = item.MatGroupName;
                matg.Add(iTmp);
            }
            ViewBag.Matg = matg;

            //dropdown customer marketing////////////////////////////////////////////////////////
            var CustMS = db.T_SAPMAT_Cust_MS.ToList();
            List<ListDropDownMatG> custms = new List<ListDropDownMatG>();
            foreach (var item in CustMS.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.CustName;
                iTmp.iValue = item.CustCD;
                custms.Add(iTmp);
            }
            ViewBag.CustMS = custms;

            var Mold = db.T_SAPMAT_Mold_Owner.OrderBy(x => x.MoldOwnerName).ToList();
            List<ListDropDownMatG> mold = new List<ListDropDownMatG>();
            foreach (var item in Mold.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.MoldOwnerName;
                iTmp.iValue = item.MoldOwnerCD;
                mold.Add(iTmp);
            }
            ViewBag.Mold = mold;

            var LightT = db.T_SAPMAT_Light_Type.ToList();
            List<ListDropDownMatG> light = new List<ListDropDownMatG>();
            foreach (var item in LightT.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.LightTypeName;
                iTmp.iValue = item.LightTypeCD;
                light.Add(iTmp);
            }
            ViewBag.LightT = light;

            var Side = db.T_SAPMAT_Part_Side.ToList();
            List<ListDropDownMatG> pside = new List<ListDropDownMatG>();
            foreach (var item in Side.OrderByDescending(x => x.SideCD).Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.SideName;
                iTmp.iValue = item.SideCD;
                pside.Add(iTmp);
            }
            ViewBag.Side = pside;

            var carType = db.CarTypes.ToList();
            List<ListDropDownMatG> carList = new List<ListDropDownMatG>();
            foreach (var item in carType.OrderByDescending(x => x.Id).Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.CarType;
                iTmp.iValue = item.CarType;
                carList.Add(iTmp);
            }

            ViewBag.CarType = carList;


            return View(tmp);
        }

        [CustomAuthorize(Roles = "MK,MgrMK")]
        public ActionResult AddMarketing(string Id)
        {
            TKC_MASTER_SAPEntities db = new TKC_MASTER_SAPEntities();
            var list = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == Id).FirstOrDefault();

            Marketing tmp = new Marketing();
            /*tmp.I_RegID = list.I_RegID;*/
            tmp.CUSTOMER = list.CUSTOMER;
            tmp.M_Model = list.M_Model;
            tmp.M_Lineoff = list.M_Lineoff;
            tmp.M_MatNo = list.M_MatNo;
            /*tmp.M_MatDesc = list.M_MatDesc;*/
            tmp.M_Remark = list.M_Remark;
            /*tmp.M_TKCCustPNo = list.M_TKCCustPNo;*/
            tmp.M_CustPName = " ";
            tmp.C_CustPNo = " ";
            tmp.M_SellPrice = list.M_SellPrice.ToString();
            tmp.M_ST_Effect = list.M_ST_Effect;
            tmp.A_Mat_Group = list.A_Mat_Group;
            tmp.A_Part_Name = list.A_Part_Name;
            tmp.A_Mold_Owner = list.A_Mold_Owner;
            tmp.A_Light_Type = list.A_Light_Type;
            /*tmp.A_Type_No = list.A_Type_No;*/
            tmp.A_Part_Side = list.A_Part_Side;
            tmp.SellPrice_Status = list.SellPrice_Status;
            tmp.C_STD = list.C_STD;
            tmp.STD_Status = list.STD_Status;
            tmp.Car_Type = list.Car_Type;

            var cust = db.T_SAPMAT_Cust_MS.ToList();
            List<ListDropDown> Customer = new List<ListDropDown>();
            foreach (var item in cust.OrderBy(x => x.CustName).Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.CustName;
                iTmp.iValue = item.CustCD;
                Customer.Add(iTmp);
            }
            ViewBag.cust = Customer;

            var model = db.T_SAPMAT_Model.ToList();
            List<ListDropDown> Model = new List<ListDropDown>();
            foreach (var item in model.Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.Model;
                iTmp.iValue = item.Model;
                Model.Add(iTmp);
            }
            ViewBag.model = Model;

            var Pname = db.T_SAPMAT_Part_Name.ToList();
            List<ListDropDownMatG> pname = new List<ListDropDownMatG>();
            foreach (var item in Pname.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.PartNameDesc;
                iTmp.iValue = item.PartNameCD;
                pname.Add(iTmp);
            }
            ViewBag.Pname = pname;

            var partn = db.T_SAPMAT_Part_Name.ToList();
            List<ListDropDown> partname = new List<ListDropDown>();
            foreach (var item in partn.Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.PartNameCD;
                iTmp.iValue = item.PartNameDesc;
                partname.Add(iTmp);
            }
            ViewBag.partn = partname;

            var Matg = db.T_SAPMAT_Mat_Group.ToList();
            List<ListDropDownMatG> matg = new List<ListDropDownMatG>();
            foreach (var item in Matg.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.MatGroupCD;
                iTmp.iValue = item.MatGroupName;
                matg.Add(iTmp);
            }
            ViewBag.Matg = matg;

            //dropdown customer marketing////////////////////////////////////////////////////////
            var CustMS = db.T_SAPMAT_Cust_MS.ToList();
            List<ListDropDownMatG> custms = new List<ListDropDownMatG>();
            foreach (var item in CustMS.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.CustName;
                iTmp.iValue = item.CustCD;
                custms.Add(iTmp);
            }
            ViewBag.CustMS = custms;

            var Mold = db.T_SAPMAT_Mold_Owner.OrderBy(x => x.MoldOwnerName).ToList();
            List<ListDropDownMatG> mold = new List<ListDropDownMatG>();
            foreach (var item in Mold.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.MoldOwnerName;
                iTmp.iValue = item.MoldOwnerCD;
                mold.Add(iTmp);
            }
            ViewBag.Mold = mold;

            var LightT = db.T_SAPMAT_Light_Type.ToList();
            List<ListDropDownMatG> light = new List<ListDropDownMatG>();
            foreach (var item in LightT.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.LightTypeName;
                iTmp.iValue = item.LightTypeCD;
                light.Add(iTmp);
            }
            ViewBag.LightT = light;

            var Side = db.T_SAPMAT_Part_Side.ToList();
            List<ListDropDownMatG> pside = new List<ListDropDownMatG>();
            foreach (var item in Side.OrderByDescending(x => x.SideCD).Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.SideName;
                iTmp.iValue = item.SideCD;
                pside.Add(iTmp);
            }
            ViewBag.Side = pside;

            return View(tmp);
        }
        public ActionResult Final()
        {
            var db = new TKC_MASTER_SAPEntities();
            DateTime Start = new DateTime(2022, 6, 19);
            //Format 03-06-2021 15:21:35
            DateTime Back = DateTime.Now.AddDays(-30);

            var items = db.V_SAP_MAT_SETUP_LAST.Where(c => (c.I_Status_MK == "OK" && c.SellPrice_Status == "true") /*&& (c.i_approve_mk > back) (c.i_approve_mk > start)*/).ToList(); //'2022-06-19'

            return View(items);
        }

        [CustomAuthorize(Roles = "MK,MgrMK")]
        public ActionResult Marketing()
        {
            TKC_MASTER_SAPEntities db = new TKC_MASTER_SAPEntities();

            var cust = db.T_SAPMAT_Cust_MS.ToList();
            List<ListDropDown> Customer = new List<ListDropDown>();
            foreach (var item in cust.OrderBy(x => x.CustName).Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.CustName;
                iTmp.iValue = item.CustCD;
                Customer.Add(iTmp);
            }
            ViewBag.cust = Customer;

            var partn = db.T_SAPMAT_Part_Name.ToList();
            List<ListDropDown> partname = new List<ListDropDown>();
            foreach (var item in partn.Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.PartNameDesc;
                iTmp.iValue = item.PartNameCD;
                partname.Add(iTmp);
            }
            ViewBag.partn = partname;

            var Pname = db.T_SAPMAT_Part_Name.ToList();
            List<ListDropDownMatG> pname = new List<ListDropDownMatG>();
            foreach (var item in Pname.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.PartNameDesc;
                iTmp.iValue = item.PartNameCD;
                pname.Add(iTmp);
            }
            ViewBag.Pname = pname;

            var Matg = db.T_SAPMAT_Mat_Group.ToList();
            List<ListDropDownMatG> matg = new List<ListDropDownMatG>();
            foreach (var item in Matg.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.MatGroupName;
                iTmp.iValue = item.MatGroupCD;
                matg.Add(iTmp);
            }
            ViewBag.Matg = matg;

            var Mold = db.T_SAPMAT_Mold_Owner.OrderBy(x => x.MoldOwnerName).ToList();
            List<ListDropDownMatG> mold = new List<ListDropDownMatG>();
            foreach (var item in Mold.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.MoldOwnerName;
                iTmp.iValue = item.MoldOwnerCD;
                mold.Add(iTmp);
            }
            ViewBag.Mold = mold;

            var LightT = db.T_SAPMAT_Light_Type.ToList();
            List<ListDropDownMatG> light = new List<ListDropDownMatG>();
            foreach (var item in LightT.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.LightTypeName;
                iTmp.iValue = item.LightTypeCD;
                light.Add(iTmp);
            }
            ViewBag.LightT = light;

            var Side = db.T_SAPMAT_Part_Side.ToList();
            List<ListDropDownMatG> pside = new List<ListDropDownMatG>();
            foreach (var item in Side.OrderByDescending(x => x.SideCD).Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.SideName;
                iTmp.iValue = item.SideCD;
                pside.Add(iTmp);
            }
            ViewBag.Side = pside;

            var carType = db.CarTypes.ToList();
            List<ListDropDownMatG> carList = new List<ListDropDownMatG>();
            foreach (var item in carType.OrderByDescending(x => x.Id).Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.CarType;
                iTmp.iValue = item.CarType;
                carList.Add(iTmp);
            }

            ViewBag.CarType = carList;

            return View();
        }

        public JsonResult getModel(string custid)
        {
            TKC_MASTER_SAPEntities db = new TKC_MASTER_SAPEntities();
            var model = db.T_SAPMAT_Model.ToList();

            List<ListDropDown> Model = new List<ListDropDown>();
            foreach (var item in model.Where(x => x.CustCD == custid).Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.Model;
                iTmp.iValue = item.Model;
                Model.Add(iTmp);
            }
            ViewBag.model = Model;

            return Json(new { result = ViewBag.model, message = "request successfully" });
        }

        public JsonResult getCust(string plant)
        {
            TKC_MASTER_SAPEntities db = new TKC_MASTER_SAPEntities();
            var model = db.T_SAPMAT_PLANT.ToList();

            List<ListDropDown> cust = new List<ListDropDown>();
            foreach (var item in model.Where(x => x.PLANT == plant).Distinct().OrderBy(x => x.CUSTID))
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.CUSTID + " " + item.CUST_NAME;
                iTmp.iValue = item.CUSTID;
                cust.Add(iTmp);
            }
            ViewBag.Cust = cust;

            return Json(new { result = ViewBag.Cust, message = "request successfully" });
        }

        public JsonResult getShipToName(string M_MatNo)
        {
            TKC_MASTER_SAPEntities db = new TKC_MASTER_SAPEntities();
            var Shipto = db.V_Customer_Package.ToList();

            List<ListDropDown> Model = new List<ListDropDown>();
            foreach (var item in Shipto.Where(x => x.M_MatNo == M_MatNo).OrderBy(x => x.Cust_ID).Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.Cust_ID + " " + item.CUST_NAME;
                iTmp.iValue = item.Cust_ID;
                Model.Add(iTmp);
            }
            ViewBag.Shipto = Model;

            return Json(new { result = ViewBag.Shipto, message = "request successfully" });
        }

        public ActionResult Approve()
        {

            var db = new TKC_MASTER_SAPEntities();
            DateTime Start = new DateTime(2022, 6, 19);
            //Format 03-06-2021 15:21:35
            DateTime Back = DateTime.Now.AddDays(-30);

            var approvelist = (from c in db.V_SAP_MAT_SETUP_LAST
                               where (c.I_Status_MK == "OK" && c.I_Approve_MK == null)
                               || (c.SellPrice_Status == "false")
                               select c).OrderByDescending(c=> c.I_RegID).ToList();

            //var items = db.V_SAP_MAT_SETUP_LAST.Where(c => (c.I_Status_MK == "OK" && c.I_Approve_MK == null) || (c.SellPrice_Status == "false" && c.I_Approve_MK != null) && (c.I_Approve_MK > Back)/*(c.I_Approve_MK > start)*/).ToList(); //'2022-06-19'

            return View(approvelist);
        }


        public ActionResult SellPriceCheck()
        {
            var db = new TKC_MASTER_SAPEntities();
            var db2 = new TKC_MASTEREntities();
            /* var items = db.T_SAPMAT_PRICE_PACK.Where(x => x.I_RegID == Id).OrderBy(y => y.Seq).ToList();
             ViewBag.sell = items;
             ViewBag.I_RegID = Id;*/

            var name = db2.T_SAP_Price_Package.ToList();
            List<ListDropDown> Name = new List<ListDropDown>();
            foreach (var item in name)
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.Master_Price_Confirm_No;
                iTmp.iValue = item.Pk_CD;
                Name.Add(iTmp);
            }
            ViewBag.name = Name;

            var sell = db.T_SAPMAT_PRICE_PACK1.ToList();
            if (sell != null)
            {
                ViewBag.sell = sell;
            }
            return View();
        }

        public JsonResult insSellPrice(T_SAPMAT_PRICE_PACK1 model)
        {
            string msg = "";
            var db = new TKC_MASTER_SAPEntities();
            TKC_MASTER_SAPEntities dbcontext = new TKC_MASTER_SAPEntities();
            try
            {
                T_SAPMAT_PRICE_PACK1 insMat = new T_SAPMAT_PRICE_PACK1();

                insMat.I_RegID = model.I_RegID;
                insMat.Seq = model.Seq;
                insMat.Package = model.Package;
                insMat.Price = model.Price;
                insMat.ST_EFFECT = model.ST_EFFECT;
                insMat.UpdBy = User.Identity.Name;
                insMat.UpdDate = DateTime.Now;
                insMat.SellPrice_Status = model.SellPrice_Status;

                //model.Channel = "10"; สามารถfixedค่าได้
                dbcontext.T_SAPMAT_PRICE_PACK1.Add(insMat);
                var i = dbcontext.SaveChanges();
                msg = "Success!!";

            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                msg = "error !!";
            }
            return Json(new { result = msg, message = "request successfully" });
        }

        public JsonResult updateSellPrice(T_SAPMAT_PRICE_PACK1 model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var db = new TKC_MASTER_SAPEntities();
                    var Data = db.T_SAPMAT_PRICE_PACK1.Where(x => x.I_RegID == model.I_RegID && x.Seq == model.Seq).FirstOrDefault();

                    if (Data != null)
                    {
                        Data.Seq = model.Seq;
                        Data.Package = model.Package;
                        Data.Price = model.Price;
                        Data.ST_EFFECT = model.ST_EFFECT;
                        Data.ExpBy = null;
                        Data.Exported = null;
                        Data.ExpDate = null;
                        //db.T_SAPMAT_PRICE_PACK.Attach(Data);
                        var i = db.SaveChanges();
                    }
                    else
                    {
                        return Json(new { result = "no data !!", message = "request successfully" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { result = "error", message = "request successfully" });
                }
            }
            else
            {
                return Json(new { result = "input data !!", message = "request successfully" });
            }
            return Json(new { result = "insert success !!", message = "request successfully" });
        }

        public ActionResult ShipToPlant()
        {

            TKC_MASTER_SAPEntities db = new TKC_MASTER_SAPEntities();

            var shipto = db.T_SAPMAT_PLANT.ToList();
            List<ListDropDown> ShipTo = new List<ListDropDown>();
            foreach (var item in shipto.OrderBy(x => x.CUST_NAME))
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.CUSTID + " " + item.CUST_NAME;
                iTmp.iValue = item.CUSTID;
                ShipTo.Add(iTmp);
            }
            ViewBag.shipto = ShipTo;

            var plant = db.T_SAPMAT_SELLING_PLANT1.ToList();
            if (plant != null)
            {
                ViewBag.MyPlant = plant;
            }
            return View();
        }

        public JsonResult getShipToPlant(string RegID)
        {
            TKC_MASTER_SAPEntities db = new TKC_MASTER_SAPEntities();
            var plant = db.T_SAPMAT_SELLING_PLANT1.Where(x => x.I_RegID == RegID).ToList();
            return Json(new { result = plant, message = "Successfully !" });
        }

        public JsonResult getSellPrice(string RegID)
        {
            TKC_MASTER_SAPEntities db = new TKC_MASTER_SAPEntities();
            var plant = db.T_SAPMAT_PRICE_PACK1.Where(x => x.I_RegID == RegID).OrderBy(y => y.Seq).ToList();
            return Json(new { result = plant, message = "Successfully !" });
        }

        [HttpPost]
        public JsonResult getDataPlant(string RegID, string Plant, string Amount, string Currency)
        {
            try
            {
                var seq = "0";
                var db = new TKC_MASTER_SAPEntities();
                LogisticController getClassLG = new LogisticController();
                var items = db.T_SAPMAT_SELLING_PLANT1.Where(x => x.I_RegID == RegID).ToList();
                var lstTable = db.T_SAPMAT_PLANT.Where(x => x.CUSTID == Plant).ToList();
                var checklg = db.T_SAP_MAT_SETUP.Where(y => y.I_RegID == RegID).FirstOrDefault();

                if (items != null)
                {
                    var result = items.OrderByDescending(x => x.Seq).FirstOrDefault();
                    if (result == null)
                    {
                        seq = (1).ToString();
                    }
                    else
                    {
                        seq = (Convert.ToInt32(result.Seq) + 1).ToString();
                    }
                }

                List<T_SAPMAT_SELLING_PLANT1> lstinsert = new List<T_SAPMAT_SELLING_PLANT1>();
                foreach (var item in lstTable)
                {
                    T_SAPMAT_SELLING_PLANT1 insPlant = new T_SAPMAT_SELLING_PLANT1();
                    insPlant.I_RegID = RegID;
                    insPlant.Seq = seq;
                    insPlant.Plant = item.PLANT;
                    insPlant.Cust_ID = item.CUSTID;
                    insPlant.Cust_Name = item.CUST_NAME;
                    insPlant.Sold_To = item.SOLD_TO;
                    insPlant.Channel = item.CHANNEL;
                    insPlant.Division = item.DIVISION;
                    insPlant.Ship_To = item.SHIP_TO;
                    insPlant.Str_Loc = item.Str_Loc;
                    if (Amount != "")
                    {
                        insPlant.Amount = decimal.Parse(Amount);
                        insPlant.Currency = Currency;
                    }


                    insPlant.UpdBy = User.Identity.Name;
                    insPlant.UpdDate = DateTime.Now;
                    lstinsert.Add(insPlant);

                    var dbUser = new AuthenticationDB();
                    List<string> _Roles = new List<string> { "LG" };
                    var getemail = dbUser.Users.Where(x => (x.Roles.Any(y => _Roles.Contains(y.RoleName)))).ToList();

                    foreach (var itemmail in getemail)
                    {
                        string html = "<span style='font-family:Tahoma;font-size: 13pt;color:#1E90FF;'>";
                        html += "Dear  All concerns<br>";
                        html += "TKC add new customer in SAP Material System.<br>";
                        html += "I_RegID : " + RegID + "<br>";
                        html += "CustName : " + item.CUST_NAME + "<br>";
                        html += "Please check and add L_boxQty.<br>";
                        html += "TKC <br>";
                        html += "System Development. <br>";
                        html += "Tel. 109";
                        html += "</span>";
                        NotificationEmail(itemmail.Email, "Add new customer from SAP Material System", html);
                    }

                }
                if (lstinsert.Count > 0)
                {
                    db.T_SAPMAT_SELLING_PLANT1.AddRange(lstinsert);
                    getClassLG.getLGOK(RegID);
                    var i = db.SaveChanges();
                    getMKOK(RegID);

                    items = db.T_SAPMAT_SELLING_PLANT1.Where(x => x.I_RegID == RegID).ToList();

                }

                return Json(new { isValid = true, result = lstTable, message = "request successfully", newSeq = seq });

                //return Json(new { result = lstTable, message = seq });
            }

            catch (Exception ex)
            {
                errorMsg = ex.Message.ToString();
                return Json(new { isValid = false, result = "error !!", message = errorMsg });
            }
        }

        [HttpPost]
        public JsonResult insSelliingPlant(List<T_SAPMAT_SELLING_PLANT1> model)
        {
            string msg = "";
            var db = new TKC_MASTER_SAPEntities();
            TKC_MASTER_SAPEntities dbcontext = new TKC_MASTER_SAPEntities();
            var CheckDup = dbcontext.T_SAPMAT_SELLING_PLANT1.ToList();
            T_SAP_MAT_SETUP insNG = new T_SAP_MAT_SETUP();
            List<T_SAPMAT_SELLING_PLANT1> lstinsert = new List<T_SAPMAT_SELLING_PLANT1>();
            try
            {
                foreach (var item in model)
                {
                    var Chk = CheckDup.Where(x => x.Cust_ID == item.Cust_ID && x.I_RegID == item.I_RegID).FirstOrDefault();
                    var mat = db.T_SAP_MAT_SETUP.Where(y => y.I_RegID == item.I_RegID).FirstOrDefault();

                    if (Chk == null)
                    {
                        //checking before save
                        T_SAPMAT_SELLING_PLANT1 insPlant = new T_SAPMAT_SELLING_PLANT1();

                        insPlant.I_RegID = item.I_RegID;
                        insPlant.Seq = item.Seq;
                        insPlant.Plant = item.Plant;
                        insPlant.Cust_ID = item.Cust_ID;
                        insPlant.Cust_Name = item.Cust_Name;
                        insPlant.Sold_To = item.Sold_To;
                        insPlant.Channel = item.Channel;
                        insPlant.Division = item.Division;
                        insPlant.Ship_To = item.Ship_To;
                        insPlant.Str_Loc = item.Str_Loc;
                        insPlant.Amount = item.Amount;
                        insPlant.Currency = item.Currency;
                        insPlant.UpdDate = DateTime.Now;
                        insPlant.UpdBy = User.Identity.Name;
                        lstinsert.Add(insPlant);

                        var Data = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == item.I_RegID).FirstOrDefault();
                        Data.I_Status_LG = null;
                        Data.I_Approve_LG = null;
                        var i = db.SaveChanges();


                        var dbUser = new AuthenticationDB();
                        List<string> _Roles = new List<string> { "LG" };
                        var getemail = dbUser.Users.Where(x => (x.Roles.Any(y => _Roles.Contains(y.RoleName)))).ToList();

                        foreach (var items in getemail)
                        {
                            string html = "<span style='font-family:Tahoma;font-size: 13pt;color:#1E90FF;'>";
                            html += "Dear  All concerns<br>";
                            html += "TKC have update ship to in SAP Material System.<br>";
                            html += "I_RegID " + insPlant.I_RegID + "<br>";
                            html += "Model " + mat.M_Model + "<br>";
                            html += "Material number " + mat.M_MatNo + "<br>";
                            html += "Please check and prepare the information that related to you within due date.<br>";
                            html += "TKC <br>";
                            html += "System Development. <br>";
                            html += "Tel. 109";
                            html += "</span>";
                            NotificationEmail(items.Email, "Start new material from SAP Material System", html);
                        }
                    }
                }

                //model.Channel = "10"; สามารถfixedค่าได้
                if (lstinsert.Count > 0)
                {
                    dbcontext.T_SAPMAT_SELLING_PLANT1.AddRange(lstinsert);
                    var i = dbcontext.SaveChanges();
                    msg = "Success!!";
                }
                else
                {
                    msg = "Success!!";
                }

                return Json(new { isValid = true, result = "insert success !!", message = "request successfully" });
            }

            catch (Exception ex)
            {
                //foreach (var eve in e.EntityValidationErrors)
                //{
                //    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                //        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                //    foreach (var ve in eve.ValidationErrors)
                //    {
                //        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                //            ve.PropertyName, ve.ErrorMessage);
                //    }
                //}
                //msg = "error !!";
                errorMsg = ex.Message.ToString();
                return Json(new { isValid = false, result = "error !!", message = errorMsg });
            }
        }
        
        [HttpPost]
        public JsonResult DelDataPlant(string id, string custID)
        {
            try
            {
                var db = new TKC_MASTER_SAPEntities();
                var user = new AuthenticationDB();
                var items = db.T_SAPMAT_SELLING_PLANT1.Where(x => x.Cust_ID == custID && x.I_RegID == id).FirstOrDefault();
                var updateSapMat = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == id).FirstOrDefault();
                
                if (items != null)
                {
                    db.T_SAPMAT_SELLING_PLANT1.Remove(items);
                    var Save = new T_SAPMAT_REVISE_LOG();
                    var getupdate = User.Identity.Name;
                    var getUserRole = user.Users.Where(x => x.Username == getupdate).FirstOrDefault();
                    //if (items.Revise == null)
                    //{
                    //    items.Revise = 0;
                    //}
                    //if (items.Exported == "1")
                    //{
                    //    items.Revise += 1;
                    //}
                    //else
                    //{
                    //    items.Revise = items.Revise;
                    //}
                    //items.Exported = null;
                    //items.ExpBy = null;
                    //items.ExpDate = null;

                    if (updateSapMat.I_Approve_MK != null)
                    {
                        updateSapMat.I_Approve_MK = null;
                    }
                    Save.i_Reg_id = items.I_RegID;
                    Save.page = "Delete Ship To";
                    Save.RevNo = items.Revise;
                    Save.updDept = getUserRole.Roles.FirstOrDefault().RoleFullName.ToString();
                    Save.updby = User.Identity.Name;
                    Save.updDate = DateTime.Now;
                    db.T_SAPMAT_REVISE_LOG.Add(Save);
                    db.SaveChanges();
                    List<string> _Roles = new List<string> { "MK" };
                    var getemail = user.Users.Where(x => (x.Roles.Any(y => _Roles.Contains(y.RoleName)))).ToList();

                    foreach (var item in getemail)
                    {
                        string html = "<span style='font-family:Tahoma;font-size: 13pt;color:#1E90FF;'>";
                        html += "Dear  All concerns<br>";
                        html += "Marketing have update Ship to in SAP Material System.<br>";
                        html += "I_RegID :" + updateSapMat.I_RegID + "<br>";
                        html += "Model :" + updateSapMat.M_Model + "<br>";
                        html += "Material number :" + updateSapMat.M_MatNo + "<br>";
                        html += "Please check and prepare the information that related to you within due date.<br>";
                        html += "TKC <br>";
                        html += "System Development. <br>";
                        html += "Tel. 109";
                        html += "</span>";
                        NotificationEmail(item.Email, "SAP Material System have update", html);
                    }
                }


                //ViewBag.plant = items;
                //return Json(new { result = "Success !!", message = "request successfully" });
                return Json(new { isValid = true, result = "insert success !!", message = "request successfully" });
            }

            catch (Exception ex)
            {
                errorMsg = ex.Message.ToString();
                return Json(new { isValid = false, result = "error !!", message = errorMsg });
            }

        }
        public JsonResult UpdateDataPlant(string id, string custID, string custMat, string custname)
        {
            var db = new TKC_MASTER_SAPEntities();
            var items = db.T_SAPMAT_SELLING_PLANT1.Where(x => x.Cust_ID == custID && x.I_RegID == id).FirstOrDefault();
            TempData["CustName"] = custname;
            TempData["MatNo"] = custMat;
            TempData["item"] = items;
            return Json(new { result = "Success !!", message = "request successfully" });
        }
        public ActionResult UpdateShipto()
        {
            var db = new TKC_MASTER_SAPEntities();
            var getdata = TempData["item"] as T_SAPMAT_SELLING_PLANT1;
            var shipto = db.T_SAPMAT_PLANT.ToList();

            List<ListDropDown> ShipTo = new List<ListDropDown>();
            foreach (var item in shipto.OrderBy(x => x.CUST_NAME))
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.CUSTID + " " + item.CUST_NAME;
                iTmp.iValue = item.CUSTID;
                ShipTo.Add(iTmp);
            }
            ViewBag.shipto = ShipTo;
            //if (items != null)
            //{
            //    db.T_SAPMAT_SELLING_PLANT1.Remove(items);
            //    db.SaveChanges();
            //}
            ViewBag.custname = TempData["CustName"];
            ViewBag.custMatNo = TempData["MatNo"];
            TempData["item"] = getdata;
            return View(getdata);
        }
        
        [HttpPost]
        public JsonResult getShipToUpdate(ShipToMk data)
        {
            try
            {
                var db = new TKC_MASTER_SAPEntities();
                LogisticController getLGClass = new LogisticController();
                var user = new AuthenticationDB();
                var updateSapMat = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == data.I_RegID).FirstOrDefault();
                var Update = db.T_SAPMAT_SELLING_PLANT1.Where(x => x.I_RegID == data.I_RegID && x.Seq == data.seq).FirstOrDefault();
                if (data.CustomerName == null)
                {
                    data.CustomerName = Update.Cust_ID + " " + Update.Cust_Name;
                }
                int Index = data.CustomerName.IndexOf(' ');
                var cusid = data.CustomerName.Substring(0, Index);
                var cusname = data.CustomerName.Substring(Index + 1);

                Update.Amount = data.Amount;
                Update.I_RegID = data.I_RegID;
                Update.Seq = data.seq;
                Update.Plant = data.Plant;
                Update.Cust_ID = cusid;
                Update.Cust_Name = cusname;
                Update.Sold_To = data.SoldTo;
                Update.Channel = data.Channel;
                Update.Division = data.Division;
                Update.Ship_To = data.ShipTo;
                Update.UpdBy = User.Identity.Name;
                Update.UpdDate = DateTime.Now;
                if (Update.Revise == null)
                {
                    Update.Revise = 0;
                }
                if (Update.Exported == "1")
                {
                    Update.Revise += 1;
                }
                else
                {
                    Update.Revise = Update.Revise;
                }
                Update.Exported = null;
                Update.ExpBy = null;
                Update.ExpDate = null;
                getMKOK(data.I_RegID);
                getLGClass.getLGOK(data.I_RegID);

                if (updateSapMat.I_Approve_MK != null)
                {
                    updateSapMat.I_Approve_MK = null;
                }
                var Save = new T_SAPMAT_REVISE_LOG();
                var getupdate = User.Identity.Name;
                var getUserRole = user.Users.Where(x => x.Username == getupdate).FirstOrDefault();

                Save.i_Reg_id = Update.I_RegID;
                Save.page = "Ship To";
                Save.RevNo = Update.Revise;
                Save.updDept = getUserRole.Roles.FirstOrDefault().RoleFullName.ToString();
                Save.updby = User.Identity.Name;
                Save.updDate = DateTime.Now;
                db.T_SAPMAT_REVISE_LOG.Add(Save);
                db.SaveChanges();
                List<string> _Roles = new List<string> { "MK" };
                var getemail = user.Users.Where(x => (x.Roles.Any(y => _Roles.Contains(y.RoleName)))).ToList();

                foreach (var items in getemail)
                {
                    string html = "<span style='font-family:Tahoma;font-size: 13pt;color:#1E90FF;'>";
                    html += "Dear  All concerns<br>";
                    html += "Marketing have update Ship to in SAP Material System.<br>";
                    html += "I_RegID :" + updateSapMat.I_RegID + "<br>";
                    html += "Model :" + updateSapMat.M_Model + "<br>";
                    html += "Material number :" + updateSapMat.M_MatNo + "<br>";
                    html += "Please check and prepare the information that related to you within due date.<br>";
                    html += "TKC <br>";
                    html += "System Development. <br>";
                    html += "Tel. 109";
                    html += "</span>";
                    NotificationEmail(items.Email, "SAP Material System have update", html);
                }

                List<string> _RolesLG = new List<string> { "LG" };
                var getemailLG = user.Users.Where(x => (x.Roles.Any(y => _RolesLG.Contains(y.RoleName)))).ToList();

                foreach (var items in getemailLG)
                {
                    string html = "<span style='font-family:Tahoma;font-size: 13pt;color:#1E90FF;'>";
                    html += "Dear  All concerns<br>";
                    html += "Marketing have update Cust mat in SAP Material System.<br>";
                    html += "I_RegID :" + updateSapMat.I_RegID + "<br>";
                    html += "Model :" + updateSapMat.M_Model + "<br>";
                    html += "Material number :" + updateSapMat.M_MatNo + "<br>";
                    html += "Please check and prepare the information that related to you within due date.<br>";
                    html += "TKC <br>";
                    html += "System Development. <br>";
                    html += "Tel. 109";
                    html += "</span>";
                    NotificationEmail(items.Email, "SAP Material System have update", html);
                }

                //return Json(new { result = "success", ID = data.I_RegID });
                return Json(new { isValid = true, result = "insert success !!", message = "request successfully" });
            }

            catch (Exception ex)
            {
                errorMsg = ex.Message.ToString();
                return Json(new { isValid = false, result = "error !!", message = errorMsg });
            }
            

        }
        public JsonResult DelSellPrice(string id, string seq)
        {
            var db = new TKC_MASTER_SAPEntities();
            var items = db.T_SAPMAT_PRICE_PACK1.Where(x => x.Seq == seq && x.I_RegID == id).FirstOrDefault();
            if (items != null)
            {
                db.T_SAPMAT_PRICE_PACK1.Remove(items);
                db.SaveChanges();
            }
            //ViewBag.plant = items;
            return Json(new { result = "Success !!", message = "request successfully" });
        }

        public JsonResult insPlant(T_CUST_SHIP_TO_BY_CUSTID model)
        {
            string msg = "";
            var db = new TKC_MASTER_SAPEntities();
            TKC_MASTER_SAPEntities dbcontext = new TKC_MASTER_SAPEntities();
            try
            {
                T_CUST_SHIP_TO_BY_CUSTID insMat = new T_CUST_SHIP_TO_BY_CUSTID();

                insMat.DockCode = model.DockCode;
                insMat.CustSAP_SOLD_TO = model.CustSAP_SOLD_TO;
                insMat.Str_Loc = model.Str_Loc;
                //model.Channel = "10"; สามารถfixedค่าได้
                dbcontext.T_CUST_SHIP_TO_BY_CUSTID.Add(insMat);
                var i = dbcontext.SaveChanges();
                msg = "Success!!";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                msg = "error !!";
            }
            return Json(new { result = msg, message = "request successfully" });
        }

        [CustomAuthorize(Roles = "CS,MK,LG,CO,PPC,SaleAdmin,PU,MgrMK,MgrSaleAdmin")]
        public ActionResult Display()
        {
            var db = new TKC_MASTER_SAPEntities();
            var data = db.T_SAP_MAT_SETUP.ToList();
            ViewBag.Mydata = data;
            return View();
        }

        [CustomAuthorize(Roles = "CS")]
        public ActionResult AddPlant()
        {
            var db = new TKC_MASTER_SAPEntities();
            var data = db.T_CUST_SHIP_TO_BY_CUSTID.ToList();

            var strLoc = db.T_LOCATION_SAP.ToList();
            List<ListDropDown> Loc = new List<ListDropDown>();
            foreach (var item in strLoc.OrderBy(x => x.LOC_SAP_ID))
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.LOC_SAP_ID + " : " + item.LOC_NAME;
                iTmp.iValue = item.LOC_SAP_ID;
                Loc.Add(iTmp);
            }
            ViewBag.strLoc = Loc;

            return View();
        }

        [CustomAuthorize(Roles = "CS,MK,LG,CO,PPC,SaleAdmin,PU,MgrMK,MgrSaleAdmin")]
        public ActionResult ViewDetail(string id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var data = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == id).FirstOrDefault();
            return View(data);
        }

        public bool IsAnyNullOrEmpty(object myObject)
        {
            foreach (PropertyInfo pi in myObject.GetType().GetProperties())
            {
                if (pi.PropertyType == typeof(string))
                {
                    //pi.PropertyType != "I_Status"
                    if (pi.Name != "I_Status")
                    {
                        string value = (string)pi.GetValue(myObject);
                        if (string.IsNullOrEmpty(value))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public JsonResult updateMarketing(Marketing model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = new AuthenticationDB();
                    var db = new TKC_MASTER_SAPEntities();
                    var Data = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == model.I_RegID).FirstOrDefault();
                    var Save = new T_SAPMAT_REVISE_LOG();

                    if (Data != null)
                    {
                        decimal M_SellPrice = decimal.Parse(model.M_SellPrice);

                        var C_CustPNo = model.C_CustPNo;
                        var CustPno = db.T_SAPMAT_SELLING_PLANT1.Where(x => x.I_RegID == model.I_RegID).ToList();
                        ////////////////reset Price Export//////////////////////
                        Data.Price_Exported = null;
                        Data.Price_ExpBy = null;
                        Data.Price_ExpDate = null;
                        //if (Data.Price_Revise == null)
                        //{
                        //    Data.Price_Revise = 0;
                        //}
                        //if (Data.Price_Exported == "1")
                        //{
                        //    Data.Price_Exported = null;
                        //    Data.Price_Revise += 1;
                        //}
                        //else
                        //{
                        //    Data.Price_Revise = Data.Price_Revise;
                        //}
                        //Data.Price_ExpBy = null;
                        //Data.Price_ExpDate = null;
                        ////////////////reset Mat Export//////////////////////
                        ///
                        Data.Mat_Exported = null;
                        Data.Mat_ExpBy = null;
                        Data.Mat_ExpDate = null;
                        Data.Mat_Revise = null;

                        //if (Data.Mat_Revise == null)
                        //{
                        //    Data.Mat_Revise = 0;
                        //}
                        //if (Data.Mat_Exported == "1")
                        //{
                        //    Data.Mat_Exported = null;
                        //    Data.Mat_Revise += 1;
                        //}
                        //else
                        //{
                        //    Data.Mat_Revise = Data.Mat_Revise;
                        //}
                        //Data.Mat_ExpBy = null;
                        //Data.Mat_ExpDate = null;
                        ////////////////////////////////////
                        Data.CUSTOMER = model.CUSTOMER;
                        Data.M_Model = model.M_Model;
                        Data.M_Lineoff = model.M_Lineoff;
                        Data.M_MatNo = model.M_MatNo;
                        /* Data.M_MatDesc = model.M_MatDesc;*/
                        Data.M_TKCCustPNo = model.C_CustPNo.Replace("-", "");
                        Data.M_Remark = model.M_Remark;
                        Data.M_CustPName = model.M_CustPName;
                        Data.C_CustPNo = model.C_CustPNo;
                        Data.M_SellPrice = M_SellPrice;
                        Data.M_ST_Effect = model.M_ST_Effect;
                        Data.A_Mat_Group = model.A_Mat_Group;
                        Data.A_Part_Name = model.A_Part_Name;
                        Data.A_Mold_Owner = model.A_Mold_Owner;
                        Data.A_Light_Type = model.A_Light_Type;
                        /* Data.A_Type_No = model.A_Type_No;*/
                        Data.A_Part_Side = model.A_Part_Side;
                        Data.M_Remark = model.M_Remark;
                        if (model.getFirst == true)
                        {
                            if (model.SellPrice_Status == "true")
                            {
                                Data.SellPrice_Status = "false";
                            }
                            else
                            {
                                Data.SellPrice_Status = "true";
                            }
                        }
                        else
                        {
                            Data.SellPrice_Status = model.SellPrice_Status;
                        }
                        //Data.I_Status_MK = "OK";
                        ////////////////////////////////////////
                        Data.LastUpdBy = User.Identity.Name;
                        Data.LastUpdDate = DateTime.Now;
                        //////////////////////////////////////
                        if (Data.I_Approve_MK != null)
                        {
                            Data.I_Approve_MK = null;
                        }

                        Data.Car_Type = model.Car_Type;
                        var getupdate = User.Identity.Name;
                        var getUserRole = user.Users.Where(x => x.Username == getupdate).FirstOrDefault();

                        Save.i_Reg_id = Data.I_RegID;
                        Save.page = "Mat Master";
                        Save.RevNo = Data.Price_Revise;
                        Save.updDept = getUserRole.Roles.FirstOrDefault().RoleFullName.ToString();
                        Save.updby = User.Identity.Name;
                        Save.updDate = DateTime.Now;
                        db.T_SAPMAT_REVISE_LOG.Add(Save);
                        var i = db.SaveChanges();
                        getMKOK(model.I_RegID);
                        getPartOK(model.I_RegID);
                        List<string> _Roles = new List<string> { "MK" };
                        var getemail = user.Users.Where(x => (x.Roles.Any(y => _Roles.Contains(y.RoleName)))).ToList();

                        foreach (var items in getemail)
                        {
                            string html = "<span style='font-family:Tahoma;font-size: 13pt;color:#1E90FF;'>";
                            html += "Dear  All concerns<br>";
                            html += "Marketing have update in SAP Material System.<br>";
                            html += "I_RegID " + Data.I_RegID + "<br>";
                            html += "Model " + Data.M_Model + "<br>";
                            html += "Material number " + Data.M_MatNo + "<br>";
                            html += "Please check and prepare the information that related to you within due date.<br>";
                            html += "TKC <br>";
                            html += "System Development. <br>";
                            html += "Tel. 109";
                            html += "</span>";
                            NotificationEmail(items.Email, "SAP Material System have update", html);
                        }

                        //Data.I_Status_LG = null;
                        //Data.I_Approve_LG = null;
                        return Json(new { isValid = true, result = "insert success !!", message = "request successfully" });

                    }
                    else
                    {
                        //return Json(new { result = "no data !!", message = "request successfully" });
                        return Json(new { isValid = false, result = "no data !!", message = "no data to send email !!" });
                    }
                }
                catch (Exception ex)
                {
                    //return Json(new { result = "error", message = "request successfully" });
                    errorMsg = ex.Message.ToString();
                    return Json(new { isValid = false, result = "error !!", message = errorMsg });
                }
                //return Json(new { result = "insert success !!", message = "request successfully" });
            }
            else
            {
                //return Json(new { result = "input data!!", message = "request successfully" });
                return Json(new { isValid = false, result = "error !!", message = "input data!!" });
            }
        }

        public void getMKOK(string TkcPart) //check status
        {
            var db = new TKC_MASTER_SAPEntities();
            var Customer = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == TkcPart)
                .FirstOrDefault();

            var shipto = db.T_SAPMAT_SELLING_PLANT1.Where(x => x.I_RegID == TkcPart)
                .FirstOrDefault();

            if (Customer.CUSTOMER != null) // true
            {
                if (Customer.M_Model != null)
                {
                    if (Customer.M_Lineoff != null)
                    {
                        if (Customer.M_MatNo != null)
                        {
                            if (Customer.C_CustPNo != null)
                            {
                                if (Customer.M_CustPName != null)
                                {
                                    if (Customer.M_ST_Effect != null)
                                    {
                                        if (Customer.A_Mat_Group != null)
                                        {
                                            if (Customer.A_Part_Name != null)
                                            {
                                                if (Customer.A_Light_Type != null)
                                                {
                                                    if (Customer.A_Part_Side != null)
                                                    {
                                                        if (shipto.Ship_To != null)
                                                        {
                                                            Customer.I_Status_MK = "OK";
                                                            db.SaveChanges();
                                                        }
                                                        else
                                                        {
                                                            Customer.I_Status_MK = "NG";
                                                            db.SaveChanges();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Customer.I_Status_MK = "NG";
                                                        db.SaveChanges();
                                                    }
                                                }
                                                else
                                                {
                                                    Customer.I_Status_MK = "NG";
                                                    db.SaveChanges();
                                                }
                                            }
                                            else
                                            {
                                                Customer.I_Status_MK = "NG";
                                                db.SaveChanges();
                                            }
                                        }
                                        else
                                        {
                                            Customer.I_Status_MK = "NG";
                                            db.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        Customer.I_Status_MK = "NG";
                                        db.SaveChanges();
                                    }
                                }
                                else
                                {
                                    Customer.I_Status_MK = "NG";
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                Customer.I_Status_MK = "NG";
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            Customer.I_Status_MK = "NG";
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        Customer.I_Status_MK = "NG";
                        db.SaveChanges();
                    }
                }
                else
                {
                    Customer.I_Status_MK = "NG";
                    db.SaveChanges();
                }
            }
            else
            {
                Customer.I_Status_MK = "NG";
                db.SaveChanges();
            }
        }

        [HttpPost]
        public JsonResult updateApproveMK(List<ListDropDown> listkey)
        {
            try
            {
                using (var db = new TKC_MASTER_SAPEntities())
                {
                    foreach (var item in listkey)
                    {
                        var update = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == item.iKey).FirstOrDefault();

                        if (update != null)
                        {
                            update.I_Approve_MK = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "error", message = "request successfully" });
            }
            return Json(new { result = "insert success !!", message = "request successfully" });
        }

        public JsonResult getAllData(string Type)
        {
            var db = new TKC_MASTER_SAPEntities();
            var _Data = db.T_SAP_MAT_SETUP.ToList();
            if (Type == "OK")
            {
                _Data = _Data.Where(x => x.I_Status == "OK").ToList();
            }
            else if (Type == "NG")
            {
                _Data = _Data.Where(x => x.I_Status == "NG").ToList();
            }
            foreach (var item in _Data)
            {
                if (item.M_Lineoff == null)
                {
                    item.I_Status = "0";
                }
                else
                {
                    var splitline = item.M_Lineoff.Split('-');
                    var lineoff = Convert.ToDateTime(splitline[0].ToString() + "-" + splitline[1].ToString() + "-01");
                    var currentLineoff = DateTime.Now;
                    var Endlineoff = lineoff.AddDays(+30);
                    var s_lineoff = (lineoff.Date - currentLineoff.Date).Days;
                    item.I_Status = s_lineoff.ToString();
                }
            }
            return Json(new { result = _Data.Where(x => Convert.ToInt32(x.I_Status) <= 62), message = "request successfully" });
        }

        public JsonResult getPlant(string Type)
        {
            var db = new TKC_MASTER_SAPEntities();
            var _Data = db.T_SAPMAT_PLANT.ToList();

            _Data = _Data.Where(x => x.CUST_NAME == Type).ToList();

            return Json(new { result = _Data, message = "request successfully" });
        }

        public ActionResult CustMS()
        {
            var db = new TKC_MASTER_SAPEntities();
            var Custms = db.T_SAPMAT_Cust_MS.ToList();
            return View(Custms);
        }

        public ActionResult EditCustMS(string Id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var mds = db.T_SAPMAT_Cust_MS.Where(x => x.CustCD == Id).FirstOrDefault();
            return View(mds);
        }

        public ActionResult AddCustMS(string Id)//ดึง data ขึ้นมา show
        {
            return View();
        }

        public JsonResult insCustMS(T_SAPMAT_Cust_MS model)
        {
            string msg = "";
            var db = new TKC_MASTER_SAPEntities();
            TKC_MASTER_SAPEntities dbcontext = new TKC_MASTER_SAPEntities();
            try
            {
                T_SAPMAT_Cust_MS insMat = new T_SAPMAT_Cust_MS();

                insMat.CustCD = model.CustCD;
                insMat.CustName = model.CustName;

                //model.Channel = "10"; สามารถfixedค่าได้
                dbcontext.T_SAPMAT_Cust_MS.Add(insMat);
                var i = dbcontext.SaveChanges();

                msg = "Success!!";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                msg = "error !!";
            }
            return Json(new { result = msg, message = "request successfully" });
        }

        public JsonResult updateCustMS(T_SAPMAT_Cust_MS model) // update data ลง sql
        {
            try
            {
                var db = new TKC_MASTER_SAPEntities();
                var Data = db.T_SAPMAT_Cust_MS.Where(x => x.CustCD == model.CustCD).FirstOrDefault();

                if (Data != null)
                {
                    Data.CustCD = model.CustCD;
                    Data.CustName = model.CustName;

                    var i = db.SaveChanges();
                }
                else
                {
                    return Json(new { result = "no data !!", message = "request successfully" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "error", message = "request successfully" });
            }
            return Json(new { result = "insert success !!", message = "request successfully" });
        }

        public JsonResult DelCustMS(string id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var items = db.T_SAPMAT_Cust_MS.Where(x => x.CustCD == id).FirstOrDefault();
            if (items != null)
            {
                db.T_SAPMAT_Cust_MS.Remove(items);
                db.SaveChanges();
            }
            //ViewBag.plant = items;
            return Json(new { result = "Success !!", message = "request successfully" });
        }

        public ActionResult MatG()
        {
            var db = new TKC_MASTER_SAPEntities();
            var matg = db.T_SAPMAT_Part_Name.ToList();
            return View(matg);
        }

        public ActionResult EditModel(string Id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var mds = db.T_SAPMAT_Model.Where(x => x.CustCD == Id).FirstOrDefault();
            return View(mds);
        }

        public ActionResult AddMatG(string Id)//ดึง data ขึ้นมา show
        {
            var db = new TKC_MASTER_SAPEntities();
            return View();
        }

        [HttpPost]
        public JsonResult insMatG(T_SAPMAT_Part_Name infos)
        {
            string msg = "";
            var db = new TKC_MASTER_SAPEntities();
            TKC_MASTER_SAPEntities dbcontext = new TKC_MASTER_SAPEntities();
            try
            {
                T_SAPMAT_Part_Name insMat = new T_SAPMAT_Part_Name();

                insMat.PartNameCD = infos.PartNameCD;
                insMat.PartNameDesc = infos.PartNameDesc;

                //model.Channel = "10"; สามารถfixedค่าได้
                dbcontext.T_SAPMAT_Part_Name.Add(insMat);
                var i = dbcontext.SaveChanges();
                msg = "Success!!";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                msg = "error !!";
            }
            return Json(new { result = msg, message = "request successfully" });
        }

        public JsonResult DelMatG(string id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var items = db.T_SAPMAT_Part_Name.Where(x => x.PartNameCD == id).FirstOrDefault();
            if (items != null)
            {
                db.T_SAPMAT_Part_Name.Remove(items);
                db.SaveChanges();
            }
            //ViewBag.plant = items;
            return Json(new { result = "Success !!", message = "request successfully" });
        }

        public ActionResult MoldOwner()
        {
            var db = new TKC_MASTER_SAPEntities();
            var mold = db.T_SAPMAT_Mold_Owner.ToList();
            return View(mold);
        }

        public ActionResult EditMold(string Id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var mds = db.T_SAPMAT_Mold_Owner.Where(x => x.MoldOwnerCD == Id).FirstOrDefault();
            return View(mds);
        }

        public JsonResult updateMold(T_SAPMAT_Mold_Owner model) // update data ลง sql
        {
            try
            {
                var db = new TKC_MASTER_SAPEntities();
                var Data = db.T_SAPMAT_Mold_Owner.Where(x => x.MoldOwnerCD == model.MoldOwnerCD).FirstOrDefault();

                if (Data != null)
                {
                    Data.MoldOwnerCD = model.MoldOwnerCD;
                    Data.MoldOwnerName = model.MoldOwnerName;

                    var i = db.SaveChanges();
                }
                else
                {
                    return Json(new { result = "no data !!", message = "request successfully" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "error", message = "request successfully" });
            }

            return Json(new { result = "insert success !!", message = "request successfully" });
        }

        public JsonResult DelMold(string id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var items = db.T_SAPMAT_Mold_Owner.Where(x => x.MoldOwnerCD == id).FirstOrDefault();
            if (items != null)
            {
                db.T_SAPMAT_Mold_Owner.Remove(items);
                db.SaveChanges();
            }
            //ViewBag.plant = items;
            return Json(new { result = "Success !!", message = "request successfully" });
        }

        public ActionResult AddMold(string Id)//ดึง data ขึ้นมา show
        {
            return View();
        }

        public JsonResult insMold(T_SAPMAT_Mold_Owner model)
        {
            string msg = "";
            var db = new TKC_MASTER_SAPEntities();
            TKC_MASTER_SAPEntities dbcontext = new TKC_MASTER_SAPEntities();
            try
            {
                T_SAPMAT_Mold_Owner insMat = new T_SAPMAT_Mold_Owner();

                insMat.MoldOwnerCD = model.MoldOwnerCD;
                insMat.MoldOwnerName = model.MoldOwnerName;

                //model.Channel = "10"; สามารถfixedค่าได้
                dbcontext.T_SAPMAT_Mold_Owner.Add(insMat);
                var i = dbcontext.SaveChanges();
                msg = "Success!!";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                msg = "error !!";
            }
            return Json(new { result = msg, message = "request successfully" });
        }

        public ActionResult PName()
        {
            var db = new TKC_MASTER_SAPEntities();
            var pname = db.T_SAPMAT_Part_Name.ToList();
            return View(pname);
        }

        public JsonResult DelPName(string id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var items = db.T_SAPMAT_Part_Name.Where(x => x.PartNameCD == id).FirstOrDefault();
            if (items != null)
            {
                db.T_SAPMAT_Part_Name.Remove(items);
                db.SaveChanges();
            }
            //ViewBag.plant = items;
            return Json(new { result = "Success !!", message = "request successfully" });
        }

        public JsonResult updatePName(T_SAPMAT_Part_Name model) // update data ลง sql
        {
            try
            {
                var db = new TKC_MASTER_SAPEntities();
                var Data = db.T_SAPMAT_Part_Name.Where(x => x.PartNameCD == model.PartNameCD).FirstOrDefault();

                if (Data != null)
                {
                    Data.PartNameCD = model.PartNameCD;
                    Data.PartNameDesc = model.PartNameDesc;

                    var i = db.SaveChanges();
                }
                else
                {
                    return Json(new { result = "no data !!", message = "request successfully" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "error", message = "request successfully" });
            }
            return Json(new { result = "insert success !!", message = "request successfully" });
        }

        public ActionResult EditPName(string Id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var mds = db.T_SAPMAT_Part_Name.Where(x => x.PartNameCD == Id).FirstOrDefault();
            return View(mds);
        }

        public ActionResult AddPName(string Id)//ดึง data ขึ้นมา show
        {
            return View();
        }

        public JsonResult insPName(T_SAPMAT_Part_Name model)
        {
            string msg = "";
            var db = new TKC_MASTER_SAPEntities();
            TKC_MASTER_SAPEntities dbcontext = new TKC_MASTER_SAPEntities();
            try
            {
                T_SAPMAT_Part_Name insMat = new T_SAPMAT_Part_Name();

                insMat.PartNameCD = model.PartNameCD;
                insMat.PartNameDesc = model.PartNameDesc;

                //model.Channel = "10"; สามารถfixedค่าได้
                dbcontext.T_SAPMAT_Part_Name.Add(insMat);
                var i = dbcontext.SaveChanges();
                msg = "Success!!";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                msg = "error !!";
            }
            return Json(new { result = msg, message = "request successfully" });
        }

        public void NotificationEmail(string email, string subject, string body)
        {

            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress("prones_g@tkoito.co.th");
            msg.To.Add(email);
            msg.Subject = subject;
            msg.Body = body;
            msg.IsBodyHtml = true;

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Port = 25;
            smtpClient.Host = "172.18.1.2";
            smtpClient.Credentials = new NetworkCredential("prones", "zaq1@wsx");
            smtpClient.Send(msg);
        }

        public async System.Threading.Tasks.Task<JsonResult> DownloadExcel(String chkdate)
        {
            var db = new TKC_MASTER_SAPEntities();
            var conn = db.Database.Connection;
            var dt = new DataTable();
            var fileName = "Approve_List_" + DateTime.Now.ToString("yyMMddHHmmss") + ".xls";
            try
            {
                await conn.OpenAsync();
                DbDataReader reader;
                using (var command = conn.CreateCommand())
                {
                    string sql = "SELECT  * FROM V_SAP_MAT_SETUP WHERE I_Status_MK = 'OK' and I_Approve_MK is null";
                    command.CommandText = sql;
                    command.CommandTimeout = 0;

                    string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);
                    FileStream file = new FileStream(fullPath, FileMode.Create, FileAccess.Write);

                    reader = await command.ExecuteReaderAsync();
                    dt.Load(reader);
                    reader.Close();

                    ExcelPackage Ep = new ExcelPackage();
                    ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("ApproveList");

                    Sheet.Cells["A1"].Value = "Customer";
                    Sheet.Cells["B1"].Value = "Model";
                    Sheet.Cells["C1"].Value = "Lineoff";
                    Sheet.Cells["D1"].Value = "Material No";
                    Sheet.Cells["E1"].Value = "Cust P/No";
                    Sheet.Cells["F1"].Value = "Cust P/Name";
                    Sheet.Cells["G1"].Value = "SellPrice";
                    Sheet.Cells["H1"].Value = "Start Effect";
                    Sheet.Cells["I1"].Value = "Part Side";
                    Sheet.Cells["J1"].Value = "Remark";
                    Sheet.Cells["K1"].Value = "TKC Cust P/No";
                    Sheet.Cells["L1"].Value = "Reg ID";
                    int row = 2;
                    foreach (DataRow item in dt.Rows)
                    {

                        Sheet.Cells[string.Format("A{0}", row)].Value = item["Customer"].ToString();
                        Sheet.Cells[string.Format("B{0}", row)].Value = item["M_Model"].ToString();
                        Sheet.Cells[string.Format("C{0}", row)].Value = item["M_Lineoff"].ToString();
                        Sheet.Cells[string.Format("D{0}", row)].Value = item["M_MatNo"].ToString();
                        Sheet.Cells[string.Format("E{0}", row)].Value = item["C_CustPNo"].ToString();
                        Sheet.Cells[string.Format("F{0}", row)].Value = item["M_CustPName"].ToString();
                        Sheet.Cells[string.Format("G{0}", row)].Value = item["M_SellPrice"].ToString();
                        Sheet.Cells[string.Format("H{0}", row)].Value = item["M_ST_Effect"].ToString();
                        Sheet.Cells[string.Format("I{0}", row)].Value = item["A_Part_Side"].ToString();
                        Sheet.Cells[string.Format("J{0}", row)].Value = item["M_Remark"].ToString();
                        Sheet.Cells[string.Format("K{0}", row)].Value = item["M_TKCCustPNo"].ToString();
                        Sheet.Cells[string.Format("L{0}", row)].Value = item["I_RegID"].ToString();

                        row += 1;
                    }


                    Sheet.Cells["A:AZ"].AutoFitColumns();
                    Response.Clear();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment: filename=\"" + fileName + "\"");
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        Ep.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(file);
                        file.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
            return Json(new { fileName = fileName, errorMessage = "" });

        }
        public ActionResult Download(string file)
        {
            //get the temp folder and file path in server
            string fullPath = Path.Combine(Server.MapPath("~/temp"), file);

            //return the file for download, this is an Excel 
            //so I set the file content type to "application/vnd.ms-excel"
            return File(fullPath, "application/vnd.ms-excel", file);
        }
    }
}