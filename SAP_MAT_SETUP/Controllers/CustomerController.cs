﻿using SAP_MAT_SETUP.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Validation;
using System.IO;
using SAP_MAT_SETUP.Models;
using static SAP_MAT_SETUP.Controllers.HomeController;
using System.Data.SqlClient;

namespace SAP_MAT_SETUP.Controllers
{
    public class CustomerController : Controller
    {

        string connectionString = @"Data Source=tkcb01s;Initial Catalog=Sales;User ID=sa;Password=sa;Connection Timeout=5000;";
        // GET: Customer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SoldTo()
        {
            var db = new TKC_MASTER_SAPEntities();
            var Soldto = db.T_CUST_SOLD_TO.ToList();
            return View(Soldto);
        }

        public ActionResult Master()
        {
            /*var db = new TKC_MASTER_SAPEntities();
            var Soldto = db.T_CUST_SOLD_TO.ToList();*/
            return View();
        }

        public ActionResult EditSoldTo(string Id)//ดึง data ขึ้นมา show
        {
            var db = new TKC_MASTER_SAPEntities();
            int _Id = Convert.ToInt32(Id); //แปลง id เพราะมีค่าเป็น int
            var Soldto = db.T_CUST_SOLD_TO.Where(x => x.Id == _Id).FirstOrDefault();
            return View(Soldto);
        }

        public ActionResult AddSoldTo(string Id)//ดึง data ขึ้นมา show
        {
            var db = new TKC_MASTER_SAPEntities();
            var model = db.T_Division_Master.ToList();
            List<ListDropDown> List = new List<ListDropDown>();
            foreach (var item in model)
            {
                ListDropDown addlist = new ListDropDown();
                addlist.iKey = item.DivisionCD + " " + item.DivisionName;
                addlist.iValue = item.DivisionCD;
                List.Add(addlist);
            }
            ViewBag.List = List;
            return View();
        }

        public JsonResult insSoldTo(T_CUST_SOLD_TO model)
        {
            string msg = "";
            var db = new TKC_MASTER_SAPEntities();
            TKC_MASTER_SAPEntities dbcontext = new TKC_MASTER_SAPEntities();
            try
            {
                T_CUST_SOLD_TO insMat = new T_CUST_SOLD_TO();

                insMat.Id = '1';
                insMat.CustID_SOLD_TO = model.CustID_SOLD_TO;
                insMat.CustSAP_SOLD_TO = model.CustSAP_SOLD_TO;
                insMat.CustomerDesc = model.CustomerDesc;
                insMat.BranchCode = model.BranchCode;
                insMat.BranchDesc = model.BranchDesc;
                insMat.Channel = model.Channel;
                insMat.Division = model.Division;
                insMat.ExportCust = model.ExportCust;


                //model.Channel = "10"; สามารถfixedค่าได้
                dbcontext.T_CUST_SOLD_TO.Add(insMat);
                var i = dbcontext.SaveChanges();

                msg = "Success!!";

            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                msg = "error !!";
            }
            return Json(new { result = msg, message = "request successfully" });
        }

        public JsonResult DelSoldTo(int id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var items = db.T_CUST_SOLD_TO.Where(x => x.Id == id).FirstOrDefault();
            if (items != null)
            {
                db.T_CUST_SOLD_TO.Remove(items);
                db.SaveChanges();
            }
            //ViewBag.plant = items;
            return Json(new { result = "Success !!", message = "request successfully" });
        }

        public JsonResult updateSoldto(T_CUST_SOLD_TO model) // update data ลง sql
        {
            try
            {
                var db = new TKC_MASTER_SAPEntities();
                var Data = db.T_CUST_SOLD_TO.Where(x => x.Id == model.Id).FirstOrDefault();

                if (Data != null)
                {
                    Data.CustID_SOLD_TO = model.CustID_SOLD_TO;
                    Data.CustSAP_SOLD_TO = model.CustSAP_SOLD_TO;
                    Data.CustomerDesc = model.CustomerDesc;
                    Data.BranchCode = model.BranchCode;
                    Data.BranchDesc = model.BranchDesc;
                    Data.Channel = model.Channel;
                    Data.Division = model.Division;
                    Data.ExportCust = model.ExportCust;

                    var i = db.SaveChanges();

                }
                else
                {
                    return Json(new { result = "no data !!", message = "request successfully" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "error", message = "request successfully" });
            }

            return Json(new { result = "insert success !!", message = "request successfully" });
        }

        public ActionResult Cust4MDS()
        {
            List<Models.Cust4MDSModels> list = new List<Cust4MDSModels>();
            try
            {
                using (SqlConnection con = new SqlConnection())
                {
                    String sql = @"select * from T_SchCust4MDSOrders";
                    con.ConnectionString = @"Data Source=tkcb01s;Initial Catalog=Sales;User ID=sa;Password=sa;Connection Timeout=5000;";

                    DataTable dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = new SqlCommand(sql, con);

                    da.Fill(dt);

                    foreach (DataRow row in dt.Rows)
                    {
                        var MDS = new Models.Cust4MDSModels();
                        MDS.CustID = row["CustID"].ToString();
                        MDS.DataType = row["DataType"].ToString();
                        MDS.DataTypeRPT = row["DataTypeRPT"].ToString();
                        MDS.Sellingtype = row["Sellingtype"].ToString();

                        list.Add(MDS);
                    }
                }
                return View(list);
            }
            catch
            {
                return View("Error");
            }

        }

        public ActionResult AddCust4MDS()//ดึง data ขึ้นมา show
        {
            return View();
        }
        [HttpPost]
        public JsonResult insCust4MDS(Cust4MDSModels model)
        {

            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    string query = "Insert Into [Sales].[dbo].[T_SchCust4MDSOrders]([CustID],[DataType],[DataTypeRPT],[Sellingtype]) VALUES (@CustID,@DataType,@DataTypeRPT,@Sellingtype)";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@CustID", model.CustID);
                    cmd.Parameters.AddWithValue("@DataType", model.DataType);
                    cmd.Parameters.AddWithValue("@DataTypeRPT", model.DataTypeRPT);
                    cmd.Parameters.AddWithValue("@Sellingtype", model.Sellingtype);
                    cmd.ExecuteNonQuery();
                }
                return Json(new { result = "success", message = "request successfully" });
            }
            catch (DbEntityValidationException e)
            {
                return Json(new { result = e, message = "request successfully" });
            }

        }

        [HttpPost]
        public JsonResult DelCust4MDS(string id)
        {
            try
            {
                var custid = id.Substring(0, 4);
                var dt = id.Substring(4, 2);
                var dt_rpt = id.Substring(6, 2);
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    string query = "Delete [Sales].[dbo].[T_SchCust4MDSOrders] where CustID = @CustID and DataType=@DataType and DataTypeRPT=@DataTypeRPT ";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@CustID", custid);
                    cmd.Parameters.AddWithValue("@DataType", dt);
                    cmd.Parameters.AddWithValue("@DataTypeRPT", dt_rpt);
                    cmd.ExecuteNonQuery();

                    return Json(new { result = "Success", message = "request successfully" });
                }
            }
            catch (DbEntityValidationException e)
            {
                return Json(new { result = e, message = "request successfully" });
            }

        }

        public ActionResult ShipTo()
        {
            var db = new TKC_MASTER_SAPEntities();
            var Shipto = db.T_CUST_SHIP_TO_BY_CUSTID.ToList();
            return View(Shipto);
        }

        public ActionResult EditShipTo(string Id)
        {
            var db = new TKC_MASTER_SAPEntities();
            /*int _Id = Convert.ToInt32(Id);*/
            var Shipto = db.T_CUST_SHIP_TO_BY_CUSTID.Where(x => x.DockCode == Id).FirstOrDefault();
            return View(Shipto);
        }
        [HttpPost]
        public JsonResult DelShipTo(string Dockcode)
        {
            var db = new TKC_MASTER_SAPEntities();
            var items = db.T_CUST_SHIP_TO_BY_CUSTID.Where(x => x.DockCode == Dockcode).FirstOrDefault();
            if (items != null)
            {
                db.T_CUST_SHIP_TO_BY_CUSTID.Remove(items);
                db.SaveChanges();
            }
            //ViewBag.plant = items;
            return Json(new { result = "Success !!", message = "request successfully" });
        }

        public JsonResult inslog(T_SAP_MAT_SETUP_LOG model)
        {
            string msg = "";
            var db = new TKC_MASTER_SAPEntities();
            TKC_MASTER_SAPEntities dbcontext = new TKC_MASTER_SAPEntities();
            try
            {
                T_SAP_MAT_SETUP_LOG insLog = new T_SAP_MAT_SETUP_LOG();

                insLog.UserId = User.Identity.Name;
                insLog.FieldData = "Delete Ship To";
                insLog.Old = model.Old;
                insLog.UpDateTime = DateTime.Now.ToString();



                //model.Channel = "10"; สามารถfixedค่าได้
                dbcontext.T_SAP_MAT_SETUP_LOG.Add(insLog);
                var i = dbcontext.SaveChanges();

                msg = "Success!!";

            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                msg = "error !!";
            }
            return Json(new { result = msg, message = "request successfully" });
        }

        public JsonResult updateShipto(T_CUST_SHIP_TO_BY_CUSTID model)
        {
            try
            {
                var db = new TKC_MASTER_SAPEntities();
                var Data = db.T_CUST_SHIP_TO_BY_CUSTID.Where(x => x.DockCode == model.DockCode).FirstOrDefault();

                if (Data != null)
                {
                    Data.DockCode = model.DockCode;
                    Data.CustSAP_SOLD_TO = model.CustSAP_SOLD_TO;

                    var i = db.SaveChanges();

                }
                else
                {
                    return Json(new { result = "no data !!", message = "request successfully" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { result = "error", message = "request successfully" });
            }

            return Json(new { result = "insert success !!", message = "request successfully" });
        }

        public ActionResult Mdstosap()
        {
            var db = new TKC_MASTER_SAPEntities();
            var tmp = db.T_UPLOAD_SO_TO_SAP.ToList();

            return View(tmp);
        }

        public ActionResult EditMds(string Id)
        {
            var db = new TKC_MASTER_SAPEntities();
            int _Id = Convert.ToInt32(Id);
            var mds = db.T_UPLOAD_SO_TO_SAP.Where(x => x.Id == _Id).FirstOrDefault();
            return View(mds);
        }

        public JsonResult DelMDS(int id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var items = db.T_UPLOAD_SO_TO_SAP.Where(x => x.Id == id).FirstOrDefault();
            if (items != null)
            {
                db.T_UPLOAD_SO_TO_SAP.Remove(items);
                db.SaveChanges();
            }
            //ViewBag.plant = items;
            return Json(new { result = "Success !!", message = "request successfully" });
        }

        public JsonResult updateMds(T_UPLOAD_SO_TO_SAP model)
        {
            try
            {
                var db = new TKC_MASTER_SAPEntities();
                var Data = db.T_UPLOAD_SO_TO_SAP.Where(x => x.Id == model.Id).FirstOrDefault();
                if (Data != null)
                {
                    Data.CUST_ID = model.CUST_ID;
                    Data.DATATYPE = model.DATATYPE;
                    Data.CUSTNAME_GROUP = model.CUSTNAME_GROUP;
                    Data.PlantCode = model.PlantCode;
                    Data.DueTime = model.DueTime;

                    var i = db.SaveChanges();
                }
                else
                {
                    return Json(new { result = "no data !!", message = "request successfully" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "error", message = "request successfully" });
            }

            return Json(new { result = "insert success !!", message = "request successfully" });
        }

        public ActionResult DeleteSoldTo(int id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var Soldto = db.T_CUST_SOLD_TO.Where(x => x.Id == id).FirstOrDefault();

            db.T_CUST_SOLD_TO.Remove(Soldto);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteShipTo(string id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var Shipto = db.T_CUST_SHIP_TO_BY_CUSTID.Where(x => x.DockCode == id).FirstOrDefault();

            db.T_CUST_SHIP_TO_BY_CUSTID.Remove(Shipto);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddMds(string Id)//ดึง data ขึ้นมา show
        {
            return View();
        }

        public JsonResult insMds(T_UPLOAD_SO_TO_SAP model)
        {
            string msg = "";
            var db = new TKC_MASTER_SAPEntities();
            TKC_MASTER_SAPEntities dbcontext = new TKC_MASTER_SAPEntities();
            try
            {
                T_UPLOAD_SO_TO_SAP insMat = new T_UPLOAD_SO_TO_SAP();


                insMat.CUST_ID = model.CUST_ID;
                insMat.DATATYPE = model.DATATYPE;
                insMat.CUSTNAME_GROUP = model.CUSTNAME_GROUP;
                insMat.PlantCode = model.PlantCode;
                insMat.DueTime = model.DueTime;
                insMat.Type = model.Type;
                insMat.Rem1 = model.Rem1;
                insMat.Rem2 = model.Rem2;

                //model.Channel = "10"; สามารถfixedค่าได้
                dbcontext.T_UPLOAD_SO_TO_SAP.Add(insMat);
                var i = dbcontext.SaveChanges();

                msg = "Success!!";

            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                msg = "error !!";
            }
            return Json(new { result = msg, message = "request successfully" });
        }

        public ActionResult DeleteMds(int id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var mds = db.T_UPLOAD_SO_TO_SAP.Where(x => x.Id == id).FirstOrDefault();

            db.T_UPLOAD_SO_TO_SAP.Remove(mds);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToPrones(string Id)//ดึง data ขึ้นมา show
        {
            TKC_MASTER_SAPEntities db = new TKC_MASTER_SAPEntities();

            var name = db.T_DATA_TO_PRONES.ToList();
            List<ListDropDown> Name = new List<ListDropDown>();
            foreach (var item in name.Where(x => x.flag == 1).OrderBy(x => x.DATA_NAME).Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.DATA_NAME;
                iTmp.iValue = item.DATA_SOURCE;
                Name.Add(iTmp);
            }
            ViewBag.name = Name;

            var model = db.V_SapMat_Item_Prones.OrderBy(x => x.I_MODEL).ToList();
            List<ListDropDown> Model = new List<ListDropDown>();
            var disModel = model.Select(x => x.I_MODEL).Distinct().ToList();
            foreach (var item in disModel)
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item;
                iTmp.iValue = item;
                Model.Add(iTmp);
            }
            ViewBag.model = Model;
            return View();
        }

        public async System.Threading.Tasks.Task<JsonResult> ExcelItemMaster(string Model) //////////mat master
        {
            var db = new TKC_MASTER_SAPEntities();
            var conn = db.Database.Connection;
            //var data = db.v_sap_mat_to_sap.ToList();
            var dt = new DataTable();
            var fileName = User.Identity.Name + "_Item MASTER" + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                await conn.OpenAsync();
                DbDataReader reader;
                using (var command = conn.CreateCommand())
                {
                    string sql = "SELECT  * FROM V_SAPMat_2_Prones__ItemMaster WHERE I_MODEL = '" + Model + "' ";
                    command.CommandText = sql;
                    command.CommandTimeout = 0;

                    //DbParameter param1 = command.CreateParameter();
                    //param1.ParameterName = "@PROD_DATE";
                    //param1.Value = models.PRD_DATE.ToString("yyyy-MM-dd");
                    //command.Parameters.Add(param1);

                    //DbParameter param3 = command.CreateParameter();
                    //param3.ParameterName = "@LINE";
                    //param3.Value = models.LINE;
                    //command.Parameters.Add(param3);

                    reader = await command.ExecuteReaderAsync();
                    dt.Load(reader);
                    reader.Close();
                }
                string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);
                FileStream file = new FileStream(fullPath, FileMode.Create, FileAccess.Write);

                //Create column and inser rows
                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add(dt, "ItemMaster");
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(file);
                        file.Close();
                    }

                    /* var mm = db.T_SAP_MAT_SETUP.Where(x => x.I_Approve_MK != null
                     && x.I_Approve_PPC != null && x.I_Approve_LG != null && x.I_Approve_CO != null
                     && x.Mat_Exported == null).ToList();

                     foreach (var item in mm)
                     {
                         item.Mat_Exported = "1";
                         item.Mat_ExpBy = User.Identity.Name;
                         item.Mat_ExpDate = DateTime.Now;
                         var i = db.SaveChanges();
                     }*/
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                conn.Close();
            }
            return Json(new { fileName = fileName, errorMessage = "" });
        }

        public async System.Threading.Tasks.Task<JsonResult> ExcelSoldTo() //Process Master
        {
            var db = new TKC_MASTER_SAPEntities();
            var conn = db.Database.Connection;

            var dt = new DataTable();
            var fileName = User.Identity.Name + "_Soldto" + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                await conn.OpenAsync();
                DbDataReader reader;
                using (var command = conn.CreateCommand())
                {
                    string sql = "SELECT  * FROM T_CUST_SOLD_TO";
                    command.CommandText = sql;
                    command.CommandTimeout = 0;

                    //DbParameter param1 = command.CreateParameter();
                    //param1.ParameterName = "@PROD_DATE";
                    //param1.Value = models.PRD_DATE.ToString("yyyy-MM-dd");
                    //command.Parameters.Add(param1);

                    //DbParameter param3 = command.CreateParameter();
                    //param3.ParameterName = "@LINE";
                    //param3.Value = models.LINE;
                    //command.Parameters.Add(param3);

                    reader = await command.ExecuteReaderAsync();
                    dt.Load(reader);
                    reader.Close();
                }
                string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);
                FileStream file = new FileStream(fullPath, FileMode.Create, FileAccess.Write);

                //Create column and inser rows
                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add(dt, "SoldTo");
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(file);
                        file.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
            return Json(new { fileName = fileName, errorMessage = "" });
        }


        public async System.Threading.Tasks.Task<JsonResult> ExcelShipTo() //Process Master
        {
            var db = new TKC_MASTER_SAPEntities();
            var conn = db.Database.Connection;

            var dt = new DataTable();
            var fileName = User.Identity.Name + "_Shipto" + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                await conn.OpenAsync();
                DbDataReader reader;
                using (var command = conn.CreateCommand())
                {
                    string sql = "SELECT  * FROM T_CUST_SHIP_TO_BY_CUSTID";
                    command.CommandText = sql;
                    command.CommandTimeout = 0;

                    //DbParameter param1 = command.CreateParameter();
                    //param1.ParameterName = "@PROD_DATE";
                    //param1.Value = models.PRD_DATE.ToString("yyyy-MM-dd");
                    //command.Parameters.Add(param1);

                    //DbParameter param3 = command.CreateParameter();
                    //param3.ParameterName = "@LINE";
                    //param3.Value = models.LINE;
                    //command.Parameters.Add(param3);

                    reader = await command.ExecuteReaderAsync();
                    dt.Load(reader);
                    reader.Close();
                }
                string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);
                FileStream file = new FileStream(fullPath, FileMode.Create, FileAccess.Write);

                //Create column and inser rows
                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add(dt, "ShipTo");
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(file);
                        file.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
            return Json(new { fileName = fileName, errorMessage = "" });
        }

        public async System.Threading.Tasks.Task<JsonResult> ExcelProcessMaster(string Model) //Process Master
        {
            var db = new TKC_MASTER_SAPEntities();
            var conn = db.Database.Connection;

            var dt = new DataTable();
            var fileName = User.Identity.Name + "_Process Master" + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                await conn.OpenAsync();
                DbDataReader reader;
                using (var command = conn.CreateCommand())
                {
                    string sql = "SELECT  * FROM V_SAPMat_2_Prones__ProcessMaster WHERE I_MODEL = '" + Model + "' ";
                    command.CommandText = sql;
                    command.CommandTimeout = 0;

                    //DbParameter param1 = command.CreateParameter();
                    //param1.ParameterName = "@PROD_DATE";
                    //param1.Value = models.PRD_DATE.ToString("yyyy-MM-dd");
                    //command.Parameters.Add(param1);

                    //DbParameter param3 = command.CreateParameter();
                    //param3.ParameterName = "@LINE";
                    //param3.Value = models.LINE;
                    //command.Parameters.Add(param3);

                    reader = await command.ExecuteReaderAsync();
                    dt.Load(reader);
                    reader.Close();
                }
                string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);
                FileStream file = new FileStream(fullPath, FileMode.Create, FileAccess.Write);

                //Create column and inser rows
                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add(dt, "Process Master");
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(file);
                        file.Close();
                    }

                    /* var mm = db.T_SAP_MAT_SETUP.Where(x => x.I_Approve_MK != null
                     && x.I_Approve_PPC != null && x.I_Approve_LG != null && x.I_Approve_CO != null
                     && x.Mat_Exported == null).ToList();

                     foreach (var item in mm)
                     {
                         item.Mat_Exported = "1";
                         item.Mat_ExpBy = User.Identity.Name;
                         item.Mat_ExpDate = DateTime.Now;
                         var i = db.SaveChanges();
                     }*/
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
            return Json(new { fileName = fileName, errorMessage = "" });
        }

        public async System.Threading.Tasks.Task<JsonResult> ExcelCustomerItemMaster(string Model) //Customer Item Master
        {
            var db = new TKC_MASTER_SAPEntities();
            var conn = db.Database.Connection;

            var dt = new DataTable();
            var fileName = User.Identity.Name + "_Customer Item Master" + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                await conn.OpenAsync();
                DbDataReader reader;
                using (var command = conn.CreateCommand())
                {
                    string sql = "SELECT  * FROM V_SAPMat_2_Prones__CustomerItemMaster WHERE I_MODEL = '" + Model + "' ";
                    command.CommandText = sql;
                    command.CommandTimeout = 0;

                    //DbParameter param1 = command.CreateParameter();
                    //param1.ParameterName = "@PROD_DATE";
                    //param1.Value = models.PRD_DATE.ToString("yyyy-MM-dd");
                    //command.Parameters.Add(param1);

                    //DbParameter param3 = command.CreateParameter();
                    //param3.ParameterName = "@LINE";
                    //param3.Value = models.LINE;
                    //command.Parameters.Add(param3);

                    reader = await command.ExecuteReaderAsync();
                    dt.Load(reader);
                    reader.Close();
                }
                string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);
                FileStream file = new FileStream(fullPath, FileMode.Create, FileAccess.Write);

                //Create column and inser rows
                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add(dt, "Customer Item Master");
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(file);
                        file.Close();
                    }

                    /*var mm = db.T_SAP_MAT_SETUP.Where(x => x.I_Approve_MK != null
                    && x.I_Approve_PPC != null && x.I_Approve_LG != null && x.I_Approve_CO != null
                    && x.Mat_Exported == null).ToList();

                    foreach (var item in mm)
                    {
                        item.Mat_Exported = "1";
                        item.Mat_ExpBy = User.Identity.Name;
                        item.Mat_ExpDate = DateTime.Now;
                        var i = db.SaveChanges();
                    }*/
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
            return Json(new { fileName = fileName, errorMessage = "" });
        }

        public async System.Threading.Tasks.Task<JsonResult> ExcelBOMMaster(string Model) //BOM Master
        {
            var db = new TKC_MASTER_SAPEntities();
            var conn = db.Database.Connection;

            var dt = new DataTable();
            var fileName = User.Identity.Name + "_BOM Master" + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            try
            {
                await conn.OpenAsync();
                DbDataReader reader;
                using (var command = conn.CreateCommand())
                {
                    string sql = "SELECT  * FROM V_SAPMat_2_Prones_BOMMaster WHERE I_MODEL = '" + Model + "'";
                    command.CommandText = sql;
                    command.CommandTimeout = 0;

                    //DbParameter param1 = command.CreateParameter();
                    //param1.ParameterName = "@PROD_DATE";
                    //param1.Value = models.PRD_DATE.ToString("yyyy-MM-dd");
                    //command.Parameters.Add(param1);

                    //DbParameter param3 = command.CreateParameter();
                    //param3.ParameterName = "@LINE";
                    //param3.Value = models.LINE;
                    //command.Parameters.Add(param3);

                    reader = await command.ExecuteReaderAsync();
                    dt.Load(reader);
                    reader.Close();
                }
                string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);
                FileStream file = new FileStream(fullPath, FileMode.Create, FileAccess.Write);

                //Create column and inser rows
                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add(dt, "BOM Master");
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(file);
                        file.Close();
                    }

                    /*var mm = db.T_SAP_MAT_SETUP.Where(x => x.I_Approve_MK != null
                    && x.I_Approve_PPC != null && x.I_Approve_LG != null && x.I_Approve_CO != null
                    && x.Mat_Exported == null).ToList();

                    foreach (var item in mm)
                    {
                        item.Mat_Exported = "1";
                        item.Mat_ExpBy = User.Identity.Name;
                        item.Mat_ExpDate = DateTime.Now;
                        var i = db.SaveChanges();
                    }*/
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
            return Json(new { fileName = fileName, errorMessage = "" });
        }

        public DataTable ConvertToDataTable<T>(IList<T> data) //เอา data เข้า table
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));

            DataTable table = new DataTable();

            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);

            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }
                table.Rows.Add(row);
            }
            return table;
        }

        [HttpGet]
        [DeleteFileAttribute] //Action Filter, it will auto delete the file after download, 
                              //I will explain it later
        public ActionResult Download(string file)
        {
            //get the temp folder and file path in server
            string fullPath = Path.Combine(Server.MapPath("~/temp"), file);

            //return the file for download, this is an Excel 
            //so I set the file content type to "application/vnd.ms-excel"
            return File(fullPath, "application/vnd.ms-excel", file);
        }

        public class DeleteFileAttribute : ActionFilterAttribute
        {
            public override void OnResultExecuted(ResultExecutedContext filterContext)
            {
                filterContext.HttpContext.Response.Flush();
                //convert the current filter context to file and get the file path
                string filePath = (filterContext.Result as FilePathResult).FileName;
                //delete the file after download
                System.IO.File.Delete(filePath);
            }
        }
    }
}