﻿using SAP_MAT_SETUP.CustomAuthentication;
using SAP_MAT_SETUP.Database;
using SAP_MAT_SETUP.Models;
using SAP_MAT_SETUP.MultipleModels;
using SAP_MAT_SETUP.Users;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace SAP_MAT_SETUP.Controllers
{
    public class LogisticController : Controller
    {
        // GET: Logistic
        string errorMsg;

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult NGList()
        {
            var db = new TKC_MASTER_SAPEntities();
            var RegID = db.T_SAP_MAT_SETUP.Where(x => x.I_Status_LG == null).OrderByDescending(c => c.I_RegID).ToList();
            return View(RegID);
        }

        [CustomAuthorize(Roles = "LG")]
        public ActionResult Logistic(string Id, string MatN)
        {
            var db = new TKC_MASTER_SAPEntities();
            var list = db.T_SAPMAT_SELLING_TYPE_PACKAGE.Where(x => x.M_MatNo == MatN).FirstOrDefault();


            var Mat = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == Id).FirstOrDefault();

            Logistic tmp = new Logistic();
            tmp.I_RegID = Mat.I_RegID;
            tmp.M_Model = Mat.M_Model;
            tmp.M_Lineoff = Mat.M_Lineoff;
            tmp.M_MatNo = Mat.M_MatNo;
            tmp.M_MatDesc = Mat.M_MatDesc;
            tmp.L_BoxQty = Mat.L_BoxQty;

            /* tmp.OEM = list.OEM;
             tmp.Part_Center = list.Part_Center;
             tmp.Export = list.Export;
             tmp.ComExport = list.ComExport;*/

            var Shipto = db.T_SAPMAT_SELLING_PLANT1.Where(x => x.I_RegID == Id).OrderBy(x => x.Ship_To).ToList();
            List<ListDropDownMatG> proc = new List<ListDropDownMatG>();
            foreach (var item in Shipto.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.Ship_To;
                iTmp.iValue = item.Ship_To;
                proc.Add(iTmp);
            }
            ViewBag.Shipto = proc;

            T_SAPMAT_SELLING_TYPE_PACKAGE tt = new T_SAPMAT_SELLING_TYPE_PACKAGE();
            if (list == null)
            {
                /*tt.OEM = 0;*/
                tt.Part_Center = 0;
                tt.Export = 0;
                tt.ComExport = 0;
            }
            else
            {
                /*tmp.OEM = list.OEM;*/
                tmp.Part_Center = list.Part_Center;
                tmp.Export = list.Export;
                tmp.ComExport = list.ComExport;
            }



            var shipto = db.T_SAPMAT_SELLING_PLANT1.Where(x => x.I_RegID == Id).OrderBy(x => x.Plant).ToList();
            if (shipto != null)
            {
                ViewBag.lstLG = shipto;
            }



                return View(tmp);
        }

        [HttpPost]
        public JsonResult updateLogistic(T_SAP_MAT_SETUP model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var db = new TKC_MASTER_SAPEntities();
                    var Data = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == model.I_RegID).FirstOrDefault();

                    if (Data != null)
                    {
                        Data.L_BoxQty = model.L_BoxQty;
                        Data.I_Status_LG = "OK";
                        Data.LastUpdBy = User.Identity.Name;
                        Data.LastUpdDate = DateTime.Now;
                        if (Data.I_Approve_LG != null)
                        {
                            Data.I_Approve_LG = null;
                        }

                        Data.Mat_Exported = null;
                        Data.Mat_ExpBy = null;
                        Data.Mat_ExpDate = null;

                        Data.Mat_Exported = null;
                        Data.Mat_ExpBy = null;
                        Data.Mat_ExpDate = null;
                        Data.Mat_Revise = null;

                        var i = db.SaveChanges();
                        getLGOK(model.I_RegID);
                        getPartOK(model.I_RegID);
                    }
                    else
                    {
                        return Json(new { result = "no data !!", message = "request successfully" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { result = "error", message = "request successfully" });
                }
            }
            else
            {
                return Json(new { result = "input data!!", message = "request successfully" });
            }
            return Json(new { result = "insert success !!", message = "request successfully" });
        }

        public void getPartOK(string TkcPart) //check status
        {
            var db = new TKC_MASTER_SAPEntities();
            var Customer = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == TkcPart)
                .FirstOrDefault();

            if (Customer.I_Status_MK == "OK")
            {
                if (Customer.I_Status_PC == "OK")
                {
                    if (Customer.I_Status_LG == "OK")
                    {
                        if (Customer.I_Status_CO == "OK")
                        {
                            Customer.I_Status = "OK";
                            db.SaveChanges();
                        }
                        else
                        {
                            Customer.I_Status = "NG";
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        Customer.I_Status = "NG";
                        db.SaveChanges();
                    }
                }
                else
                {
                    Customer.I_Status = "NG";
                    db.SaveChanges();
                }
            }
            else
            {
                Customer.I_Status = "NG";
                db.SaveChanges();
            }
        }

        [HttpPost]
        public JsonResult getCustName(string Customers)
        {
            var db = new TKC_MASTER_SAPEntities();
            var Customer = db.T_SAPMAT_Cust_MS.Where(x => x.CustCD == Customers)
                .FirstOrDefault();
            //.OrderBy(x => x.iValue)
            return Json(new { result = Customer.CustName, message = "request successfully" });
        }

        public JsonResult getShipToName(string M_MatNo)
        {
            TKC_MASTER_SAPEntities db = new TKC_MASTER_SAPEntities();
            var Shipto = db.V_Customer_Package.ToList();

            List<ListDropDown> Model = new List<ListDropDown>();
            foreach (var item in Shipto.Where(x => x.M_MatNo == M_MatNo).OrderBy(x => x.Cust_ID).Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.Cust_ID + " " + item.CUST_NAME;
                iTmp.iValue = item.Cust_ID;
                Model.Add(iTmp);
            }
            ViewBag.Shipto = Model;

            return Json(new { result = ViewBag.Shipto, message = "request successfully" });
        }

        [HttpPost]
        public JsonResult updateMatBoxQty(T_SAP_MAT_SETUP model)
        {
            try
            {
                
                var db = new TKC_MASTER_SAPEntities();
                var user = new AuthenticationDB();
                TKC_MASTER_SAPEntities dbcontext = new TKC_MASTER_SAPEntities();
                T_SAP_MAT_SETUP lstinsert = new T_SAP_MAT_SETUP();

                var Upd = dbcontext.T_SAP_MAT_SETUP.Where(x => x.I_RegID == model.I_RegID).FirstOrDefault();
                var matNo = dbcontext.T_SAP_MAT_SETUP.Where(y => y.I_RegID == model.I_RegID).FirstOrDefault();

                if (Upd != null)
                {
                    //checking before save

                    Upd.L_BoxQty = model.L_BoxQty;
                    if(Upd.I_Approve_LG != null)
                    {
                        Upd.I_Approve_LG = null;
                    }
                    ////////////////reset Price Export//////////////////////
                    //if (Upd.Price_Revise == null)
                    //{
                    //    Upd.Price_Revise = 0;
                    //}
                    //if (Upd.Price_Exported == "1")
                    //{
                    //    Upd.Price_Exported = null;
                    //    Upd.Price_Revise += 1;
                    //}
                    //else
                    //{
                    //    Upd.Price_Revise = Upd.Price_Revise;
                    //}
                    //Upd.Price_ExpBy = null;
                    //Upd.Price_ExpDate = null;
                    ////////////////reset Mat Export//////////////////////
                    Upd.Mat_Exported = null;
                    Upd.Mat_ExpBy = null;
                    Upd.Mat_ExpDate = null;
                    //if (Upd.Mat_Revise == null)
                    //{
                    //    Upd.Mat_Revise = 0;
                    //}
                    //if (Upd.Mat_Exported == "1")
                    //{
                    //    Upd.Mat_Exported = null;
                    //    Upd.Mat_Revise += 1;
                    //}
                    //Upd.Mat_ExpBy = null;
                    //Upd.Mat_ExpDate = null;
                    ////////////////////////////////////



                    var getupdate = User.Identity.Name;
                    var getUserRole = user.Users.Where(x => x.Username == getupdate).FirstOrDefault();
                    var Save = new T_SAPMAT_REVISE_LOG();
                    Save.i_Reg_id = Upd.I_RegID;
                    Save.page = "Mat Master";
                    Save.RevNo = Upd.Price_Revise;
                    Save.updDept = getUserRole.Roles.FirstOrDefault().RoleFullName.ToString();
                    Save.updby = User.Identity.Name;
                    Save.updDate = DateTime.Now;
                    db.T_SAPMAT_REVISE_LOG.Add(Save);
                    var check = db.T_SAPMAT_SELLING_PLANT1.Where(y => y.I_RegID == Upd.I_RegID && y.L_BoxQty == null).ToList();
                    if (check == null)
                    {
                        getLGOK(model.I_RegID);
                        getPartOK(model.I_RegID);
                    }

                    var i = dbcontext.SaveChanges();
                    //msg = "sucess!!";


                    List<string> _Roles = new List<string> { "LG" };
                    var getemail = user.Users.Where(x => (x.Roles.Any(y => _Roles.Contains(y.RoleName)))).ToList();

                    foreach (var items in getemail)
                    {
                        string html = "<span style='font-family:Tahoma;font-size: 13pt;color:#1E90FF;'>";
                        html += "Dear  All concerns<br>";
                        html += "Logistic have update in SAP Material System.<br>";
                        html += "I_RegID " + Upd.I_RegID + "<br>";
                        html += "Model " + Upd.M_Model + "<br>";
                        html += "Material number " + Upd.M_MatNo + "<br>";
                        html += "Please check and prepare the information that related to you within due date.<br>";
                        html += "TKC <br>";
                        html += "System Development. <br>";
                        html += "Tel. 109";
                        html += "</span>";
                        NotificationEmail(items.Email, "SAP Material System have update", html);
                    }

                    //return Json(new { result = msg, message = "request successfully" });
                    return Json(new { isValid = true, result = "insert success !!", message = "request successfully" });
                }

                else
                {
                    //return Json("Error");
                    return Json(new { isValid = false, result = "no data !!", message = "no data to send email !!" });
                }

            }
            catch (Exception ex)
            {
                //foreach (var eve in e.EntityValidationErrors)
                //{
                //    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                //        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                //    foreach (var ve in eve.ValidationErrors)
                //    {
                //        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                //            ve.PropertyName, ve.ErrorMessage);
                //    }
                //}
                errorMsg = ex.Message.ToString();
                //return Json("Error");
                return Json(new { isValid = false, result = "error !!", message = errorMsg });
            }
            
        }
        public void getLGOK(string TkcPart) //check status
        {
            var db = new TKC_MASTER_SAPEntities();
            var Customer = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == TkcPart).FirstOrDefault();
            var Sellplant = db.T_SAPMAT_SELLING_PLANT1.Where(x => x.I_RegID == TkcPart).FirstOrDefault();
            /*var package = db.T_SAPMAT_SELLING_TYPE_PACKAGE.Where(y => y.M_MatNo == matNo).FirstOrDefault();
*/
            //if (Customer.L_BoxQty != null && Sellplant.L_BoxQty != null) // true
            //{
            //    Customer.I_Status_LG = "OK";
            //    Customer.I_Approve_LG = null;
            //    db.SaveChanges();
            //}
            //else
            //{
            //    Customer.I_Status_LG = "NG";
            //    db.SaveChanges();
            //}

            if (Customer.L_BoxQty != null)
            {
                if (Sellplant == null)
                {

                }

                else
                {
                    Customer.I_Status_LG = "OK";
                    Customer.I_Approve_LG = null;
                    db.SaveChanges();
                }
            }

            else
            {
                Customer.I_Status_LG = "NG";
                db.SaveChanges();
            }
        }

        public JsonResult insPKSelling(T_SAPMAT_SELLING_TYPE_PACKAGE model)
        {
            string msg = "";
            var db = new TKC_MASTER_SAPEntities();
            TKC_MASTER_SAPEntities dbcontext = new TKC_MASTER_SAPEntities();

            /* var CheckPk_CD = dbcontext.T_SAPMAT_SELLING_TYPE_PACKAGE.Where(x => x.seq.Substring(0, 1) == x.seq.Substring(0, 1))
                                                               .OrderByDescending(y => y.seq).FirstOrDefault();*/
            try
            {
                T_SAPMAT_SELLING_TYPE_PACKAGE insMat = new T_SAPMAT_SELLING_TYPE_PACKAGE();

                insMat.M_MatNo = model.M_MatNo;
                insMat.OEM = model.OEM;
                insMat.Part_Center = model.Part_Center;
                insMat.Export = model.Export;
                insMat.ComExport = model.ComExport;
                insMat.UpdBy = User.Identity.Name;
                insMat.UpdDate = DateTime.Now;



                /*getLGOK(model.I_RegID);*/

                //model.Channel = "10"; สามารถfixedค่าได้
                dbcontext.T_SAPMAT_SELLING_TYPE_PACKAGE.Add(insMat);
                var i = dbcontext.SaveChanges();
                msg = "Success!!";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                msg = "error !!";
            }
            return Json(new { result = msg, message = "request successfully" });
        }

        public JsonResult updatePKSelling(List<T_SAPMAT_SELLING_PLANT1> model) // update data ลง sql
        {
            try
            {
                var user = new AuthenticationDB();
                var db = new TKC_MASTER_SAPEntities();
                var c = true;
                foreach (var item in model)
                {
                    var Data = db.T_SAPMAT_SELLING_PLANT1.Where(x => x.I_RegID == item.I_RegID && x.Cust_ID == item.Cust_ID && x.Plant == item.Plant).FirstOrDefault();
                    var setup = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == item.I_RegID).FirstOrDefault();
                    TempData["getid"] = Data.I_RegID;
                    if (Data != null)
                    {

                        Data.L_BoxQty = item.L_BoxQty;
                        Data.L_BoxQtyType = item.L_BoxQtyType;
                        Data.UpdBy = User.Identity.Name;
                        Data.UpdDate = DateTime.Now;
                        setup.I_Status_LG = "OK";
                        if (setup.I_Approve_LG != null)
                        {
                            setup.I_Approve_LG = null;
                        }

                        if (Data.Revise == null)
                        {
                            Data.Revise = 0;
                        }
                        if (Data.Exported == "1")
                        {

                            Data.Revise += 1;
                        }
                        else
                        {
                            Data.Revise = Data.Revise;
                        }
                        Data.Exported = null;
                        Data.ExpBy = null;
                        Data.ExpDate = null;

                        var Save = new T_SAPMAT_REVISE_LOG();
                        var getupdate = User.Identity.Name;
                        var getUserRole = user.Users.Where(x => x.Username == getupdate).FirstOrDefault();
                        if(c == true)
                        {
                            Save.i_Reg_id = Data.I_RegID;
                            Save.page = "Ship To";
                            Save.RevNo = Data.Revise;
                            Save.updDept = getUserRole.Roles.FirstOrDefault().RoleFullName.ToString();
                            Save.updby = User.Identity.Name;
                            Save.updDate = DateTime.Now;
                            db.T_SAPMAT_REVISE_LOG.Add(Save);
                            c = false;
                        }
                        
                        var i = db.SaveChanges();
                        getLGOK(item.I_RegID);

                        TempData["I_RegID"] = setup.I_RegID;
                        TempData["M_Model"] = setup.M_Model;
                        TempData["M_MatNo"] = setup.M_MatNo;
                        
                    }
                    else
                    {
                        return Json(new { result = "no data !!", message = "request successfully" });
                    }
                }
                List<string> _Roles = new List<string> { "LG"};
                var getemail = user.Users.Where(x => (x.Roles.Any(y => _Roles.Contains(y.RoleName)))).ToList();

                foreach (var items in getemail)
                {
                    string html = "<span style='font-family:Tahoma;font-size: 13pt;color:#1E90FF;'>";
                    html += "Dear  All concerns<br>";
                    html += "Logistic have update Ship to in SAP Material System.<br>";
                    html += "I_RegID :" + TempData["I_RegID"] + "<br>";
                    html += "Model :" + TempData["M_Model"] + "<br>";
                    html += "Material number :" + TempData["M_MatNo"] + "<br>";
                    html += "Please check and prepare the information that related to you within due date.<br>";
                    html += "TKC <br>";
                    html += "System Development. <br>";
                    html += "Tel. 109";
                    html += "</span>";
                    NotificationEmail(items.Email, "SAP Material System have update", html);
                }

                return Json(new { isValid = true, result = "insert success !!", message = "request successfully" });
            }
            catch (Exception ex)
            {
                //foreach (var eve in ex.EntityValidationErrors)
                //{
                //    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                //        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                //    foreach (var ve in eve.ValidationErrors)
                //    {
                //        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                //            ve.PropertyName, ve.ErrorMessage);
                //    }
                //}
                errorMsg = ex.Message.ToString();
                return Json(new { isValid = false, result = "error !!", message = errorMsg });
            }

            //return Json(new { result = "Update Complete!!", message = "request successfully" });
        }

        public ActionResult ApproveLG()
        {
            var db = new TKC_MASTER_SAPEntities();
            var items = db.T_SAP_MAT_SETUP.Where(c => c.I_Status_LG == "OK").Where(x => x.I_Approve_LG == null && x.L_BoxQty != null).ToList();
            return View(items);
        }

        [HttpPost]
        public JsonResult updateApproveLG(List<ListDropDown> listkey)
        {
            try
            {
                using (var db = new TKC_MASTER_SAPEntities())
                {
                    foreach (var item in listkey)
                    {
                        var update = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == item.iKey).FirstOrDefault();
                        var check = db.T_SAPMAT_SELLING_PLANT1.Where(x => x.I_RegID == item.iKey && x.L_BoxQty == null).ToList();
                        if (update != null)
                        {
                            if (check.Count == 0 && update.L_BoxQty != null)
                            {
                                update.I_Approve_LG = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"); ;
                                db.SaveChanges();
                            }
                            else
                            {
                                return Json(new { result = "ผิดพลาด, กรุณาใส่ข้อมูลจำนวนกล่องของลูกค้า", message = "request successfully" });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "error", message = "request successfully" });
            }
            return Json(new { result = "success !!", message = "request successfully" });
        }

        [CustomAuthorize(Roles = "LG")]
        public ActionResult PKSelling()
        {
            var db = new TKC_MASTER_SAPEntities();
            var PKSelling = db.T_SAPMAT_SELLING_TYPE_PACKAGE.ToList();
            return View(PKSelling);
        }
        [CustomAuthorize(Roles = "LG")]
        public ActionResult EditPKSelling(string M_MatNo)//ดึง data ขึ้นมา show
        {
            var db = new TKC_MASTER_SAPEntities();
            var PKSelling = db.T_SAPMAT_SELLING_TYPE_PACKAGE.Where(x => x.M_MatNo == M_MatNo).FirstOrDefault();

            var mat = db.V_Customer_Package.ToList();
            List<ListDropDown> Mat = new List<ListDropDown>();
            foreach (var item in mat.OrderBy(x => x.M_MatNo).Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.M_MatNo;
                iTmp.iValue = item.M_MatNo;
                Mat.Add(iTmp);
            }
            ViewBag.mat = Mat.Distinct();

            var Shipto = db.V_Customer_Package.ToList();
            List<ListDropDown> ShipTo = new List<ListDropDown>();
            foreach (var item in Shipto.Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.CUST_NAME;
                iTmp.iValue = item.CUST_NAME;
                ShipTo.Add(iTmp);
            }
            ViewBag.Shipto = ShipTo;

            return View(PKSelling);
        }

        [CustomAuthorize(Roles = "LG")]
        public ActionResult AddPKSelling()
        {
            var db = new TKC_MASTER_SAPEntities();
            var data = db.T_SAPMAT_SELLING_TYPE_PACKAGE.ToList();

            var mat = db.V_Customer_Package.OrderBy(x => x.M_MatNo).ToList();
            List<ListDropDown> Mat = new List<ListDropDown>();
            var disMat = mat.Select(x => x.M_MatNo).Distinct().ToList();
            foreach (var item in disMat)
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item;
                iTmp.iValue = item;
                Mat.Add(iTmp);
            }
            ViewBag.mat = Mat;

            var Shipto = db.V_Customer_Package.ToList();
            List<ListDropDown> ShipTo = new List<ListDropDown>();
            foreach (var item in Shipto.Distinct())
            {
                ListDropDown iTmp = new ListDropDown();
                iTmp.iKey = item.Cust_ID + " " + item.CUST_NAME;
                iTmp.iValue = item.CUST_NAME;
                ShipTo.Add(iTmp);
            }
            ViewBag.Shipto = ShipTo;

            return View();
        }

        public ActionResult IMCT_PART_INPUT_QTY()
        {
            var db = new SZYSPISDBEntities();
            var inputQtyList = db.T_IMCT_PART_INPUT_QTY.ToList();
            var inputQtyList2 = db.T_CustID_MS.ToList();
            var inputQtyList3 = db.T_SPIS_PATH_GTOPAS.ToList();

            INPUT_QTY_MODEL model = new INPUT_QTY_MODEL()
            {
                t_IMCT_PART_INPUT_QTY = inputQtyList,
                t_CustID_MS = inputQtyList2,
                t_SPIS_PATH_GTOPAS = inputQtyList3
            };

            return View(model);
        }
        [HttpPost]
        public JsonResult addInputQty(string custpartNo)
        {
            try
            {
                using (var db = new SZYSPISDBEntities())
                {
                    var chkDup = db.T_IMCT_PART_INPUT_QTY.Where(x => x.CustPartNo == custpartNo).FirstOrDefault();

                    if (chkDup == null)
                    {
                        var inputQty = new T_IMCT_PART_INPUT_QTY();
                        inputQty.CustPartNo = custpartNo;

                        db.T_IMCT_PART_INPUT_QTY.Add(inputQty);
                        db.SaveChanges();
                    }
                    else
                        return Json(new { result = "already add !!", message = "request successfully" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "error", message = "request successfully" });
            }
            return Json(new { result = "add success !!", message = "request successfully" });
        }
        public JsonResult addInputQty2(string custId)
        {
            try
            {
                using (var db = new SZYSPISDBEntities())
                {
                    var chkDup = db.T_CustID_MS.Where(x => x.CUSTID == custId).FirstOrDefault();

                    if (chkDup == null)
                    {
                        var inputQty = new T_CustID_MS();
                        inputQty.CUSTID = custId;

                        db.T_CustID_MS.Add(inputQty);
                        db.SaveChanges();
                    }
                    else
                        return Json(new { result = "already add !!", message = "request successfully" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "error", message = "request successfully" });
            }
            return Json(new { result = "add success !!", message = "request successfully" });
        }
        public void NotificationEmail(string email, string subject, string body)
        {

            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress("prones_g@tkoito.co.th");
            msg.To.Add(email);
            msg.Subject = subject;
            msg.Body = body;
            msg.IsBodyHtml = true;

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Port = 25;
            smtpClient.Host = "172.18.1.2";
            smtpClient.Credentials = new NetworkCredential("prones", "zaq1@wsx");
            smtpClient.Send(msg);
        }
        public JsonResult addInputQty3(string partNo)
        {
            try
            {
                using (var db = new SZYSPISDBEntities())
                {
                    var chkDup = db.T_SPIS_PATH_GTOPAS.Where(x => x.PartNumber == partNo).FirstOrDefault();

                    if (chkDup == null)
                    {
                        var inputPath = new T_SPIS_PATH_GTOPAS();
                        inputPath.PartNumber = partNo;

                        db.T_SPIS_PATH_GTOPAS.Add(inputPath);
                        db.SaveChanges();
                    }
                    else
                        return Json(new { result = "already add !!", message = "request successfully" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "error", message = "request successfully" });
            }
            return Json(new { result = "add success !!", message = "request successfully" });
        }
    }
}