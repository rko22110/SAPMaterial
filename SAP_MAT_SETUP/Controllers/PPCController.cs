﻿using Oracle.ManagedDataAccess.Client;
using SAP_MAT_SETUP.CustomAuthentication;
using SAP_MAT_SETUP.Database;
using SAP_MAT_SETUP.Models;
using SAP_MAT_SETUP.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace SAP_MAT_SETUP.Controllers
{
    public class PPCController : Controller
    {
        string errorMsg, errorProp;
        // GET: Purchase
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult NGList()
        {
            var db = new TKC_MASTER_SAPEntities();
            var RegID = db.T_SAP_MAT_SETUP.Where(x => x.I_Status_PC == null).OrderByDescending(c => c.I_RegID).ToList();
            return View(RegID);
        }

        [CustomAuthorize(Roles = "PPC")]
        public ActionResult Purchase(string Id)
        {
            var db = new TKC_MASTER_SAPEntities();
            //Id = "2102-002";
            var Mat = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == Id).FirstOrDefault();
            Purchase tmp = new Purchase();
            tmp.I_RegID = Mat.I_RegID;
            tmp.M_Model = Mat.M_Model;
            tmp.M_Lineoff = Mat.M_Lineoff;
            tmp.M_MatNo = Mat.M_MatNo;
            tmp.M_MatDesc = Mat.M_MatDesc;
            tmp.M_Remark = Mat.M_Remark;
            tmp.P_Process = Mat.P_Process;
            tmp.P_Spec = Mat.P_Spec;
            tmp.P_KBNo = Mat.P_KBNo;
            tmp.P_Kb2 = Mat.P_Kb2;
            tmp.P_FacNo = Mat.P_FacNo;
            tmp.P_TkcPName = Mat.P_TkcPName;

            var Process = db.T_SAPMAT_Process.ToList();
            List<ListDropDownMatG> proc = new List<ListDropDownMatG>();
            foreach (var item in Process.Distinct())
            {
                ListDropDownMatG iTmp = new ListDropDownMatG();
                iTmp.iKey = item.ProcessCD + " " + item.ProcessName;
                iTmp.iValue = item.ProcessCD;
                proc.Add(iTmp);
            }
            ViewBag.Process = proc;
            return View(tmp);
        }

        [HttpPost]
        public JsonResult updatePurchase(T_SAP_MAT_SETUP model)
        {
            try
            {
                var user = new AuthenticationDB();
                var db = new TKC_MASTER_SAPEntities();
                var Data = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == model.I_RegID).FirstOrDefault();

                Purchase tmp = new Purchase();
                //tmp.I_RegID = Data.I_RegID;
                tmp.M_MatNo = Data.M_MatNo;

                if (Data != null)
                {
                    Data.P_Process = model.P_Process;
                    Data.P_Spec = model.P_Spec;
                    //if (Data.P_KBNo != model.P_KBNo )
                    //{
                    //    Data.Mat_Exported = null;
                    //    Data.Mat_ExpDate = null;
                    //    Data.Mat_ExpDate = null;
                    //}
                    Data.P_KBNo = model.P_KBNo;
                    Data.P_Kb2 = model.P_Kb2;
                    Data.P_FacNo = model.P_FacNo;

                    if (model.P_Spec == "-")
                    {
                        Data.M_MatDesc = tmp.M_MatNo + "-" + model.P_Process;
                    }
                    else
                    {
                        Data.M_MatDesc = tmp.M_MatNo + "-" + model.P_Process + model.P_Spec;
                    }

                    Data.P_TkcPName = model.P_TkcPName;
                    //Data.I_Status_PC = "OK";
                    Data.LastUpdBy = User.Identity.Name;
                    Data.LastUpdDate = DateTime.Now;
                    if (Data.I_Approve_PPC != null)
                    {
                        Data.I_Approve_PPC = null;
                    }
                    ////////////////reset Price Export//////////////////////
                    //if (Data.Price_Revise == null)
                    //{
                    //    Data.Price_Revise = 0;
                    //}
                    //if (Data.Price_Exported == "1")
                    //{

                    //    Data.Price_Exported = null;
                    //    Data.Price_Revise += 1;
                    //}
                    //else
                    //{
                    //    Data.Price_Revise = Data.Price_Revise;
                    //}

                    //Data.Price_ExpBy = null;
                    //Data.Price_ExpDate = null;
                    ////////////////reset Mat Export//////////////////////
                    Data.Mat_Exported = null;
                    Data.Mat_ExpBy = null;
                    Data.Mat_ExpDate = null;
                    //if (Data.Mat_Revise == null)
                    //{
                    //    Data.Mat_Revise = 0;
                    //}
                    //if (Data.Mat_Exported == "1")
                    //{
                    //    Data.Mat_Exported = null;
                    //    Data.Mat_Revise += 1;
                    //}
                    //else
                    //{
                    //    Data.Mat_Revise = Data.Mat_Revise;
                    //}
                    //Data.Mat_ExpBy = null;
                    //Data.Mat_ExpDate = null;
                    ////////////////////////////////////

                    var getupdate = User.Identity.Name;
                    var getUserRole = user.Users.Where(x => x.Username == getupdate).FirstOrDefault();
                    var Save = new T_SAPMAT_REVISE_LOG();
                    Save.i_Reg_id = Data.I_RegID;
                    Save.page = "Mat Master";
                    Save.RevNo = Data.Price_Revise;
                    Save.updDept = getUserRole.Roles.FirstOrDefault().RoleFullName.ToString();
                    Save.updby = User.Identity.Name;
                    Save.updDate = DateTime.Now;
                   

                    db.T_SAPMAT_REVISE_LOG.Add(Save);


                    var i = db.SaveChanges();
                    getPPCOK(model.I_RegID);
                    getPartOK(model.I_RegID);

                    List<string> _Roles = new List<string> { "PPC" };
                    var getemail = user.Users.Where(x => (x.Roles.Any(y => _Roles.Contains(y.RoleName)) && !x.Email.Contains("SSUTAWAN"))).ToList();

                    foreach (var items in getemail)
                    {
                        string html = "<span style='font-family:Tahoma;font-size: 13pt;color:#1E90FF;'>";
                        html += "Dear  All concerns<br>";
                        html += "PPC have update in SAP Material System.<br>";
                        html += "I_RegID " + Data.I_RegID + "<br>";
                        html += "Model " + Data.M_Model + "<br>";
                        html += "Material number " + Data.M_MatNo + "<br>";
                        html += "Please check and prepare the information that related to you within due date.<br>";
                        html += "TKC <br>";
                        html += "System Development. <br>";
                        html += "Tel. 109";
                        html += "</span>";
                        NotificationEmail(items.Email, "SAP Material System have update", html);
                    }


                    return Json(new {  isValid = true, result = "insert success !!", message = "request successfully" });
                    //return Json("OK");
                }
                else
                {
                    return Json(new { isValid = false, result = "no data !!", message = "no data to send email !!" });
                    //return Json("Error");
                }
            }
            catch (Exception ex)
            {
                //foreach (var eve in e.EntityValidationErrors)
                //{
                //    errorMsg = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                //        eve.Entry.Entity.GetType().Name, eve.Entry.State);

                //    Console.WriteLine(errorMsg);

                //    foreach (var ve in eve.ValidationErrors)
                //    {
                //        errorProp = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                //            ve.PropertyName, ve.ErrorMessage);

                //        Console.WriteLine(errorProp);
                //    }
                //}
                errorMsg = ex.Message.ToString();
                return Json(new { isValid = false, result = "error !!", message = errorMsg});
                //return Json("Not Found");
            }
        }

        public void getPPCOK(string TkcPart) //check status
        {
            var db = new TKC_MASTER_SAPEntities();
            var Customer = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == TkcPart)
                .FirstOrDefault();

            if (Customer.CUSTOMER != null) // true
            {
                if (Customer.P_Process != null)
                {
                    if (Customer.P_FacNo != null)
                    {
                        if (Customer.P_KBNo != null)
                        {
                            if (Customer.P_Kb2 != null)
                            {
                                if (Customer.P_TkcPName != null)
                                {
                                    Customer.I_Status_PC = "OK";
                                    db.SaveChanges();
                                }
                                else
                                {
                                    Customer.I_Status_PC = "NG";
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                Customer.I_Status_PC = "NG";
                                db.SaveChanges();
                            }

                        }
                    }
                    else
                    {
                        Customer.I_Status_PC = "NG";
                        db.SaveChanges();
                    }
                }
                else
                {
                    Customer.I_Status_PC = "NG";
                    db.SaveChanges();
                }
            }
            else
            {
                Customer.I_Status_PC = "NG";
                db.SaveChanges();
            }
        }

        public void getPartOK(string TkcPart) //check status
        {
            var db = new TKC_MASTER_SAPEntities();
            var Customer = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == TkcPart)
                .FirstOrDefault();

            if (Customer.I_Status_MK == "OK")
            {
                if (Customer.I_Status_PC == "OK")
                {
                    if (Customer.I_Status_LG == "OK")
                    {
                        if (Customer.I_Status_CO == "OK")
                        {
                            Customer.I_Status = "OK";
                            db.SaveChanges();
                        }
                        else
                        {
                            Customer.I_Status = "NG";
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        Customer.I_Status = "NG";
                        db.SaveChanges();
                    }
                }
                else
                {
                    Customer.I_Status = "NG";
                    db.SaveChanges();
                }
            }
            else
            {
                Customer.I_Status = "NG";
                db.SaveChanges();
            }
        }

        [CustomAuthorize(Roles = "PPC")]
        public ActionResult RptGroup()
        {
            OracleDatabase oracleDatabase = new OracleDatabase();
            oracleDatabase.ConnectDatabase("TKCDEV");

            string sql = "select * from tkcdev.t_sap_rpt_group ";
            var ds = oracleDatabase.OracleGet(sql, "t_sap_rpt_group");
            var dt = ds.Tables["t_sap_rpt_group"].AsEnumerable().Select(r => new T_SAP_RPT_GROUP()
            {
                I_ITEM_CD = (string)r["I_ITEM_CD"],
                PLANT = (string)r["PLANT"],
                SELL_TYPE = (string)r["SELL_TYPE"],
                RPT_GROUP = (string)r["RPT_GROUP"],
                ADD_MNTH = (string)r["ADD_MNTH"],

            }).ToList();

            return View(dt); 
        }

        [CustomAuthorize(Roles = "PPC")]
        public ActionResult AddRptGroup(T_SAP_RPT_GROUP model)
        {
            OracleDatabase oracleDatabase = new OracleDatabase();
            oracleDatabase.ConnectDatabase("TKCDEV");
            /*var db = new TKC_MASTER_SAPEntities();
            var data = db.T_SAP_RPT_GROUP.ToList();*/
            return View();
        }

        public JsonResult insRptGroup(T_SAP_RPT_GROUP model)
        {
            string msg = "";
            var db = new TKC_MASTER_SAPEntities();
            TKC_MASTER_SAPEntities dbcontext = new TKC_MASTER_SAPEntities();

            try
            {
                OracleDatabase oracleDatabase = new OracleDatabase();
                oracleDatabase.ConnectDatabase("TKCDEV");

                string sql = "insert INTO tkcdev.t_sap_rpt_group (I_ITEM_CD, PLANT, SELL_TYPE, RPT_GROUP, ADD_MNTH) ";
                sql += "VALUES(:I_ITEM_CD, :PLANT, :SELL_TYPE, :RPT_GROUP, :ADD_MNTH)";

                OracleParameterCollection param = new OracleCommand().Parameters;
                param.Add("I_ITEM_CD", OracleDbType.NVarchar2).Value = model.I_ITEM_CD;
                param.Add("PLANT", OracleDbType.NVarchar2).Value = model.PLANT;
                param.Add("SELL_TYPE", OracleDbType.NVarchar2).Value = model.SELL_TYPE;
                param.Add("RPT_GROUP", OracleDbType.NVarchar2).Value = model.RPT_GROUP;
                param.Add("ADD_MNTH", OracleDbType.NVarchar2).Value = model.ADD_MNTH;
                int i2 = oracleDatabase.OracleExecute(sql, param);

                msg = "Success!!";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                msg = "error !!";
            }
            return Json(new { result = msg, message = "request successfully" });
        }

        [CustomAuthorize(Roles = "PPC")]
        public ActionResult EditRptGroup(string Item)
        {
            OracleDatabase oracleDatabase = new OracleDatabase();
            oracleDatabase.ConnectDatabase("TKCDEV");

            string sql = "select * from tkcdev.t_sap_rpt_group where I_ITEM_CD = '" + Item + "'  ";
            var ds = oracleDatabase.OracleGet(sql, "t_sap_rpt_group");
            var dt = ds.Tables["t_sap_rpt_group"];

            T_SAP_RPT_GROUP tmp = new T_SAP_RPT_GROUP();
            tmp.I_ITEM_CD = dt.Rows[0]["I_ITEM_CD"].ToString();
            tmp.PLANT = dt.Rows[0]["PLANT"].ToString();
            tmp.SELL_TYPE = dt.Rows[0]["SELL_TYPE"].ToString();
            tmp.RPT_GROUP = dt.Rows[0]["RPT_GROUP"].ToString();
            tmp.ADD_MNTH = dt.Rows[0]["ADD_MNTH"].ToString();

            return View(tmp);
        }

        public JsonResult updateRptGroup(T_SAP_RPT_GROUP model)
        {

            try
            {
                 
                OracleDatabase oracleDatabase = new OracleDatabase();
                oracleDatabase.ConnectDatabase("TKCDEV");

                string sql = "UPDATE tkcdev.t_sap_rpt_group set PLANT=:PLANT, SELL_TYPE=:SELL_TYPE, RPT_GROUP=:RPT_GROUP, ADD_MNTH=:ADD_MNTH ";
                sql += "WHERE I_ITEM_CD = '" + model.I_ITEM_CD + "' ";


                OracleParameterCollection param = new OracleCommand().Parameters;
                //param.Add("I_ITEM_CD", SqlDbType.NVarChar).Value = model.I_ITEM_CD;
                param.Add("PLANT", OracleDbType.NVarchar2).Value = model.PLANT;
                param.Add("SELL_TYPE", OracleDbType.NVarchar2).Value = model.SELL_TYPE;
                param.Add("RPT_GROUP", OracleDbType.NVarchar2).Value = model.RPT_GROUP;
                param.Add("ADD_MNTH", OracleDbType.NVarchar2).Value = model.ADD_MNTH;
                int i2 = oracleDatabase.OracleExecute(sql, param);
            }
            catch (Exception ex)
            {
                return Json(new { result = "error", message = "request error" });
            }
            return Json(new { result = "insert success !!", message = "request successfully" });
        }

        [CustomAuthorize(Roles = "CS,MK,MgrMK,LG,CO,PPC,SaleAdmin,MgrSaleAdmin,PU")]
        public ActionResult ViewDetail(string id)
        {
            var db = new TKC_MASTER_SAPEntities();
            var data = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == id).FirstOrDefault();
            return View(data);
        }
        public void NotificationEmail(string email, string subject, string body)
        {
            //try
            //{


            //}
            //catch (DbEntityValidationException e)
            //{
            //    //foreach (var eve in e.EntityValidationErrors)
            //    //{
            //    //    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            //    //        eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //    //    foreach (var ve in eve.ValidationErrors)
            //    //    {
            //    //        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
            //    //            ve.PropertyName, ve.ErrorMessage);
            //    //    }
            //    //}

            //}

            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress("prones_g@tkoito.co.th");
            msg.To.Add(email);
            msg.Subject = subject;
            msg.Body = body;
            msg.IsBodyHtml = true;

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Port = 25;
            smtpClient.Host = "172.18.1.2";
            smtpClient.Credentials = new NetworkCredential("prones", "zaq1@wsx");
            smtpClient.Send(msg);

        }

        public ActionResult ApprovePPC()
        {
            var db = new TKC_MASTER_SAPEntities();
            var items = db.T_SAP_MAT_SETUP.Where(c => c.I_Status_PC == "OK").Where(x => x.I_Approve_PPC == null).ToList();
            return View(items);
        }

        [HttpPost]
        public JsonResult updateApprovePPC(List<ListDropDown> listkey)
        {
            try
            {
                using (var db = new TKC_MASTER_SAPEntities())
                {
                    foreach (var item in listkey)
                    {
                        var update = db.T_SAP_MAT_SETUP.Where(x => x.I_RegID == item.iKey).FirstOrDefault();

                        if (update != null)
                        {
                            update.I_Approve_PPC = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"); ;
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = "error", message = "request error" });
            }
            return Json(new { result = "insert success !!", message = "request successfully" });
        }
    }
}