﻿using SAP_MAT_SETUP.Models;
using SAP_MAT_SETUP.Users;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Configuration.Provider;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace SAP_MAT_SETUP.CustomAuthentication
{
    public class CustomRole : RoleProvider
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public override bool IsUserInRole(string username, string roleName)
        {
            var userRoles = GetRolesForUser(username);
            return userRoles.Contains(roleName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public override string[] GetRolesForUser(string username)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return null;
            }

            var userRoles = new string[] { };

            using (AuthenticationDB dbContext = new AuthenticationDB())
            {
                var selectedUser = (from us in dbContext.Users.Include("Roles")
                                    where string.Compare(us.Username, username, StringComparison.OrdinalIgnoreCase) == 0
                                    select us).FirstOrDefault();

                
                if(selectedUser != null)
                {
                    userRoles = new[] { selectedUser.Roles.Select(r=>r.RoleName).ToString() };
                }

                return userRoles.ToArray();
            }


        }



        #region Overrides of Role Provider

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            foreach (string rolename in roleNames)
            {
                if (rolename == null || rolename == "")
                    throw new ProviderException("Role name cannot be empty or null.");
            }

            foreach (string username in usernames)
            {
                if (username == null || username == "")
                    throw new ProviderException("User name cannot be empty or null.");
                if (username.Contains(","))
                    throw new ArgumentException("User names cannot contain commas.");

                //foreach (string rolename in rolenames)
                //{
                //    if (IsUserInRole(username, rolename))
                //        throw new ProviderException("User is already in role.");
                //}
            }

            AuthenticationDB dbContext = new AuthenticationDB();

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["UserDev"].ConnectionString);
            SqlCommand comm = conn.CreateCommand();
            string sql = "INSERT INTO UserRoles (UserId, RoleId) VALUES (@userid,@roleid)";
            comm.CommandText = sql;

            try
            {
                conn.Open();
                foreach (string username in usernames)
                {
                    var userid = dbContext.Users.Where(x => x.Username == username).FirstOrDefault();
                    foreach (string rolename in roleNames)
                    {
                        var roleid = dbContext.Roles.Where(x => x.RoleName == rolename).FirstOrDefault();


                        comm.Parameters.AddWithValue("@roleid", roleid.RoleId);
                        comm.Parameters.AddWithValue("@userid", userid.UserId);
                        var i =comm.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException ex)
            {
                // Handle exception.
            }
            finally
            {
                conn.Close();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }


        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}