﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.IO;
using System.Data;

namespace SAP_MAT_SETUP
{
    public class SqlData
    {
        public static string strSQLConn = System.Configuration.ConfigurationManager.ConnectionStrings["SAPMaterialConnectionString"].ConnectionString;

        public static bool CommandSqlData(string command)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(strSQLConn))
                {
                    cn.Open();
                    using (SqlCommand sqlcm = new SqlCommand(command, cn))
                    {
                        sqlcm.ExecuteNonQuery();
                    }
                    cn.Close();
                }
                return true;
            }
            catch //(Exception ex)
            {
                //MessageBox.Show(ex.Message, "MsgBox", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public static DataTable SelectSqlData(string command)
        {
            try
            {
                DataTable result = new DataTable();
                using (SqlConnection cn = new SqlConnection(strSQLConn))
                {
                    cn.Open();
                    
                    using (SqlDataAdapter sqlda = new SqlDataAdapter(command, cn))
                    {
                        sqlda.Fill(result);
                    }
                    cn.Close();
                }
                return result;
            }
            catch(Exception ex) { return new DataTable(); }
        }
    }
}
